#include<iostream>
#include<stdio.h>
#include<stdlib.h>
#include<memory.h>

#include<X11/Xlib.h>
#include<X11/Xutil.h>
#include<X11/XKBlib.h>
#include<X11/keysym.h>

#include<GL/gl.h>
#include<GL/glu.h>
#include<GL/glx.h>      // OpenGL for XWindows
#include<SOIL/SOIL.h>

//namespaces
using namespace std;

//global variables declarations

bool bFullscreen = false;
Display *gpDisplay = NULL;
XVisualInfo *gpXVisualInfo = NULL;
Colormap gColormap;
Window gWindow;
int giWindowWidth = 800;
int giWindowHeight = 600;
GLXContext gGLXContext;

// Following parameters are spesic for light
bool bLight = false;
GLfloat LightAmbientZero[] = { 0.0f,0.0f,0.0f,0.0f };
GLfloat LightDiffuseZero[] = { 1.0f,0.0f,0.0f,1.0f };	//Red
GLfloat LightSpecularZero[] = { 1.0f,0.0f,0.0f,1.0f };
GLfloat LightPositionZero[] = { 0.0f,0.0f,0.0f,1.0f };

GLfloat LightAmbientOne[] = { 0.0f,0.0f,0.0f,0.0f };
GLfloat LightDiffuseOne[] = { 0.0f,1.0f,0.0f,1.0f };		//Green
GLfloat LightSpecularOne[] = { 0.0f,1.0f,0.0f,1.0f };
GLfloat LightPositionOne[] = { 0.0f,0.0f,0.0f,1.0f };

GLfloat LightAmbientTwo[] = { 0.0f,0.0f,0.0f,0.0f };
GLfloat LightDiffuseTwo[] = { 0.0f,0.0f,1.0f,1.0f };		//Blue
GLfloat LightSpecularTwo[] = { 0.0f,0.0f,1.0f,1.0f };
GLfloat LightPositionTwo[] = { 0.0f,0.0f,0.0f,1.0f };

GLfloat MaterialAmbient[] = { 0.0f,0.0f,0.0f,1.0f };	
GLfloat MaterialDiffuse[] = { 1.0f,1.0f,1.0f,1.0f };	
GLfloat MaterialSpecular[] = { 1.0f,1.0f,1.0f,1.0f };	
GLfloat MaterialShininess[] = { 50.0f };	// 128.0f	

GLUquadric *quadric = NULL;			

GLfloat lightAngleZero = 0.0f;
GLfloat lightAngleOne = 0.0f;
GLfloat lightAngleTwo = 0.0f;

//entry point function
int main(void){
	// function prototypes
	void CreateWindow(void);
	void ToggleFullScreen(void);
	void uninitialize();
    void update(void);

    void initialize(void);
    void resize(int, int);
    void display(void);
    void update(void);

	//Variable declarations
	int winWidth = giWindowWidth;
	int winHeight = giWindowHeight;
    char keys[26];
    bool bDone = false;


	//Code
	CreateWindow();
    initialize();
	//Message Loop
	XEvent event;
	KeySym keysym;

	while(bDone == false)
	{
        while(XPending(gpDisplay))
        {
            XNextEvent(gpDisplay, &event);
            switch(event.type)
            {
                case MapNotify:
                    break;
                case KeyPress:
                    keysym = XkbKeycodeToKeysym(gpDisplay, event.xkey.keycode, 0, 0);
                    switch(keysym)
                    {
                        case XK_Escape:
                            bDone = true;
                        default:
                            break;
                    }
                    XLookupString(&event.xkey, keys, sizeof(keys), NULL, NULL);
                    switch (keys[0])
                    {
                        case 'F':
                        case 'f':
                            if(bFullscreen == false)
                            {
                                ToggleFullScreen();
                                bFullscreen = true;
                            }
                            else
                            {
                                ToggleFullScreen();
                                bFullscreen = false;
                            }
                            break;
                        case 'L':
                        case 'l':
                            if (bLight == false)
                            {
                                bLight = true;
                                glEnable(GL_LIGHTING);
                            }
                            else
                            {
                                bLight = false;
                                glDisable(GL_LIGHTING);
                            }
                            break;    
                        default:
                            break;
                    }
                    break;
                case ButtonPress:
                    switch(event.xbutton.button)
                    {
                        case 1:
                            break;
                        case 2:
                            break;
                        case 3:
                            break;
                        case 4:
                            break;
                        default:
                            break;
                    }
                    break;
                case MotionNotify:
                    break;
                case ConfigureNotify:
                    winWidth = event.xconfigure.width;
                    winHeight = event.xconfigure.height;
                    resize(winWidth, winHeight);
                    break;
                case Expose:
                    break;
                case DestroyNotify:
                    break;
                case 33:		// window manager message for close button 
                    bDone = true;
                default:
                    break;		
            }
        }
        update();
        display();
    }    
	uninitialize();
	return 0;
}


void CreateWindow(void){
	//Function prototypes
	void uninitialize(void);

	//Variable declarations
	XSetWindowAttributes winAttribs;
	int defaultScreen;
	int defaultDepth;
	int styleMask;

    //OpenGL changes
    static int frameBufferAttributes[] = {  GLX_RGBA,
                                            GLX_DOUBLEBUFFER, True,
                                            GLX_RED_SIZE, 8,
                                            GLX_GREEN_SIZE, 8,
                                            GLX_BLUE_SIZE, 8,
                                            GLX_ALPHA_SIZE, 8,
                                            GLX_DEPTH_SIZE, 24,
                                            None
                                         };

	//Code
	gpDisplay = XOpenDisplay(NULL);
	if (gpDisplay == NULL)
	{
		printf("ERROR : Unable to open X Display. \nExiting Now...\n");
		uninitialize();
		exit(1);
	}

	defaultScreen = XDefaultScreen(gpDisplay);

	defaultDepth = DefaultDepth(gpDisplay, defaultScreen);

	gpXVisualInfo = glXChooseVisual(gpDisplay, defaultScreen, frameBufferAttributes);
	if(gpXVisualInfo == NULL)
	{
		printf("ERROR : Unable to allocate memory for Visual Info. \nExiting Now...\n");
		uninitialize();
		exit(1);	
	}

	winAttribs.border_pixel = 0;
	winAttribs.background_pixmap = 0;
	winAttribs.colormap = XCreateColormap(gpDisplay,
						  RootWindow(gpDisplay, gpXVisualInfo -> screen),
						  gpXVisualInfo -> visual,
						  AllocNone);
	gColormap = winAttribs.colormap;

	winAttribs.background_pixel = BlackPixel(gpDisplay, defaultScreen);
	winAttribs.event_mask = ExposureMask | VisibilityChangeMask |
							ButtonPressMask | KeyPressMask |
							PointerMotionMask | StructureNotifyMask;

	styleMask = CWBorderPixel | CWBackPixel | CWEventMask | CWColormap;

	gWindow = XCreateWindow(gpDisplay,
					RootWindow(gpDisplay, gpXVisualInfo -> screen),
					0,
					0,
					giWindowWidth,
					giWindowHeight,
					0,
					gpXVisualInfo -> depth,
					InputOutput,
					gpXVisualInfo -> visual,
					styleMask,
					&winAttribs);
	if(!gWindow)
	{
		printf("ERROR : Failed to create main window. \nExiting Now...\n");
		uninitialize();
		exit(1);	
	}

	XStoreName(gpDisplay, gWindow, "FFP - XWindow 3 Lights On Sphere");

	Atom windowManagerDelete = XInternAtom(gpDisplay, "WM_DELETE_WINDOW", True);
	XSetWMProtocols(gpDisplay, gWindow, &windowManagerDelete, 1);

	XMapWindow(gpDisplay, gWindow);	
}


void initialize(void){
    //Function declarations
    void uninitialize(void);
    void resize(int, int);
    void ToggleFullScreen(void);
    bool loadTexture(GLuint *, const char *);
    //Code
    gGLXContext = glXCreateContext( gpDisplay,
                                    gpXVisualInfo,
                                    NULL,
                                    GL_TRUE
                                  );

    glXMakeCurrent(gpDisplay, gWindow, gGLXContext);
    
    glShadeModel(GL_SMOOTH);
	glEnable(GL_DEPTH_TEST);		// To enable depth test
	glDepthFunc(GL_LEQUAL);		// Compare z value of each object with 1.0 which is the max and show only those objects whose depth value is less than or equal to 1.0
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

	glLightfv(GL_LIGHT0, GL_AMBIENT, LightAmbientZero);		
	glLightfv(GL_LIGHT0, GL_DIFFUSE, LightDiffuseZero);		
	glLightfv(GL_LIGHT0, GL_SPECULAR, LightSpecularZero);		
	//glLightfv(GL_LIGHT0, GL_POSITION, LightPositionZero);		
	glEnable(GL_LIGHT0);		

	glLightfv(GL_LIGHT1, GL_AMBIENT, LightAmbientOne);
	glLightfv(GL_LIGHT1, GL_DIFFUSE, LightDiffuseOne);
	glLightfv(GL_LIGHT1, GL_SPECULAR, LightSpecularOne);
	//glLightfv(GL_LIGHT0, GL_POSITION, LightPositionZero);		
	glEnable(GL_LIGHT1);

	glLightfv(GL_LIGHT2, GL_AMBIENT, LightAmbientTwo);
	glLightfv(GL_LIGHT2, GL_DIFFUSE, LightDiffuseTwo);
	glLightfv(GL_LIGHT2, GL_SPECULAR, LightSpecularTwo);
	//glLightfv(GL_LIGHT0, GL_POSITION, LightPositionZero);		
	glEnable(GL_LIGHT2);

	glMaterialfv(GL_FRONT, GL_AMBIENT, MaterialAmbient);		//Right
	glMaterialfv(GL_FRONT, GL_DIFFUSE, MaterialDiffuse);		//Right
	glMaterialfv(GL_FRONT, GL_SPECULAR, MaterialSpecular);		//Right
	glMaterialfv(GL_FRONT, GL_SHININESS, MaterialShininess);		//Right

	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

	glClearDepth(1.0);		// To give existance to depth buffer....3D specific call  (1.0 is the maximum number we are filling depth buffer with)
 
    resize(giWindowWidth, giWindowHeight);          // This is warm up call to resize()
    ToggleFullScreen();
}



void resize(int width, int height){
    //Code
	if (height == 0)
	{
		height = 1;
	}
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45.0f,
		(GLfloat)width/(GLfloat)height,
		0.1f,
		100.0f);
}

void update(){
    //Code
	lightAngleZero = lightAngleZero + 1.0f;
	if (lightAngleZero >= 360.0f)
	{
		lightAngleZero = 0.0f;
	}

	lightAngleOne = lightAngleOne + 1.0f;
	if (lightAngleOne >= 360.0f)
	{
		lightAngleOne = 0.0f;
	}
	
	lightAngleTwo = lightAngleTwo + 1.0f;
	if (lightAngleTwo >= 360.0f)
	{
		lightAngleTwo = 0.0f;
	}
}

void display(){
    
    //Code

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);			// To make depth buffer functional here

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	//glTranslatef(0.0f, 0.0f, -0.70f);		
	glPushMatrix();		// To move camera, we have to push this matrix
	gluLookAt(0.0f, 0.0f, 3.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f);	// Move camera by 3
	
	// Light 1
	glPushMatrix();
	glRotatef(lightAngleZero, 1.0f, 0.0f, 0.0f);	// Rotate light around X axis
	LightPositionZero[1] = lightAngleZero;			// Change the position of light, we are changing Y because we are rotating around X axis
	glLightfv(GL_LIGHT0, GL_POSITION, LightPositionZero);	// To change position of light
	glPopMatrix();

	// Light 2
	glPushMatrix();
	glRotatef(lightAngleOne, 0.0f, 1.0f, 0.0f);
	LightPositionOne[0] = lightAngleOne;	
	glLightfv(GL_LIGHT1, GL_POSITION, LightPositionOne);
	glPopMatrix();

	// Light 3
	glPushMatrix();
	glRotatef(lightAngleTwo, 0.0f, 0.0f, 1.0f);
	LightPositionTwo[0] = lightAngleTwo;
	glLightfv(GL_LIGHT2, GL_POSITION, LightPositionTwo);
	glPopMatrix();
	
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);		
	quadric = gluNewQuadric();
	gluSphere(quadric, 0.7f, 30, 30);

	glPopMatrix();

    glXSwapBuffers(gpDisplay, gWindow);
}




void ToggleFullScreen(void){
	//Variable declarations
	Atom wm_state;
	Atom fullscreen;
	XEvent xev = { 0 };

	//Code
	wm_state = XInternAtom(gpDisplay, "_NET_WM_STATE", False);
	memset(&xev, 0, sizeof(xev));

	xev.type = ClientMessage;
	xev.xclient.window = gWindow;
	xev.xclient.message_type = wm_state;
	xev.xclient.format = 32;
	xev.xclient.data.l[0] = bFullscreen ? 0 : 1;

	fullscreen = XInternAtom(gpDisplay, "_NET_WM_STATE_FULLSCREEN", False);
	xev.xclient.data.l[1] = fullscreen;

	XSendEvent(gpDisplay,
			RootWindow(gpDisplay, gpXVisualInfo->screen),
			False,
			StructureNotifyMask,
			&xev);
}

void uninitialize(void){
	//Code
    GLXContext currentGLXContext = glXGetCurrentContext();

    if (currentGLXContext != NULL && currentGLXContext == gGLXContext){
        glXMakeCurrent(gpDisplay, 0, 0);
    }
    
    if (gGLXContext) {
        glXDestroyContext(gpDisplay, gGLXContext);
    }
	if(gWindow)
	{
		XDestroyWindow(gpDisplay, gWindow);
	}

	if(gColormap)
	{
		XFreeColormap(gpDisplay, gColormap);
	}

	if (gpXVisualInfo)
	{
		free(gpXVisualInfo);
		gpXVisualInfo = NULL;
	}

	if(gpDisplay)
	{
		XCloseDisplay(gpDisplay);
		gpDisplay = NULL;
	}
}
