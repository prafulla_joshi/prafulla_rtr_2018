#include<iostream>
#include<stdio.h>
#include<stdlib.h>
#include<memory.h>

#include<X11/Xlib.h>
#include<X11/Xutil.h>
#include<X11/XKBlib.h>
#include<X11/keysym.h>

#include<GL/gl.h>
#include<GL/glu.h>
#include<GL/glx.h>      // OpenGL for XWindows
#include<SOIL/SOIL.h>

//namespaces
using namespace std;

//global variables declarations

bool bFullscreen = false;
Display *gpDisplay = NULL;
XVisualInfo *gpXVisualInfo = NULL;
Colormap gColormap;
Window gWindow;
int giWindowWidth = 800;
int giWindowHeight = 600;
GLXContext gGLXContext;

//Texture specific global variables
GLdouble pyramidAngle = 0.0;		// angle for rotation of pyramid about Y axis i.e., Spinning
GLdouble cubeAngle = 0.0;			// angle for arbitary rotation of cube 
GLuint texture_stone;
GLuint texture_kundali;

//entry point function
int main(void){
	// function prototypes
	void CreateWindow(void);
	void ToggleFullScreen(void);
	void uninitialize();
    void update(void);

    void initialize(void);
    void resize(int, int);
    void display(void);
    void update(void);

	//Variable declarations
	int winWidth = giWindowWidth;
	int winHeight = giWindowHeight;
    char keys[26];
    bool bDone = false;


	//Code
	CreateWindow();
    initialize();
	//Message Loop
	XEvent event;
	KeySym keysym;

	while(bDone == false)
	{
        while(XPending(gpDisplay))
        {
            XNextEvent(gpDisplay, &event);
            switch(event.type)
            {
                case MapNotify:
                    break;
                case KeyPress:
                    keysym = XkbKeycodeToKeysym(gpDisplay, event.xkey.keycode, 0, 0);
                    switch(keysym)
                    {
                        case XK_Escape:
                            bDone = true;
                        default:
                            break;
                    }
                    XLookupString(&event.xkey, keys, sizeof(keys), NULL, NULL);
                    switch (keys[0])
                    {
                        case 'F':
                        case 'f':
                            if(bFullscreen == false)
                            {
                                ToggleFullScreen();
                                bFullscreen = true;
                            }
                            else
                            {
                                ToggleFullScreen();
                                bFullscreen = false;
                            }
                            break;
                        default:
                            break;
                    }
                    break;
                case ButtonPress:
                    switch(event.xbutton.button)
                    {
                        case 1:
                            break;
                        case 2:
                            break;
                        case 3:
                            break;
                        case 4:
                            break;
                        default:
                            break;
                    }
                    break;
                case MotionNotify:
                    break;
                case ConfigureNotify:
                    winWidth = event.xconfigure.width;
                    winHeight = event.xconfigure.height;
                    resize(winWidth, winHeight);
                    break;
                case Expose:
                    break;
                case DestroyNotify:
                    break;
                case 33:		// window manager message for close button 
                    bDone = true;
                default:
                    break;		
            }
        }
        update();
        display();
    }    
	uninitialize();
	return 0;
}


void CreateWindow(void){
	//Function prototypes
	void uninitialize(void);

	//Variable declarations
	XSetWindowAttributes winAttribs;
	int defaultScreen;
	int defaultDepth;
	int styleMask;

    //OpenGL changes
    static int frameBufferAttributes[] = {  GLX_RGBA,
                                            GLX_DOUBLEBUFFER, True,
                                            GLX_RED_SIZE, 8,
                                            GLX_GREEN_SIZE, 8,
                                            GLX_BLUE_SIZE, 8,
                                            GLX_ALPHA_SIZE, 8,
                                            GLX_DEPTH_SIZE, 24,
                                            None
                                         };

	//Code
	gpDisplay = XOpenDisplay(NULL);
	if (gpDisplay == NULL)
	{
		printf("ERROR : Unable to open X Display. \nExiting Now...\n");
		uninitialize();
		exit(1);
	}

	defaultScreen = XDefaultScreen(gpDisplay);

	defaultDepth = DefaultDepth(gpDisplay, defaultScreen);

	gpXVisualInfo = glXChooseVisual(gpDisplay, defaultScreen, frameBufferAttributes);
	if(gpXVisualInfo == NULL)
	{
		printf("ERROR : Unable to allocate memory for Visual Info. \nExiting Now...\n");
		uninitialize();
		exit(1);	
	}

	winAttribs.border_pixel = 0;
	winAttribs.background_pixmap = 0;
	winAttribs.colormap = XCreateColormap(gpDisplay,
						  RootWindow(gpDisplay, gpXVisualInfo -> screen),
						  gpXVisualInfo -> visual,
						  AllocNone);
	gColormap = winAttribs.colormap;

	winAttribs.background_pixel = BlackPixel(gpDisplay, defaultScreen);
	winAttribs.event_mask = ExposureMask | VisibilityChangeMask |
							ButtonPressMask | KeyPressMask |
							PointerMotionMask | StructureNotifyMask;

	styleMask = CWBorderPixel | CWBackPixel | CWEventMask | CWColormap;

	gWindow = XCreateWindow(gpDisplay,
					RootWindow(gpDisplay, gpXVisualInfo -> screen),
					0,
					0,
					giWindowWidth,
					giWindowHeight,
					0,
					gpXVisualInfo -> depth,
					InputOutput,
					gpXVisualInfo -> visual,
					styleMask,
					&winAttribs);
	if(!gWindow)
	{
		printf("ERROR : Failed to create main window. \nExiting Now...\n");
		uninitialize();
		exit(1);	
	}

	XStoreName(gpDisplay, gWindow, "FFP - XWindow Texture Application");

	Atom windowManagerDelete = XInternAtom(gpDisplay, "WM_DELETE_WINDOW", True);
	XSetWMProtocols(gpDisplay, gWindow, &windowManagerDelete, 1);

	XMapWindow(gpDisplay, gWindow);	
}


void initialize(void){
    //Function declarations
    void uninitialize(void);
    void resize(int, int);
    void ToggleFullScreen(void);
    bool loadTexture(GLuint *, const char *);
    //Code
    gGLXContext = glXCreateContext( gpDisplay,
                                    gpXVisualInfo,
                                    NULL,
                                    GL_TRUE
                                  );

    glXMakeCurrent(gpDisplay, gWindow, gGLXContext);
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);           // Black background
    
    glEnable(GL_DEPTH_TEST);		// To enable depth test
	glDepthFunc(GL_LEQUAL);	
    glClearDepth(1.0);		// To give existance to depth buffer....3D specific call  (1.0 is the maximum number we are filling depth buffer with)
    

    glShadeModel(GL_SMOOTH);							//To remove aliasing
    glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);	//To remove distortion

    // Enable texture mapping
    glEnable(GL_TEXTURE_2D); 
    loadTexture(&texture_kundali, "Kundali.bmp");
    loadTexture(&texture_stone, "Stone.bmp");

    resize(giWindowWidth, giWindowHeight);          // This is warm up call to resize()
    ToggleFullScreen();
}

bool loadTexture(GLuint *texture, const char *path)
{
	//Variable declaration
    bool bResult = false;
    int width;
    int height;
    unsigned char *image_data = NULL;

	//Code

	image_data = SOIL_load_image(path, &width, &height, 0, SOIL_LOAD_RGB);


    if (image_data == NULL) {
        bResult = false;
        return bResult;
    }
    else
    {
        bResult = true;
        glPixelStorei(GL_UNPACK_ALIGNMENT, 4);
        glGenTextures(1, texture);
        glBindTexture(GL_TEXTURE_2D, *texture);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
        gluBuild2DMipmaps(GL_TEXTURE_2D,
            3,
            width,
            height,
            GL_BGR_EXT,
            GL_UNSIGNED_BYTE,
            (GLvoid*)image_data);
        //Free texture data
        SOIL_free_image_data(image_data);
        return bResult;
    }

    
}

void resize(int width, int height){
    //Code
	if (height == 0)
	{
		height = 1;
	}
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45.0f,
		(GLfloat)width/(GLfloat)height,
		0.1f,
		100.0f);
}

void update(){
    //code
	pyramidAngle = pyramidAngle + 0.5;
	if (pyramidAngle >= 360.0)
		pyramidAngle = 0.0;

	cubeAngle = cubeAngle + 0.5;
	if (cubeAngle >= 360.0)
		cubeAngle = 0.0;
}

void display(){
    
    //Local functions declarations
	void DrawPyramid(void);
	void Draw_Cube(void);

	//Code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);		// To make depth buffer functional; here actually depth buffer is filled up with 1.0
	// For Pyramid
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(-1.5f, 0.0f, -6.0f);
	glRotatef(pyramidAngle, 0.0f, 1.0f, 0.0f);
	glBindTexture(GL_TEXTURE_2D, texture_stone);
	glBegin(GL_TRIANGLES);
	DrawPyramid();
	glEnd();

	// For Cube
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(1.5f, 0.0f, -6.0f);
	glScalef(0.75f, 0.75f, 0.75f);
	glRotatef(cubeAngle, 1.0f, 1.0f, 1.0f);
	glBindTexture(GL_TEXTURE_2D, texture_kundali);
	glBegin(GL_QUADS);
	Draw_Cube();
	glEnd();

    glXSwapBuffers(gpDisplay, gWindow);
}

void DrawPyramid(void)
{
	/* PYRAMID WILL HAVE 4 TRIANGUALR FACES..FOLLOWING ARE THE CO-ORIDNATES FOR EACH FACE */

	// FRONT FACE
	//glColor3f(1.0f, 0.0f, 0.0f);		// RED
	glTexCoord2f(0.5f, 1.0f);
	glVertex3f(0.0f, 1.0f, 0.0f);

	//glColor3f(0.0f, 1.0f, 0.0f);		// GREEN
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-1.0f, -1.0f, 1.0f);

	//glColor3f(0.0f, 0.0f, 1.0f);		// BLUE
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(1.0f, -1.0f, 1.0f);

	// RIGHT FACE
	//glColor3f(1.0f, 0.0f, 0.0f);		// RED
	glTexCoord2f(0.5f, 1.0f);
	glVertex3f(0.0f, 1.0f, 0.0f);

	//glColor3f(0.0f, 0.0f, 1.0f);		// BLUE
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(1.0f, -1.0f, 1.0f);

	//glColor3f(0.0f, 1.0f, 0.0f);		// GREEN
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(1.0f, -1.0f, -1.0f);

	// BACK FACE
	//glColor3f(1.0f, 0.0f, 0.0f);		// RED
	glTexCoord2f(0.5f, 1.0f);
	glVertex3f(0.0f, 1.0f, 0.0f);

	//glColor3f(0.0f, 1.0f, 0.0f);		// GREEN
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(1.0f, -1.0f, -1.0f);

	//glColor3f(0.0f, 0.0f, 1.0f);		// BLUE
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-1.0f, -1.0f, -1.0f);

	// LEFT FACE
	//glColor3f(1.0f, 0.0f, 0.0f);		// RED
	glTexCoord2f(0.5f, 1.0f);
	glVertex3f(0.0f, 1.0f, 0.0f);

	//glColor3f(0.0f, 0.0f, 1.0f);		// BLUE
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-1.0f, -1.0f, -1.0f);

	//glColor3f(0.0f, 1.0f, 0.0f);		// GREEN
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(-1.0f, -1.0f, 1.0f);
}


void Draw_Cube(void)
{
	/*Cube has total 6 surfaces. We will give 3D co-ordinates to each surface as following */

	//Top Surface
    glTexCoord2f(0.0f, 0.0f);
    glVertex3f(1.0f, 1.0f, -1.0f);

    glTexCoord2f(0.0f, 1.0f);
    glVertex3f(-1.0f, 1.0f, -1.0f);

    glTexCoord2f(1.0f, 1.0f);
    glVertex3f(-1.0f, 1.0f, 1.0f);

    glTexCoord2f(1.0f, 0.0f);
    glVertex3f(1.0f, 1.0f, 1.0f);


	//Bottom Surface
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, -1.0f);

	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(-1.0f, -1.0f, -1.0f);

	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-1.0f, -1.0f, 1.0f);

	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(1.0f, -1.0f, 1.0f);

	// Front Surface
    glTexCoord2f(1.0f, 0.0f);
	glVertex3f(1.0f, 1.0f, 1.0f);

	glTexCoord2f(0.0f, 0.0f);
    glVertex3f(-1.0f, 1.0f, 1.0f);

	glTexCoord2f(0.0f, 1.0f);
    glVertex3f(-1.0f, -1.0f, 1.0f);

	glTexCoord2f(1.0f, 1.0f);
    glVertex3f(1.0f, -1.0f, 1.0f);


	// Right Surface
	glTexCoord2f(1.0f, 1.0f);
    glVertex3f(1.0f, 1.0f, -1.0f);

	glTexCoord2f(1.0f, 0.0f);
    glVertex3f(1.0f, 1.0f, 1.0f);

	glTexCoord2f(0.0f, 0.0f);
    glVertex3f(1.0f, -1.0f, 1.0f);

	glTexCoord2f(0.0f, 1.0f);
    glVertex3f(1.0f, -1.0f, -1.0f);


	// Back Surface
	glTexCoord2f(1.0f, 1.0f);
    glVertex3f(-1.0f, 1.0f, -1.0f);

	glTexCoord2f(1.0f, 0.0f);
    glVertex3f(1.0f, 1.0f, -1.0f);

	glTexCoord2f(0.0f, 0.0f);
    glVertex3f(1.0f, -1.0f, -1.0f);

	glTexCoord2f(0.0f, 1.0f);
    glVertex3f(-1.0f, -1.0f, -1.0f);


	// Left Surface
	glTexCoord2f(1.0f, 0.0f);
    glVertex3f(-1.0f, 1.0f, 1.0f);

	glTexCoord2f(0.0f, 0.0f);
    glVertex3f(-1.0f, 1.0f, -1.0f);

	glTexCoord2f(0.0f, 1.0f);
    glVertex3f(-1.0f, -1.0f, -1.0f);

	glTexCoord2f(1.0f, 1.0f);
    glVertex3f(-1.0f, -1.0f, 1.0f);

}


void ToggleFullScreen(void){
	//Variable declarations
	Atom wm_state;
	Atom fullscreen;
	XEvent xev = { 0 };

	//Code
	wm_state = XInternAtom(gpDisplay, "_NET_WM_STATE", False);
	memset(&xev, 0, sizeof(xev));

	xev.type = ClientMessage;
	xev.xclient.window = gWindow;
	xev.xclient.message_type = wm_state;
	xev.xclient.format = 32;
	xev.xclient.data.l[0] = bFullscreen ? 0 : 1;

	fullscreen = XInternAtom(gpDisplay, "_NET_WM_STATE_FULLSCREEN", False);
	xev.xclient.data.l[1] = fullscreen;

	XSendEvent(gpDisplay,
			RootWindow(gpDisplay, gpXVisualInfo->screen),
			False,
			StructureNotifyMask,
			&xev);
}

void uninitialize(void){
	//Code
    GLXContext currentGLXContext = glXGetCurrentContext();

    if (currentGLXContext != NULL && currentGLXContext == gGLXContext){
        glXMakeCurrent(gpDisplay, 0, 0);
    }
    
    if (gGLXContext) {
        glXDestroyContext(gpDisplay, gGLXContext);
    }
	if(gWindow)
	{
		XDestroyWindow(gpDisplay, gWindow);
	}

	if(gColormap)
	{
		XFreeColormap(gpDisplay, gColormap);
	}

	if (gpXVisualInfo)
	{
		free(gpXVisualInfo);
		gpXVisualInfo = NULL;
	}

	if(gpDisplay)
	{
		XCloseDisplay(gpDisplay);
		gpDisplay = NULL;
	}
}
