#include<iostream>
#include<stdio.h>
#include<stdlib.h>
#include<memory.h>

#include<X11/Xlib.h>
#include<X11/Xutil.h>
#include<X11/XKBlib.h>
#include<X11/keysym.h>

#include<GL/gl.h>
#include<GL/glu.h>
#include<GL/glx.h>      // OpenGL for XWindows

//namespaces
using namespace std;

//global variables declarations

bool bFullscreen = false;
Display *gpDisplay = NULL;
XVisualInfo *gpXVisualInfo = NULL;
Colormap gColormap;
Window gWindow;
int giWindowWidth = 800;
int giWindowHeight = 600;

GLXContext gGLXContext;

//entry point function
int main(void){
	// function prototypes
	void CreateWindow(void);
	void ToggleFullScreen(void);
	void uninitialize();

    void initialize(void);
    void resize(int, int);
    void display(void);
    void update(void);

	//Variable declarations
	int winWidth = giWindowWidth;
	int winHeight = giWindowHeight;
    char keys[26];
    bool bDone = false;


	//Code
	CreateWindow();
    initialize();
	//Message Loop
	XEvent event;
	KeySym keysym;

	while(bDone == false)
	{
        while(XPending(gpDisplay))
        {
            XNextEvent(gpDisplay, &event);
            switch(event.type)
            {
                case MapNotify:
                    break;
                case KeyPress:
                    keysym = XkbKeycodeToKeysym(gpDisplay, event.xkey.keycode, 0, 0);
                    switch(keysym)
                    {
                        case XK_Escape:
                            bDone = true;
                        default:
                            break;
                    }
                    XLookupString(&event.xkey, keys, sizeof(keys), NULL, NULL);
                    switch (keys[0])
                    {
                        case 'F':
                        case 'f':
                            if(bFullscreen == false)
                            {
                                ToggleFullScreen();
                                bFullscreen = true;
                            }
                            else
                            {
                                ToggleFullScreen();
                                bFullscreen = false;
                            }
                            break;
                        default:
                            break;
                    }
                    break;
                case ButtonPress:
                    switch(event.xbutton.button)
                    {
                        case 1:
                            break;
                        case 2:
                            break;
                        case 3:
                            break;
                        case 4:
                            break;
                        default:
                            break;
                    }
                    break;
                case MotionNotify:
                    break;
                case ConfigureNotify:
                    winWidth = event.xconfigure.width;
                    winHeight = event.xconfigure.height;
                    resize(winWidth, winHeight);
                    break;
                case Expose:
                    break;
                case DestroyNotify:
                    break;
                case 33:		// window manager message for close button 
                    bDone = true;
                default:
                    break;		
            }
        }
        //update();
        display();
    }    
	uninitialize();
	return 0;
}


void CreateWindow(void){
	//Function prototypes
	void uninitialize(void);

	//Variable declarations
	XSetWindowAttributes winAttribs;
	int defaultScreen;
	int defaultDepth;
	int styleMask;

    //OpenGL changes
    static int frameBufferAttributes[] = {  GLX_RGBA,
                                            GLX_DOUBLEBUFFER, True,
                                            GLX_RED_SIZE, 8,
                                            GLX_GREEN_SIZE, 8,
                                            GLX_BLUE_SIZE, 8,
                                            GLX_ALPHA_SIZE, 8,
                                            GLX_DEPTH_SIZE, 24,
                                            None
                                         };

	//Code
	gpDisplay = XOpenDisplay(NULL);
	if (gpDisplay == NULL)
	{
		printf("ERROR : Unable to open X Display. \nExiting Now...\n");
		uninitialize();
		exit(1);
	}

	defaultScreen = XDefaultScreen(gpDisplay);

	defaultDepth = DefaultDepth(gpDisplay, defaultScreen);

	gpXVisualInfo = glXChooseVisual(gpDisplay, defaultScreen, frameBufferAttributes);
	if(gpXVisualInfo == NULL)
	{
		printf("ERROR : Unable to allocate memory for Visual Info. \nExiting Now...\n");
		uninitialize();
		exit(1);	
	}

	winAttribs.border_pixel = 0;
	winAttribs.background_pixmap = 0;
	winAttribs.colormap = XCreateColormap(gpDisplay,
						  RootWindow(gpDisplay, gpXVisualInfo -> screen),
						  gpXVisualInfo -> visual,
						  AllocNone);
	gColormap = winAttribs.colormap;

	winAttribs.background_pixel = BlackPixel(gpDisplay, defaultScreen);
	winAttribs.event_mask = ExposureMask | VisibilityChangeMask |
							ButtonPressMask | KeyPressMask |
							PointerMotionMask | StructureNotifyMask;

	styleMask = CWBorderPixel | CWBackPixel | CWEventMask | CWColormap;

	gWindow = XCreateWindow(gpDisplay,
					RootWindow(gpDisplay, gpXVisualInfo -> screen),
					0,
					0,
					giWindowWidth,
					giWindowHeight,
					0,
					gpXVisualInfo -> depth,
					InputOutput,
					gpXVisualInfo -> visual,
					styleMask,
					&winAttribs);
	if(!gWindow)
	{
		printf("ERROR : Failed to create main window. \nExiting Now...\n");
		uninitialize();
		exit(1);	
	}

	XStoreName(gpDisplay, gWindow, "FFP - XWindow Graph Paper");

	Atom windowManagerDelete = XInternAtom(gpDisplay, "WM_DELETE_WINDOW", True);
	XSetWMProtocols(gpDisplay, gWindow, &windowManagerDelete, 1);

	XMapWindow(gpDisplay, gWindow);	
}


void initialize(void){
    //Function declarations
    void uninitialize(void);
    void resize(int, int);
    void ToggleFullScreen(void);
    //Code
    gGLXContext = glXCreateContext( gpDisplay,
                                    gpXVisualInfo,
                                    NULL,
                                    GL_TRUE
                                  );

    glXMakeCurrent(gpDisplay, gWindow, gGLXContext);
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);           // Black background
    resize(giWindowWidth, giWindowHeight);          // This is warm up call to resize()
    ToggleFullScreen();
}

void resize(int width, int height){
    //Code
	if (height == 0)
	{
		height = 1;
	}
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
}

void display(){
    //Code
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

    glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	float i;
	
	// Horizontal lines ---- Parallel to X-Axis
	glLineWidth(1.0f);
	glBegin(GL_LINES);
	for (i = 1.0f; i > -1.0f; i = i - 0.05f)
	{
		glColor3f(1.0f, 0.0f, 0.0f);
		glVertex2f(-1.0f, i);
		glColor3f(1.0f, 0.0f, 0.0f);
		glVertex2f(1.0f, i);
	}

	glEnd();

	// Vertical lines ---- Parallel to Y-Axis
	glLineWidth(1.0f);
	glBegin(GL_LINES);
	for (i = -1.0f; i < 1.0f; i = i + 0.05f)
	{
		glColor3f(1.0f, 0.0f, 0.0f);
		glVertex2f(i, 1.0f);
		glColor3f(1.0f, 0.0f, 0.0f);
		glVertex2f(i, -1.0f);
	}

	glEnd();

	// For X-Axis
	glLineWidth(3.0f);
	glBegin(GL_LINES);
	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex2f(-1.0f, 0.0f);
	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex2f(1.0f, 0.0f);

	glEnd();
	

	// For Y-Axis
	glLineWidth(3.0f);
	glBegin(GL_LINES);
	
	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex2f(0.0f, 1.0f);
	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex2f(0.0f, -1.0f);
	
	glEnd();

    glXSwapBuffers(gpDisplay, gWindow);
}

void ToggleFullScreen(void){
	//Variable declarations
	Atom wm_state;
	Atom fullscreen;
	XEvent xev = { 0 };

	//Code
	wm_state = XInternAtom(gpDisplay, "_NET_WM_STATE", False);
	memset(&xev, 0, sizeof(xev));

	xev.type = ClientMessage;
	xev.xclient.window = gWindow;
	xev.xclient.message_type = wm_state;
	xev.xclient.format = 32;
	xev.xclient.data.l[0] = bFullscreen ? 0 : 1;

	fullscreen = XInternAtom(gpDisplay, "_NET_WM_STATE_FULLSCREEN", False);
	xev.xclient.data.l[1] = fullscreen;

	XSendEvent(gpDisplay,
			RootWindow(gpDisplay, gpXVisualInfo->screen),
			False,
			StructureNotifyMask,
			&xev);
}

void uninitialize(void){
	//Code
    GLXContext currentGLXContext = glXGetCurrentContext();

    if (currentGLXContext != NULL && currentGLXContext == gGLXContext){
        glXMakeCurrent(gpDisplay, 0, 0);
    }
    
    if (gGLXContext) {
        glXDestroyContext(gpDisplay, gGLXContext);
    }
	if(gWindow)
	{
		XDestroyWindow(gpDisplay, gWindow);
	}

	if(gColormap)
	{
		XFreeColormap(gpDisplay, gColormap);
	}

	if (gpXVisualInfo)
	{
		free(gpXVisualInfo);
		gpXVisualInfo = NULL;
	}

	if(gpDisplay)
	{
		XCloseDisplay(gpDisplay);
		gpDisplay = NULL;
	}
}
