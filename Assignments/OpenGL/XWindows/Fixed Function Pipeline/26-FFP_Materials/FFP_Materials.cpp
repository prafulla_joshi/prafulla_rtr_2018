#include<iostream>
#include<stdio.h>
#include<stdlib.h>
#include<memory.h>

#include<X11/Xlib.h>
#include<X11/Xutil.h>
#include<X11/XKBlib.h>
#include<X11/keysym.h>

#include<GL/gl.h>
#include<GL/glu.h>
#include<GL/glx.h>      // OpenGL for XWindows
#include<SOIL/SOIL.h>

//namespaces
using namespace std;

//global variables declarations

bool bFullscreen = false;
Display *gpDisplay = NULL;
XVisualInfo *gpXVisualInfo = NULL;
Colormap gColormap;
Window gWindow;
int giWindowWidth = 800;
int giWindowHeight = 600;
GLXContext gGLXContext;

// Following parameters are spesic for light
bool bLight = false;
GLfloat LightAmbient[] = { 0.0f,0.0f,0.0f,0.0f };
GLfloat LightDiffuse[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat LightPosition[] = { 0.0f,0.0f,0.0f,1.0f };


GLfloat light_model_ambient[] = { 0.2f,0.2f,0.2f,1.0f };
GLfloat light_model_local_viewer[] = { 0.0f };

GLUquadric *quadric[24];

GLfloat angleOfXRotation = 0.0f;
GLfloat angleOfYRotation = 0.0f;
GLfloat angleOfZRotation = 0.0f;
GLint keyPressed = 0;

//entry point function
int main(void){
	// function prototypes
	void CreateWindow(void);
	void ToggleFullScreen(void);
	void uninitialize();
    void update(void);

    void initialize(void);
    void resize(int, int);
    void display(void);
    void update(void);

	//Variable declarations
	int winWidth = giWindowWidth;
	int winHeight = giWindowHeight;
    char keys[26];
    bool bDone = false;


	//Code
	CreateWindow();
    initialize();
	//Message Loop
	XEvent event;
	KeySym keysym;

	while(bDone == false)
	{
        while(XPending(gpDisplay))
        {
            XNextEvent(gpDisplay, &event);
            switch(event.type)
            {
                case MapNotify:
                    break;
                case KeyPress:
                    keysym = XkbKeycodeToKeysym(gpDisplay, event.xkey.keycode, 0, 0);
                    switch(keysym)
                    {
                        case XK_Escape:
                            bDone = true;
                        default:
                            break;
                    }
                    XLookupString(&event.xkey, keys, sizeof(keys), NULL, NULL);
                    switch (keys[0])
                    {
                        case 'F':
                        case 'f':
                            if(bFullscreen == false)
                            {
                                ToggleFullScreen();
                                bFullscreen = true;
                            }
                            else
                            {
                                ToggleFullScreen();
                                bFullscreen = false;
                            }
                            break;
                        case 'L':
                        case 'l':
                            if (bLight == false)
                            {
                                bLight = true;
                                glEnable(GL_LIGHTING);
                            }
                            else
                            {
                                bLight = false;
                                glDisable(GL_LIGHTING);
                            }
                            break;
                        case 'X':
                        case 'x':
                            keyPressed = 1;
                            angleOfXRotation = 0.0f;
                            break;
                        case 'Y':
                        case 'y':
                            keyPressed = 2;
                            angleOfYRotation = 0.0f;
                            break;
                        case 'Z':
                        case 'z':
                            keyPressed = 3;
                            angleOfZRotation = 0.0f;
                            break;  
                        default:
                            break;
                    }
                    break;
                case ButtonPress:
                    switch(event.xbutton.button)
                    {
                        case 1:
                            break;
                        case 2:
                            break;
                        case 3:
                            break;
                        case 4:
                            break;
                        default:
                            break;
                    }
                    break;
                case MotionNotify:
                    break;
                case ConfigureNotify:
                    winWidth = event.xconfigure.width;
                    winHeight = event.xconfigure.height;
                    resize(winWidth, winHeight);
                    break;
                case Expose:
                    break;
                case DestroyNotify:
                    break;
                case 33:		// window manager message for close button 
                    bDone = true;
                default:
                    break;		
            }
        }
        update();
        display();
    }    
	uninitialize();
	return 0;
}


void CreateWindow(void){
	//Function prototypes
	void uninitialize(void);

	//Variable declarations
	XSetWindowAttributes winAttribs;
	int defaultScreen;
	int defaultDepth;
	int styleMask;

    //OpenGL changes
    static int frameBufferAttributes[] = {  GLX_RGBA,
                                            GLX_DOUBLEBUFFER, True,
                                            GLX_RED_SIZE, 8,
                                            GLX_GREEN_SIZE, 8,
                                            GLX_BLUE_SIZE, 8,
                                            GLX_ALPHA_SIZE, 8,
                                            GLX_DEPTH_SIZE, 24,
                                            None
                                         };

	//Code
	gpDisplay = XOpenDisplay(NULL);
	if (gpDisplay == NULL)
	{
		printf("ERROR : Unable to open X Display. \nExiting Now...\n");
		uninitialize();
		exit(1);
	}

	defaultScreen = XDefaultScreen(gpDisplay);

	defaultDepth = DefaultDepth(gpDisplay, defaultScreen);

	gpXVisualInfo = glXChooseVisual(gpDisplay, defaultScreen, frameBufferAttributes);
	if(gpXVisualInfo == NULL)
	{
		printf("ERROR : Unable to allocate memory for Visual Info. \nExiting Now...\n");
		uninitialize();
		exit(1);	
	}

	winAttribs.border_pixel = 0;
	winAttribs.background_pixmap = 0;
	winAttribs.colormap = XCreateColormap(gpDisplay,
						  RootWindow(gpDisplay, gpXVisualInfo -> screen),
						  gpXVisualInfo -> visual,
						  AllocNone);
	gColormap = winAttribs.colormap;

	winAttribs.background_pixel = BlackPixel(gpDisplay, defaultScreen);
	winAttribs.event_mask = ExposureMask | VisibilityChangeMask |
							ButtonPressMask | KeyPressMask |
							PointerMotionMask | StructureNotifyMask;

	styleMask = CWBorderPixel | CWBackPixel | CWEventMask | CWColormap;

	gWindow = XCreateWindow(gpDisplay,
					RootWindow(gpDisplay, gpXVisualInfo -> screen),
					0,
					0,
					giWindowWidth,
					giWindowHeight,
					0,
					gpXVisualInfo -> depth,
					InputOutput,
					gpXVisualInfo -> visual,
					styleMask,
					&winAttribs);
	if(!gWindow)
	{
		printf("ERROR : Failed to create main window. \nExiting Now...\n");
		uninitialize();
		exit(1);	
	}

	XStoreName(gpDisplay, gWindow, "FFP - XWindow 24 Spherers-- Materials");

	Atom windowManagerDelete = XInternAtom(gpDisplay, "WM_DELETE_WINDOW", True);
	XSetWMProtocols(gpDisplay, gWindow, &windowManagerDelete, 1);

	XMapWindow(gpDisplay, gWindow);	
}


void initialize(void){
    //Function declarations
    void uninitialize(void);
    void resize(int, int);
    void ToggleFullScreen(void);
    bool loadTexture(GLuint *, const char *);
    //Code
    gGLXContext = glXCreateContext( gpDisplay,
                                    gpXVisualInfo,
                                    NULL,
                                    GL_TRUE
                                  );

    glXMakeCurrent(gpDisplay, gWindow, gGLXContext);
    
    glShadeModel(GL_SMOOTH);
	glEnable(GL_DEPTH_TEST);		// To enable depth test
	glDepthFunc(GL_LEQUAL);		// Compare z value of each object with 1.0 which is the max and show only those objects whose depth value is less than or equal to 1.0
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

	glEnable(GL_AUTO_NORMAL);
	glEnable(GL_NORMALIZE);

	glLightfv(GL_LIGHT0, GL_AMBIENT, LightAmbient);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, LightDiffuse);
	glLightfv(GL_LIGHT0, GL_POSITION, LightPosition);
	//glLightfv(GL_LIGHT0, GL_SPECULAR, LightSpecular);	
	glLightModelfv(GL_LIGHT_MODEL_AMBIENT, light_model_ambient);
	glLightModelfv(GL_LIGHT_MODEL_LOCAL_VIEWER, light_model_local_viewer);

	glEnable(GL_LIGHT0);

	for (int i = 0; i < 24; i++)
	{
		quadric[i] = gluNewQuadric();
	}

	glClearColor(0.25f, 0.25f, 0.25f, 1.0f);

	glClearDepth(1.0);		// To give existance to depth buffer....3D specific call  (1.0 is the maximum number we are filling depth buffer with)
 
    resize(giWindowWidth, giWindowHeight);          // This is warm up call to resize()
    ToggleFullScreen();
}



void resize(int width, int height){
    /*For this application we are using ortho projection since this we are taking this application from Green Book by Mark Kilgard*/
	if (height == 0)
	{
		height = 1;
	}
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	if (width <= height)
	{
		glOrtho(0.0f,		// left
			15.5f,			// right
			0.0f,			// bottom
			(15.5f*(GLfloat)height / (GLfloat)width),	// top
			-10.0f,			// near
			10.0f);			// far
	}
	else
	{
		glOrtho(0.0f,
			(15.5f*(GLfloat)width / (GLfloat)height),
			0.0f,
			15.5f,
			-10.0f,
			10.0f);
	}
}

void update(){
    //Code
	angleOfXRotation = angleOfXRotation + 0.9f;

	angleOfYRotation = angleOfYRotation + 0.9f;

	angleOfZRotation = angleOfZRotation + 0.9f;
}

void display(){
    
    // function declarations
	void draw24Spheres(void);

	//Code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);			// To make depth buffer functional here
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	//glTranslatef(0.0f, 0.0f, -0.70f);		//Right

	if (keyPressed == 1)
	{
		glRotatef(angleOfXRotation, 1.0f, 0.0f, 0.0f);
		LightPosition[1] = angleOfXRotation;
	}
	else if (keyPressed == 2)
	{
		glRotatef(angleOfYRotation, 0.0f, 1.0f, 0.0f);
		LightPosition[2] = angleOfYRotation;
	}
	else if (keyPressed == 3)
	{
		glRotatef(angleOfZRotation, 0.0f, 0.0f, 1.0f);
		LightPosition[0] = angleOfZRotation;
	}

	glLightfv(GL_LIGHT0, GL_POSITION, LightPosition);
	draw24Spheres();

    glXSwapBuffers(gpDisplay, gWindow);
}

void draw24Spheres()
{
	//Code
	GLfloat MaterialAmbient[4];
	GLfloat MaterialDiffuse[4];
	GLfloat MaterialSpecular[4];
	GLfloat MaterialShininess[1];

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

	/* Apply material values for each sphere*/

	/* ------------------ COLUMN 1 STARTS HERE --------------------------*/

	/* SPHERE 1 STARTS HERE	*/
	MaterialAmbient[0] = 0.0215;	//R
	MaterialAmbient[1] = 0.1745;	//G
	MaterialAmbient[2] = 0.0215;	//B
	MaterialAmbient[3] = 1.0f;		//A
	glMaterialfv(GL_FRONT, GL_AMBIENT, MaterialAmbient);

	MaterialDiffuse[0] = 0.07568;	//R
	MaterialDiffuse[1] = 0.61424;	//G
	MaterialDiffuse[2] = 0.07568;	//B
	MaterialDiffuse[3] = 1.0f;		//A
	glMaterialfv(GL_FRONT, GL_DIFFUSE, MaterialDiffuse);

	MaterialSpecular[0] = 0.633;	//R
	MaterialSpecular[1] = 0.727811;	//G
	MaterialSpecular[2] = 0.633;	//B
	MaterialSpecular[3] = 1.0f;		//A
	glMaterialfv(GL_FRONT, GL_SPECULAR, MaterialSpecular);

	MaterialShininess[0] = 0.6 * 128;
	glMaterialfv(GL_FRONT, GL_SHININESS, MaterialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(1.5f, 14.0f, 0.0f);	// This will change for every sphere; since we are in ortho,range of screen is from 0 to 15.5
	gluSphere(quadric[0], 1.0f, 30, 30);
	/* SPHERE 1 ENDS HERE */

	/* SPHERE 2 STARTS HERE	*/
	MaterialAmbient[0] = 0.135;	//R
	MaterialAmbient[1] = 0.2225;	//G
	MaterialAmbient[2] = 0.1575;	//B
	MaterialAmbient[3] = 1.0f;		//A
	glMaterialfv(GL_FRONT, GL_AMBIENT, MaterialAmbient);

	MaterialDiffuse[0] = 0.54;	//R
	MaterialDiffuse[1] = 0.89;	//G
	MaterialDiffuse[2] = 0.63;	//B
	MaterialDiffuse[3] = 1.0f;		//A
	glMaterialfv(GL_FRONT, GL_DIFFUSE, MaterialDiffuse);

	MaterialSpecular[0] = 0.316228;	//R
	MaterialSpecular[1] = 0.316228;	//G
	MaterialSpecular[2] = 0.316228;	//B
	MaterialSpecular[3] = 1.0f;		//A
	glMaterialfv(GL_FRONT, GL_SPECULAR, MaterialSpecular);

	MaterialShininess[0] = 0.1 * 128;
	glMaterialfv(GL_FRONT, GL_SHININESS, MaterialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(1.5f, 11.5f, 0.0f);	// This will change for every sphere; since we are in ortho,range of screen is from 0 to 15.5
	gluSphere(quadric[1], 1.0f, 30, 30);
	/* SPHERE 2 ENDS HERE */

	/* SPHERE 3 STARTS HERE	*/
	MaterialAmbient[0] = 0.05375;	//R
	MaterialAmbient[1] = 0.05;	//G
	MaterialAmbient[2] = 0.06625;	//B
	MaterialAmbient[3] = 1.0f;		//A
	glMaterialfv(GL_FRONT, GL_AMBIENT, MaterialAmbient);

	MaterialDiffuse[0] = 0.18275;	//R
	MaterialDiffuse[1] = 0.17;	//G
	MaterialDiffuse[2] = 0.22525;	//B
	MaterialDiffuse[3] = 1.0f;		//A
	glMaterialfv(GL_FRONT, GL_DIFFUSE, MaterialDiffuse);

	MaterialSpecular[0] = 0.332741;	//R
	MaterialSpecular[1] = 0.328634;	//G
	MaterialSpecular[2] = 0.346435;	//B
	MaterialSpecular[3] = 1.0f;		//A
	glMaterialfv(GL_FRONT, GL_SPECULAR, MaterialSpecular);

	MaterialShininess[0] = 0.3 * 128;
	glMaterialfv(GL_FRONT, GL_SHININESS, MaterialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(1.5f, 9.0f, 0.0f);	// This will change for every sphere; since we are in ortho,range of screen is from 0 to 15.5
	gluSphere(quadric[2], 1.0f, 30, 30);
	/* SPHERE 3 ENDS HERE */

	/* SPHERE 4 STARTS HERE	*/
	MaterialAmbient[0] = 0.25;	//R
	MaterialAmbient[1] = 0.20725;	//G
	MaterialAmbient[2] = 0.20725;	//B
	MaterialAmbient[3] = 1.0f;		//A
	glMaterialfv(GL_FRONT, GL_AMBIENT, MaterialAmbient);

	MaterialDiffuse[0] = 1.0;	//R
	MaterialDiffuse[1] = 0.829;	//G
	MaterialDiffuse[2] = 0.829;	//B
	MaterialDiffuse[3] = 1.0f;		//A
	glMaterialfv(GL_FRONT, GL_DIFFUSE, MaterialDiffuse);

	MaterialSpecular[0] = 0.296648;	//R
	MaterialSpecular[1] = 0.296648;	//G
	MaterialSpecular[2] = 0.296648;	//B
	MaterialSpecular[3] = 1.0f;		//A
	glMaterialfv(GL_FRONT, GL_SPECULAR, MaterialSpecular);

	MaterialShininess[0] = 0.088 * 128;
	glMaterialfv(GL_FRONT, GL_SHININESS, MaterialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(1.5f, 6.5f, 0.0f);	// This will change for every sphere; since we are in ortho,range of screen is from 0 to 15.5
	gluSphere(quadric[3], 1.0f, 30, 30);
	/* SPHERE 4 ENDS HERE */

	/* SPHERE 5 STARTS HERE	*/
	MaterialAmbient[0] = 0.1745;	//R
	MaterialAmbient[1] = 0.01175;	//G
	MaterialAmbient[2] = 0.01175;	//B
	MaterialAmbient[3] = 1.0f;		//A
	glMaterialfv(GL_FRONT, GL_AMBIENT, MaterialAmbient);

	MaterialDiffuse[0] = 0.61424;	//R
	MaterialDiffuse[1] = 0.04136;	//G
	MaterialDiffuse[2] = 0.04136;	//B
	MaterialDiffuse[3] = 1.0f;		//A
	glMaterialfv(GL_FRONT, GL_DIFFUSE, MaterialDiffuse);

	MaterialSpecular[0] = 0.727811;	//R
	MaterialSpecular[1] = 0.626959;	//G
	MaterialSpecular[2] = 0.626959;	//B
	MaterialSpecular[3] = 1.0f;		//A
	glMaterialfv(GL_FRONT, GL_SPECULAR, MaterialSpecular);

	MaterialShininess[0] = 0.6 * 128;
	glMaterialfv(GL_FRONT, GL_SHININESS, MaterialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(1.5f, 4.0f, 0.0f);	// This will change for every sphere; since we are in ortho,range of screen is from 0 to 15.5
	gluSphere(quadric[4], 1.0f, 30, 30);
	/* SPHERE 5 ENDS HERE */

	/* SPHERE 6 STARTS HERE	*/
	MaterialAmbient[0] = 0.1;		//R
	MaterialAmbient[1] = 0.18725;	//G
	MaterialAmbient[2] = 0.1745;	//B
	MaterialAmbient[3] = 1.0f;		//A
	glMaterialfv(GL_FRONT, GL_AMBIENT, MaterialAmbient);

	MaterialDiffuse[0] = 0.396;	//R
	MaterialDiffuse[1] = 0.74151;	//G
	MaterialDiffuse[2] = 0.69102;	//B
	MaterialDiffuse[3] = 1.0f;		//A
	glMaterialfv(GL_FRONT, GL_DIFFUSE, MaterialDiffuse);

	MaterialSpecular[0] = 0.297254;	//R
	MaterialSpecular[1] = 0.30829;	//G
	MaterialSpecular[2] = 0.306678;	//B
	MaterialSpecular[3] = 1.0f;		//A
	glMaterialfv(GL_FRONT, GL_SPECULAR, MaterialSpecular);

	MaterialShininess[0] = 0.6 * 128;
	glMaterialfv(GL_FRONT, GL_SHININESS, MaterialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(1.5f, 1.5f, 0.0f);	// This will change for every sphere; since we are in ortho,range of screen is from 0 to 15.5
	gluSphere(quadric[5], 1.0f, 30, 30);
	/* SPHERE 6 ENDS HERE */
	/*----------------- COLUMN 1 ENDS HERE-----------------*/


	/* ------------------ COLUMN 2 STARTS HERE --------------------------*/

	/* SPHERE 1 STARTS HERE	*/
	MaterialAmbient[0] = 0.329412;	//R
	MaterialAmbient[1] = 0.223529;	//G
	MaterialAmbient[2] = 0.027451;	//B
	MaterialAmbient[3] = 1.0f;		//A
	glMaterialfv(GL_FRONT, GL_AMBIENT, MaterialAmbient);

	MaterialDiffuse[0] = 0.780392;	//R
	MaterialDiffuse[1] = 0.568627;	//G
	MaterialDiffuse[2] = 0.113725;	//B
	MaterialDiffuse[3] = 1.0f;		//A
	glMaterialfv(GL_FRONT, GL_DIFFUSE, MaterialDiffuse);

	MaterialSpecular[0] = 0.992157;	//R
	MaterialSpecular[1] = 0.941176;	//G
	MaterialSpecular[2] = 0.807843;	//B
	MaterialSpecular[3] = 1.0f;		//A
	glMaterialfv(GL_FRONT, GL_SPECULAR, MaterialSpecular);

	MaterialShininess[0] = 0.21797842 * 128;
	glMaterialfv(GL_FRONT, GL_SHININESS, MaterialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(9.0f, 14.0f, 0.0f);	// This will change for every sphere; since we are in ortho,range of screen is from 0 to 15.5
	gluSphere(quadric[6], 1.0f, 30, 30);
	/* SPHERE 1 ENDS HERE */

	/* SPHERE 2 STARTS HERE	*/
	MaterialAmbient[0] = 0.2125;	//R
	MaterialAmbient[1] = 0.1275;	//G
	MaterialAmbient[2] = 0.054;	//B
	MaterialAmbient[3] = 1.0f;		//A
	glMaterialfv(GL_FRONT, GL_AMBIENT, MaterialAmbient);

	MaterialDiffuse[0] = 0.714;	//R
	MaterialDiffuse[1] = 0.4284;	//G
	MaterialDiffuse[2] = 0.18144;	//B
	MaterialDiffuse[3] = 1.0f;		//A
	glMaterialfv(GL_FRONT, GL_DIFFUSE, MaterialDiffuse);

	MaterialSpecular[0] = 0.393548;	//R
	MaterialSpecular[1] = 0.271906;	//G
	MaterialSpecular[2] = 0.166721;	//B
	MaterialSpecular[3] = 1.0f;		//A
	glMaterialfv(GL_FRONT, GL_SPECULAR, MaterialSpecular);

	MaterialShininess[0] = 0.2 * 128;
	glMaterialfv(GL_FRONT, GL_SHININESS, MaterialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(9.0f, 11.5f, 0.0f);	// This will change for every sphere; since we are in ortho,range of screen is from 0 to 15.5
	gluSphere(quadric[7], 1.0f, 30, 30);
	/* SPHERE 2 ENDS HERE */

	/* SPHERE 3 STARTS HERE	*/
	MaterialAmbient[0] = 0.25;	//R
	MaterialAmbient[1] = 0.25;	//G
	MaterialAmbient[2] = 0.25;	//B
	MaterialAmbient[3] = 1.0f;		//A
	glMaterialfv(GL_FRONT, GL_AMBIENT, MaterialAmbient);

	MaterialDiffuse[0] = 0.4;	//R
	MaterialDiffuse[1] = 0.4;	//G
	MaterialDiffuse[2] = 0.4;	//B
	MaterialDiffuse[3] = 1.0f;		//A
	glMaterialfv(GL_FRONT, GL_DIFFUSE, MaterialDiffuse);

	MaterialSpecular[0] = 0.774597;	//R
	MaterialSpecular[1] = 0.774597;	//G
	MaterialSpecular[2] = 0.774597;	//B
	MaterialSpecular[3] = 1.0f;		//A
	glMaterialfv(GL_FRONT, GL_SPECULAR, MaterialSpecular);

	MaterialShininess[0] = 0.6 * 128;
	glMaterialfv(GL_FRONT, GL_SHININESS, MaterialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(9.0f, 9.0f, 0.0f);	// This will change for every sphere; since we are in ortho,range of screen is from 0 to 15.5
	gluSphere(quadric[8], 1.0f, 30, 30);
	/* SPHERE 3 ENDS HERE */

	/* SPHERE 4 STARTS HERE	*/
	MaterialAmbient[0] = 0.19125;	//R
	MaterialAmbient[1] = 0.0735;	//G
	MaterialAmbient[2] = 0.0225;	//B
	MaterialAmbient[3] = 1.0f;		//A
	glMaterialfv(GL_FRONT, GL_AMBIENT, MaterialAmbient);

	MaterialDiffuse[0] = 0.7038;	//R
	MaterialDiffuse[1] = 0.27048;	//G
	MaterialDiffuse[2] = 0.0828;	//B
	MaterialDiffuse[3] = 1.0f;		//A
	glMaterialfv(GL_FRONT, GL_DIFFUSE, MaterialDiffuse);

	MaterialSpecular[0] = 0.256777;	//R
	MaterialSpecular[1] = 0.137622;	//G
	MaterialSpecular[2] = 0.086014;	//B
	MaterialSpecular[3] = 1.0f;		//A
	glMaterialfv(GL_FRONT, GL_SPECULAR, MaterialSpecular);

	MaterialShininess[0] = 0.1 * 128;
	glMaterialfv(GL_FRONT, GL_SHININESS, MaterialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(9.0f, 6.5f, 0.0f);	// This will change for every sphere; since we are in ortho,range of screen is from 0 to 15.5
	gluSphere(quadric[9], 1.0f, 30, 30);
	/* SPHERE 4 ENDS HERE */

	/* SPHERE 5 STARTS HERE	*/
	MaterialAmbient[0] = 0.24725;	//R
	MaterialAmbient[1] = 0.1995;	//G
	MaterialAmbient[2] = 0.0745;	//B
	MaterialAmbient[3] = 1.0f;		//A
	glMaterialfv(GL_FRONT, GL_AMBIENT, MaterialAmbient);

	MaterialDiffuse[0] = 0.75164;	//R
	MaterialDiffuse[1] = 0.60648;	//G
	MaterialDiffuse[2] = 0.22648;	//B
	MaterialDiffuse[3] = 1.0f;		//A
	glMaterialfv(GL_FRONT, GL_DIFFUSE, MaterialDiffuse);

	MaterialSpecular[0] = 0.628281;	//R
	MaterialSpecular[1] = 0.555802;	//G
	MaterialSpecular[2] = 0.366065;	//B
	MaterialSpecular[3] = 1.0f;		//A
	glMaterialfv(GL_FRONT, GL_SPECULAR, MaterialSpecular);

	MaterialShininess[0] = 0.4 * 128;
	glMaterialfv(GL_FRONT, GL_SHININESS, MaterialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(9.0f, 4.0f, 0.0f);	// This will change for every sphere; since we are in ortho,range of screen is from 0 to 15.5
	gluSphere(quadric[10], 1.0f, 30, 30);
	/* SPHERE 5 ENDS HERE */

	/* SPHERE 6 STARTS HERE	*/
	MaterialAmbient[0] = 0.19225;		//R
	MaterialAmbient[1] = 0.19225;	//G
	MaterialAmbient[2] = 0.19225;	//B
	MaterialAmbient[3] = 1.0f;		//A
	glMaterialfv(GL_FRONT, GL_AMBIENT, MaterialAmbient);

	MaterialDiffuse[0] = 0.50754;	//R
	MaterialDiffuse[1] = 0.50754;	//G
	MaterialDiffuse[2] = 0.50754;	//B
	MaterialDiffuse[3] = 1.0f;		//A
	glMaterialfv(GL_FRONT, GL_DIFFUSE, MaterialDiffuse);

	MaterialSpecular[0] = 0.508273;	//R
	MaterialSpecular[1] = 0.508273;	//G
	MaterialSpecular[2] = 0.508273;	//B
	MaterialSpecular[3] = 1.0f;		//A
	glMaterialfv(GL_FRONT, GL_SPECULAR, MaterialSpecular);

	MaterialShininess[0] = 0.4 * 128;
	glMaterialfv(GL_FRONT, GL_SHININESS, MaterialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(9.0f, 1.5f, 0.0f);	// This will change for every sphere; since we are in ortho,range of screen is from 0 to 15.5
	gluSphere(quadric[11], 1.0f, 30, 30);
	/* SPHERE 6 ENDS HERE */
	/*----------------- COLUMN 2 ENDS HERE-----------------*/

	/* ------------------ COLUMN 3 STARTS HERE --------------------------*/

	/* SPHERE 1 STARTS HERE	*/
	MaterialAmbient[0] = 0.0;	//R
	MaterialAmbient[1] = 0.0;	//G
	MaterialAmbient[2] = 0.0;	//B
	MaterialAmbient[3] = 1.0f;		//A
	glMaterialfv(GL_FRONT, GL_AMBIENT, MaterialAmbient);

	MaterialDiffuse[0] = 0.01;	//R
	MaterialDiffuse[1] = 0.01;	//G
	MaterialDiffuse[2] = 0.01;	//B
	MaterialDiffuse[3] = 1.0f;		//A
	glMaterialfv(GL_FRONT, GL_DIFFUSE, MaterialDiffuse);

	MaterialSpecular[0] = 0.50;	//R
	MaterialSpecular[1] = 0.50;	//G
	MaterialSpecular[2] = 0.50;	//B
	MaterialSpecular[3] = 1.0f;		//A
	glMaterialfv(GL_FRONT, GL_SPECULAR, MaterialSpecular);

	MaterialShininess[0] = 0.25 * 128;
	glMaterialfv(GL_FRONT, GL_SHININESS, MaterialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(16.5f, 14.0f, 0.0f);	// This will change for every sphere; since we are in ortho,range of screen is from 0 to 15.5
	gluSphere(quadric[12], 1.0f, 30, 30);
	/* SPHERE 1 ENDS HERE */

	/* SPHERE 2 STARTS HERE	*/
	MaterialAmbient[0] = 0.0;	//R
	MaterialAmbient[1] = 0.1;	//G
	MaterialAmbient[2] = 0.06;	//B
	MaterialAmbient[3] = 1.0f;		//A
	glMaterialfv(GL_FRONT, GL_AMBIENT, MaterialAmbient);

	MaterialDiffuse[0] = 0.0;	//R
	MaterialDiffuse[1] = 0.50980392;	//G
	MaterialDiffuse[2] = 0.50980392;	//B
	MaterialDiffuse[3] = 1.0f;		//A
	glMaterialfv(GL_FRONT, GL_DIFFUSE, MaterialDiffuse);

	MaterialSpecular[0] = 0.50196078;	//R
	MaterialSpecular[1] = 0.50196078;	//G
	MaterialSpecular[2] = 0.50196078;	//B
	MaterialSpecular[3] = 1.0f;		//A
	glMaterialfv(GL_FRONT, GL_SPECULAR, MaterialSpecular);

	MaterialShininess[0] = 0.25 * 128;
	glMaterialfv(GL_FRONT, GL_SHININESS, MaterialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(16.5f, 11.5f, 0.0f);	// This will change for every sphere; since we are in ortho,range of screen is from 0 to 15.5
	gluSphere(quadric[13], 1.0f, 30, 30);
	/* SPHERE 2 ENDS HERE */

	/* SPHERE 3 STARTS HERE	*/
	MaterialAmbient[0] = 0.0;	//R
	MaterialAmbient[1] = 0.0;	//G
	MaterialAmbient[2] = 0.0;	//B
	MaterialAmbient[3] = 1.0f;		//A
	glMaterialfv(GL_FRONT, GL_AMBIENT, MaterialAmbient);

	MaterialDiffuse[0] = 0.1;	//R
	MaterialDiffuse[1] = 0.35;	//G
	MaterialDiffuse[2] = 0.1;	//B
	MaterialDiffuse[3] = 1.0f;		//A
	glMaterialfv(GL_FRONT, GL_DIFFUSE, MaterialDiffuse);

	MaterialSpecular[0] = 0.45;	//R
	MaterialSpecular[1] = 0.55;	//G
	MaterialSpecular[2] = 0.45;	//B
	MaterialSpecular[3] = 1.0f;		//A
	glMaterialfv(GL_FRONT, GL_SPECULAR, MaterialSpecular);

	MaterialShininess[0] = 0.25 * 128;
	glMaterialfv(GL_FRONT, GL_SHININESS, MaterialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(16.5f, 9.0f, 0.0f);	// This will change for every sphere; since we are in ortho,range of screen is from 0 to 15.5
	gluSphere(quadric[14], 1.0f, 30, 30);
	/* SPHERE 3 ENDS HERE */

	/* SPHERE 4 STARTS HERE	*/
	MaterialAmbient[0] = 0.0;	//R
	MaterialAmbient[1] = 0.0;	//G
	MaterialAmbient[2] = 0.0;	//B
	MaterialAmbient[3] = 1.0f;		//A
	glMaterialfv(GL_FRONT, GL_AMBIENT, MaterialAmbient);

	MaterialDiffuse[0] = 0.5;	//R
	MaterialDiffuse[1] = 0.0;	//G
	MaterialDiffuse[2] = 0.0;	//B
	MaterialDiffuse[3] = 1.0f;		//A
	glMaterialfv(GL_FRONT, GL_DIFFUSE, MaterialDiffuse);

	MaterialSpecular[0] = 0.7;	//R
	MaterialSpecular[1] = 0.6;	//G
	MaterialSpecular[2] = 0.6;	//B
	MaterialSpecular[3] = 1.0f;		//A
	glMaterialfv(GL_FRONT, GL_SPECULAR, MaterialSpecular);

	MaterialShininess[0] = 0.25 * 128;
	glMaterialfv(GL_FRONT, GL_SHININESS, MaterialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(16.5f, 6.5f, 0.0f);	// This will change for every sphere; since we are in ortho,range of screen is from 0 to 15.5
	gluSphere(quadric[15], 1.0f, 30, 30);
	/* SPHERE 4 ENDS HERE */

	/* SPHERE 5 STARTS HERE	*/
	MaterialAmbient[0] = 0.0;	//R
	MaterialAmbient[1] = 0.0;	//G
	MaterialAmbient[2] = 0.0;	//B
	MaterialAmbient[3] = 1.0f;		//A
	glMaterialfv(GL_FRONT, GL_AMBIENT, MaterialAmbient);

	MaterialDiffuse[0] = 0.55;	//R
	MaterialDiffuse[1] = 0.55;	//G
	MaterialDiffuse[2] = 0.55;	//B
	MaterialDiffuse[3] = 1.0f;		//A
	glMaterialfv(GL_FRONT, GL_DIFFUSE, MaterialDiffuse);

	MaterialSpecular[0] = 0.70;	//R
	MaterialSpecular[1] = 0.70;	//G
	MaterialSpecular[2] = 0.70;	//B
	MaterialSpecular[3] = 1.0f;		//A
	glMaterialfv(GL_FRONT, GL_SPECULAR, MaterialSpecular);

	MaterialShininess[0] = 0.25 * 128;
	glMaterialfv(GL_FRONT, GL_SHININESS, MaterialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(16.5f, 4.0f, 0.0f);	// This will change for every sphere; since we are in ortho,range of screen is from 0 to 15.5
	gluSphere(quadric[16], 1.0f, 30, 30);
	/* SPHERE 5 ENDS HERE */

	/* SPHERE 6 STARTS HERE	*/
	MaterialAmbient[0] = 0.0;		//R
	MaterialAmbient[1] = 0.0;	//G
	MaterialAmbient[2] = 0.0;	//B
	MaterialAmbient[3] = 1.0f;		//A
	glMaterialfv(GL_FRONT, GL_AMBIENT, MaterialAmbient);

	MaterialDiffuse[0] = 0.5;	//R
	MaterialDiffuse[1] = 0.5;	//G
	MaterialDiffuse[2] = 0.0;	//B
	MaterialDiffuse[3] = 1.0f;		//A
	glMaterialfv(GL_FRONT, GL_DIFFUSE, MaterialDiffuse);

	MaterialSpecular[0] = 0.60;	//R
	MaterialSpecular[1] = 0.60;	//G
	MaterialSpecular[2] = 0.50;	//B
	MaterialSpecular[3] = 1.0f;		//A
	glMaterialfv(GL_FRONT, GL_SPECULAR, MaterialSpecular);

	MaterialShininess[0] = 0.25 * 128;
	glMaterialfv(GL_FRONT, GL_SHININESS, MaterialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(16.5f, 1.5f, 0.0f);	// This will change for every sphere; since we are in ortho,range of screen is from 0 to 15.5
	gluSphere(quadric[17], 1.0f, 30, 30);
	/* SPHERE 6 ENDS HERE */
	/*----------------- COLUMN 3 ENDS HERE-----------------*/

	/* ------------------ COLUMN 4 STARTS HERE --------------------------*/

	/* SPHERE 1 STARTS HERE	*/
	MaterialAmbient[0] = 0.02;	//R
	MaterialAmbient[1] = 0.02;	//G
	MaterialAmbient[2] = 0.02;	//B
	MaterialAmbient[3] = 1.0f;		//A
	glMaterialfv(GL_FRONT, GL_AMBIENT, MaterialAmbient);

	MaterialDiffuse[0] = 0.01;	//R
	MaterialDiffuse[1] = 0.01;	//G
	MaterialDiffuse[2] = 0.01;	//B
	MaterialDiffuse[3] = 1.0f;		//A
	glMaterialfv(GL_FRONT, GL_DIFFUSE, MaterialDiffuse);

	MaterialSpecular[0] = 0.4;	//R
	MaterialSpecular[1] = 0.4;	//G
	MaterialSpecular[2] = 0.4;	//B
	MaterialSpecular[3] = 1.0f;		//A
	glMaterialfv(GL_FRONT, GL_SPECULAR, MaterialSpecular);

	MaterialShininess[0] = 0.078125 * 128;
	glMaterialfv(GL_FRONT, GL_SHININESS, MaterialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(24.0f, 14.0f, 0.0f);	// This will change for every sphere; since we are in ortho,range of screen is from 0 to 15.5
	gluSphere(quadric[18], 1.0f, 30, 30);
	/* SPHERE 1 ENDS HERE */

	/* SPHERE 2 STARTS HERE	*/
	MaterialAmbient[0] = 0.0;	//R
	MaterialAmbient[1] = 0.05;	//G
	MaterialAmbient[2] = 0.05;	//B
	MaterialAmbient[3] = 1.0f;		//A
	glMaterialfv(GL_FRONT, GL_AMBIENT, MaterialAmbient);

	MaterialDiffuse[0] = 0.4;	//R
	MaterialDiffuse[1] = 0.5;	//G
	MaterialDiffuse[2] = 0.5;	//B
	MaterialDiffuse[3] = 1.0f;		//A
	glMaterialfv(GL_FRONT, GL_DIFFUSE, MaterialDiffuse);

	MaterialSpecular[0] = 0.04;	//R
	MaterialSpecular[1] = 0.7;	//G
	MaterialSpecular[2] = 0.7;	//B
	MaterialSpecular[3] = 1.0f;		//A
	glMaterialfv(GL_FRONT, GL_SPECULAR, MaterialSpecular);

	MaterialShininess[0] = 0.078125 * 128;
	glMaterialfv(GL_FRONT, GL_SHININESS, MaterialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(24.0f, 11.5f, 0.0f);	// This will change for every sphere; since we are in ortho,range of screen is from 0 to 15.5
	gluSphere(quadric[19], 1.0f, 30, 30);
	/* SPHERE 2 ENDS HERE */

	/* SPHERE 3 STARTS HERE	*/
	MaterialAmbient[0] = 0.0;	//R
	MaterialAmbient[1] = 0.05;	//G
	MaterialAmbient[2] = 0.0;	//B
	MaterialAmbient[3] = 1.0f;		//A
	glMaterialfv(GL_FRONT, GL_AMBIENT, MaterialAmbient);

	MaterialDiffuse[0] = 0.4;	//R
	MaterialDiffuse[1] = 0.5;	//G
	MaterialDiffuse[2] = 0.4;	//B
	MaterialDiffuse[3] = 1.0f;		//A
	glMaterialfv(GL_FRONT, GL_DIFFUSE, MaterialDiffuse);

	MaterialSpecular[0] = 0.04;	//R
	MaterialSpecular[1] = 0.7;	//G
	MaterialSpecular[2] = 0.04;	//B
	MaterialSpecular[3] = 1.0f;		//A
	glMaterialfv(GL_FRONT, GL_SPECULAR, MaterialSpecular);

	MaterialShininess[0] = 0.078125 * 128;
	glMaterialfv(GL_FRONT, GL_SHININESS, MaterialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(24.0f, 9.0f, 0.0f);	// This will change for every sphere; since we are in ortho,range of screen is from 0 to 15.5
	gluSphere(quadric[20], 1.0f, 30, 30);
	/* SPHERE 3 ENDS HERE */

	/* SPHERE 4 STARTS HERE	*/
	MaterialAmbient[0] = 0.05;	//R
	MaterialAmbient[1] = 0.0;	//G
	MaterialAmbient[2] = 0.0;	//B
	MaterialAmbient[3] = 1.0f;		//A
	glMaterialfv(GL_FRONT, GL_AMBIENT, MaterialAmbient);

	MaterialDiffuse[0] = 0.5;	//R
	MaterialDiffuse[1] = 0.4;	//G
	MaterialDiffuse[2] = 0.4;	//B
	MaterialDiffuse[3] = 1.0f;		//A
	glMaterialfv(GL_FRONT, GL_DIFFUSE, MaterialDiffuse);

	MaterialSpecular[0] = 0.7;	//R
	MaterialSpecular[1] = 0.04;	//G
	MaterialSpecular[2] = 0.04;	//B
	MaterialSpecular[3] = 1.0f;		//A
	glMaterialfv(GL_FRONT, GL_SPECULAR, MaterialSpecular);

	MaterialShininess[0] = 0.078125 * 128;
	glMaterialfv(GL_FRONT, GL_SHININESS, MaterialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(24.0f, 6.5f, 0.0f);	// This will change for every sphere; since we are in ortho,range of screen is from 0 to 15.5
	gluSphere(quadric[21], 1.0f, 30, 30);
	/* SPHERE 4 ENDS HERE */

	/* SPHERE 5 STARTS HERE	*/
	MaterialAmbient[0] = 0.05;	//R
	MaterialAmbient[1] = 0.05;	//G
	MaterialAmbient[2] = 0.05;	//B
	MaterialAmbient[3] = 1.0f;		//A
	glMaterialfv(GL_FRONT, GL_AMBIENT, MaterialAmbient);

	MaterialDiffuse[0] = 0.5;	//R
	MaterialDiffuse[1] = 0.5;	//G
	MaterialDiffuse[2] = 0.5;	//B
	MaterialDiffuse[3] = 1.0f;		//A
	glMaterialfv(GL_FRONT, GL_DIFFUSE, MaterialDiffuse);

	MaterialSpecular[0] = 0.7;	//R
	MaterialSpecular[1] = 0.7;	//G
	MaterialSpecular[2] = 0.7;	//B
	MaterialSpecular[3] = 1.0f;		//A
	glMaterialfv(GL_FRONT, GL_SPECULAR, MaterialSpecular);

	MaterialShininess[0] = 0.078125 * 128;
	glMaterialfv(GL_FRONT, GL_SHININESS, MaterialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(24.0f, 4.0f, 0.0f);	// This will change for every sphere; since we are in ortho,range of screen is from 0 to 15.5
	gluSphere(quadric[22], 1.0f, 30, 30);
	/* SPHERE 5 ENDS HERE */

	/* SPHERE 6 STARTS HERE	*/
	MaterialAmbient[0] = 0.05;		//R
	MaterialAmbient[1] = 0.05;	//G
	MaterialAmbient[2] = 0.0;	//B
	MaterialAmbient[3] = 1.0f;		//A
	glMaterialfv(GL_FRONT, GL_AMBIENT, MaterialAmbient);

	MaterialDiffuse[0] = 0.5;	//R
	MaterialDiffuse[1] = 0.5;	//G
	MaterialDiffuse[2] = 0.4;	//B
	MaterialDiffuse[3] = 1.0f;		//A
	glMaterialfv(GL_FRONT, GL_DIFFUSE, MaterialDiffuse);

	MaterialSpecular[0] = 0.7;	//R
	MaterialSpecular[1] = 0.7;	//G
	MaterialSpecular[2] = 0.04;	//B
	MaterialSpecular[3] = 1.0f;		//A
	glMaterialfv(GL_FRONT, GL_SPECULAR, MaterialSpecular);

	MaterialShininess[0] = 0.078125 * 128;
	glMaterialfv(GL_FRONT, GL_SHININESS, MaterialShininess);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(24.0f, 1.5f, 0.0f);	// This will change for every sphere; since we are in ortho,range of screen is from 0 to 15.5
	gluSphere(quadric[23], 1.0f, 30, 30);
	/* SPHERE 6 ENDS HERE */
	/*----------------- COLUMN 4 ENDS HERE-----------------*/

}


void ToggleFullScreen(void){
	//Variable declarations
	Atom wm_state;
	Atom fullscreen;
	XEvent xev = { 0 };

	//Code
	wm_state = XInternAtom(gpDisplay, "_NET_WM_STATE", False);
	memset(&xev, 0, sizeof(xev));

	xev.type = ClientMessage;
	xev.xclient.window = gWindow;
	xev.xclient.message_type = wm_state;
	xev.xclient.format = 32;
	xev.xclient.data.l[0] = bFullscreen ? 0 : 1;

	fullscreen = XInternAtom(gpDisplay, "_NET_WM_STATE_FULLSCREEN", False);
	xev.xclient.data.l[1] = fullscreen;

	XSendEvent(gpDisplay,
			RootWindow(gpDisplay, gpXVisualInfo->screen),
			False,
			StructureNotifyMask,
			&xev);
}

void uninitialize(void){
	//Code
    GLXContext currentGLXContext = glXGetCurrentContext();

    if (currentGLXContext != NULL && currentGLXContext == gGLXContext){
        glXMakeCurrent(gpDisplay, 0, 0);
    }
    
    if (gGLXContext) {
        glXDestroyContext(gpDisplay, gGLXContext);
    }
	if(gWindow)
	{
		XDestroyWindow(gpDisplay, gWindow);
	}

	if(gColormap)
	{
		XFreeColormap(gpDisplay, gColormap);
	}

	if (gpXVisualInfo)
	{
		free(gpXVisualInfo);
		gpXVisualInfo = NULL;
	}

	if(gpDisplay)
	{
		XCloseDisplay(gpDisplay);
		gpDisplay = NULL;
	}
}
