#include<iostream>
#include<stdio.h>
#include<stdlib.h>
#include<memory.h>
#define _USE_MATH_DEFINES 1
#include<math.h>

#include<X11/Xlib.h>
#include<X11/Xutil.h>
#include<X11/XKBlib.h>
#include<X11/keysym.h>

#include<GL/gl.h>
#include<GL/glu.h>
#include<GL/glx.h>      // OpenGL for XWindows

//namespaces
using namespace std;

//global variables declarations

bool bFullscreen = false;
Display *gpDisplay = NULL;
XVisualInfo *gpXVisualInfo = NULL;
Colormap gColormap;
Window gWindow;
int giWindowWidth = 800;
int giWindowHeight = 600;
GLXContext gGLXContext;

float  xVal_Triangle = 4.1f;			// For Animation of triangle from Right bottom
float  yVal_Triangle = -3.3;
float  xVal_Incircle = -3.1f;			// For animation of circle from left bottom
float  yVal_Incircle = -2.3f;
float  yVal_Line = 3.0f;			// For animation of line from top
double dAngle = 0.0f;				// For rotation of triangle

//entry point function
int main(void){
	// function prototypes
	void CreateWindow(void);
	void ToggleFullScreen(void);
	void uninitialize();
    void update(void);

    void initialize(void);
    void resize(int, int);
    void display(void);
    void update(void);

	//Variable declarations
	int winWidth = giWindowWidth;
	int winHeight = giWindowHeight;
    char keys[26];
    bool bDone = false;


	//Code
	CreateWindow();
    initialize();
	//Message Loop
	XEvent event;
	KeySym keysym;

	while(bDone == false)
	{
        while(XPending(gpDisplay))
        {
            XNextEvent(gpDisplay, &event);
            switch(event.type)
            {
                case MapNotify:
                    break;
                case KeyPress:
                    keysym = XkbKeycodeToKeysym(gpDisplay, event.xkey.keycode, 0, 0);
                    switch(keysym)
                    {
                        case XK_Escape:
                            bDone = true;
                        default:
                            break;
                    }
                    XLookupString(&event.xkey, keys, sizeof(keys), NULL, NULL);
                    switch (keys[0])
                    {
                        case 'F':
                        case 'f':
                            if(bFullscreen == false)
                            {
                                ToggleFullScreen();
                                bFullscreen = true;
                            }
                            else
                            {
                                ToggleFullScreen();
                                bFullscreen = false;
                            }
                            break;
                        default:
                            break;
                    }
                    break;
                case ButtonPress:
                    switch(event.xbutton.button)
                    {
                        case 1:
                            break;
                        case 2:
                            break;
                        case 3:
                            break;
                        case 4:
                            break;
                        default:
                            break;
                    }
                    break;
                case MotionNotify:
                    break;
                case ConfigureNotify:
                    winWidth = event.xconfigure.width;
                    winHeight = event.xconfigure.height;
                    resize(winWidth, winHeight);
                    break;
                case Expose:
                    break;
                case DestroyNotify:
                    break;
                case 33:		// window manager message for close button 
                    bDone = true;
                default:
                    break;		
            }
        }
        update();
        display();
    }    
	uninitialize();
	return 0;
}


void CreateWindow(void){
	//Function prototypes
	void uninitialize(void);

	//Variable declarations
	XSetWindowAttributes winAttribs;
	int defaultScreen;
	int defaultDepth;
	int styleMask;

    //OpenGL changes
    static int frameBufferAttributes[] = {  GLX_RGBA,
                                            GLX_DOUBLEBUFFER, True,
                                            GLX_RED_SIZE, 8,
                                            GLX_GREEN_SIZE, 8,
                                            GLX_BLUE_SIZE, 8,
                                            GLX_ALPHA_SIZE, 8,
                                            GLX_DEPTH_SIZE, 24,
                                            None
                                         };

	//Code
	gpDisplay = XOpenDisplay(NULL);
	if (gpDisplay == NULL)
	{
		printf("ERROR : Unable to open X Display. \nExiting Now...\n");
		uninitialize();
		exit(1);
	}

	defaultScreen = XDefaultScreen(gpDisplay);

	defaultDepth = DefaultDepth(gpDisplay, defaultScreen);

	gpXVisualInfo = glXChooseVisual(gpDisplay, defaultScreen, frameBufferAttributes);
	if(gpXVisualInfo == NULL)
	{
		printf("ERROR : Unable to allocate memory for Visual Info. \nExiting Now...\n");
		uninitialize();
		exit(1);	
	}

	winAttribs.border_pixel = 0;
	winAttribs.background_pixmap = 0;
	winAttribs.colormap = XCreateColormap(gpDisplay,
						  RootWindow(gpDisplay, gpXVisualInfo -> screen),
						  gpXVisualInfo -> visual,
						  AllocNone);
	gColormap = winAttribs.colormap;

	winAttribs.background_pixel = BlackPixel(gpDisplay, defaultScreen);
	winAttribs.event_mask = ExposureMask | VisibilityChangeMask |
							ButtonPressMask | KeyPressMask |
							PointerMotionMask | StructureNotifyMask;

	styleMask = CWBorderPixel | CWBackPixel | CWEventMask | CWColormap;

	gWindow = XCreateWindow(gpDisplay,
					RootWindow(gpDisplay, gpXVisualInfo -> screen),
					0,
					0,
					giWindowWidth,
					giWindowHeight,
					0,
					gpXVisualInfo -> depth,
					InputOutput,
					gpXVisualInfo -> visual,
					styleMask,
					&winAttribs);
	if(!gWindow)
	{
		printf("ERROR : Failed to create main window. \nExiting Now...\n");
		uninitialize();
		exit(1);	
	}

	XStoreName(gpDisplay, gWindow, "FFP - XWindow Deathly Hallows");

	Atom windowManagerDelete = XInternAtom(gpDisplay, "WM_DELETE_WINDOW", True);
	XSetWMProtocols(gpDisplay, gWindow, &windowManagerDelete, 1);

	XMapWindow(gpDisplay, gWindow);	
}


void initialize(void){
    //Function declarations
    void uninitialize(void);
    void resize(int, int);
    void ToggleFullScreen(void);
    //Code
    gGLXContext = glXCreateContext( gpDisplay,
                                    gpXVisualInfo,
                                    NULL,
                                    GL_TRUE
                                  );

    glXMakeCurrent(gpDisplay, gWindow, gGLXContext);
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);           // Black background
    resize(giWindowWidth, giWindowHeight);          // This is warm up call to resize()
    ToggleFullScreen();
}

void resize(int width, int height){
    //Code
	if (height == 0)
	{
		height = 1;
	}
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45.0f,
		(GLfloat)width/(GLfloat)height,
		0.1f,
		100.0f);
}

void update(){
    
    //Code
    if (xVal_Triangle >= 0.0f)
	{
		xVal_Triangle = xVal_Triangle - 0.005f;		// Decrease X value of Triangle in glTranslatef()
	}
	if (yVal_Triangle <= 0.0f)
	{
		yVal_Triangle = yVal_Triangle + 0.005f;		// Increase Y value of Triangle in glTranslatef()
	}
	if (xVal_Incircle <= 0.0f)
	{
		xVal_Incircle = xVal_Incircle + 0.005f;		// Increase X value of Incircle in glTranslatef()
	}
	if (yVal_Incircle <= 0.0f)
	{
		yVal_Incircle = yVal_Incircle + 0.005f;		// Increase Y value of Incircle in glTranslatef()
	}

	if (yVal_Line >= 0.0f)
	{
		yVal_Line = yVal_Line - 0.005f;				//Increase X value of Line in glTranslatef()
	}

	if (dAngle <= 360.0f)
	{
		dAngle = dAngle + 0.8f;
	}
	else
	{
		dAngle = 0.0f;
	}
}

void display(){
    
    //Function declaration
	void DrawTriangle();
	void DrawIncircle();
	
	//Code
	glClear(GL_COLOR_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(xVal_Triangle, yVal_Triangle, -6.0f);
	glRotatef(dAngle, 0.0f, 1.0f, 0.0f);	// SPINNING ---- About Y-Axis Rotation
	glLineWidth(3.0f);
	glBegin(GL_LINE_LOOP);
	DrawTriangle();
	glEnd();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(xVal_Incircle, yVal_Incircle, -6.0f);
	glRotatef(dAngle, 0.0f, 1.0f, 0.0f);	// SPINNING ---- About Y-Axis Rotation
	glBegin(GL_LINE_LOOP);
	glColor3f(1.0f, 1.0f, 1.0f);	// White color
	DrawIncircle();												// Incircle call
	glEnd();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(0.0f, yVal_Line, -6.0f);
	glBegin(GL_LINES);
	glColor3f(1.0f, 1.0f, 1.0f);	// White color
	glVertex3f(0.0f, 1.0f, 0.0f);								// Call to line
	glVertex3f(0.0f, -1.0f, 0.0f);
	glEnd();

    glXSwapBuffers(gpDisplay, gWindow);
}

void DrawTriangle()
{
	//Code
	//Triangle
	glColor3f(1.0f, 1.0f, 1.0f);	// White color

	glVertex3f(0.0f, 1.0f, 0.0f);		// a
	glVertex3f(-1.0f, -1.0f, 0.0f);		// b
	glVertex3f(1.0f, -1.0f, 0.0f);		// c

}

void DrawIncircle()
{
	//Variable declaration
	double dSideRight, dSideLeft, dSideBase;
	double Perimeter, AreaOfTriangle;
	double Ox, Oy;
	double RadiusOfIncircle;
	int NumberOfPoints = 10000;

	dSideRight = sqrt(pow((-1 - 0), 2) + pow((-1 - 1), 2));		// Right Side of triangle using Distance formula 

	dSideLeft = sqrt(pow((1 - 0), 2) + pow((-1 - 1), 2));		// Left Side of triangle using Distance formula

	dSideBase = sqrt(pow((1 - (-1)), 2) + pow((-1 - (-1)), 2));	// Base of triangle using Distance formula

	Perimeter = (dSideRight + dSideLeft + dSideBase);		// Perimeter of Triangle
	double SemiPerimeter = Perimeter / 2;
	AreaOfTriangle = 0.5 * 2.0 * 1.0;						// Area of Triangle

	RadiusOfIncircle = (2 * AreaOfTriangle) / SemiPerimeter;		// Radius of Circle..Formula

	Ox = ((dSideBase * 0.0f) + (dSideRight * -1.0f) + (dSideLeft * 1.0f)) / Perimeter;		// Origin X.....Formula 

	Oy = ((dSideBase * 1.0f) + (dSideRight * -1.0f) + (dSideLeft * -1.0f)) / Perimeter;		// Origin Y.....Formula


	for (float angle = 0.0f; angle < 2.0f*M_PI; angle = angle + 0.001f)
	{

		glVertex3f(RadiusOfIncircle * cos(angle) + Ox, RadiusOfIncircle * sin(angle) + Oy, 0.0f);
	}
}


void ToggleFullScreen(void){
	//Variable declarations
	Atom wm_state;
	Atom fullscreen;
	XEvent xev = { 0 };

	//Code
	wm_state = XInternAtom(gpDisplay, "_NET_WM_STATE", False);
	memset(&xev, 0, sizeof(xev));

	xev.type = ClientMessage;
	xev.xclient.window = gWindow;
	xev.xclient.message_type = wm_state;
	xev.xclient.format = 32;
	xev.xclient.data.l[0] = bFullscreen ? 0 : 1;

	fullscreen = XInternAtom(gpDisplay, "_NET_WM_STATE_FULLSCREEN", False);
	xev.xclient.data.l[1] = fullscreen;

	XSendEvent(gpDisplay,
			RootWindow(gpDisplay, gpXVisualInfo->screen),
			False,
			StructureNotifyMask,
			&xev);
}

void uninitialize(void){
	//Code
    GLXContext currentGLXContext = glXGetCurrentContext();

    if (currentGLXContext != NULL && currentGLXContext == gGLXContext){
        glXMakeCurrent(gpDisplay, 0, 0);
    }
    
    if (gGLXContext) {
        glXDestroyContext(gpDisplay, gGLXContext);
    }
	if(gWindow)
	{
		XDestroyWindow(gpDisplay, gWindow);
	}

	if(gColormap)
	{
		XFreeColormap(gpDisplay, gColormap);
	}

	if (gpXVisualInfo)
	{
		free(gpXVisualInfo);
		gpXVisualInfo = NULL;
	}

	if(gpDisplay)
	{
		XCloseDisplay(gpDisplay);
		gpDisplay = NULL;
	}
}
