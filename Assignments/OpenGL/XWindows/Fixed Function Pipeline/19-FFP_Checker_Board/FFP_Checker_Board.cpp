#include<iostream>
#include<stdio.h>
#include<stdlib.h>
#include<memory.h>

#include<X11/Xlib.h>
#include<X11/Xutil.h>
#include<X11/XKBlib.h>
#include<X11/keysym.h>

#include<GL/gl.h>
#include<GL/glu.h>
#include<GL/glx.h>      // OpenGL for XWindows
#include<SOIL/SOIL.h>
#define CHECK_IMAGE_WIDTH 64
#define CHECK_IMAGE_HEIGHT 64

//namespaces
using namespace std;

//global variables declarations
bool bFullscreen = false;
Display *gpDisplay = NULL;
XVisualInfo *gpXVisualInfo = NULL;
Colormap gColormap;
Window gWindow;
int giWindowWidth = 800;
int giWindowHeight = 600;
GLXContext gGLXContext;

//Texture specific global variables
GLubyte check_image[CHECK_IMAGE_WIDTH][CHECK_IMAGE_HEIGHT][4];
GLuint TexImage;

//entry point function
int main(void){
	// function prototypes
	void CreateWindow(void);
	void ToggleFullScreen(void);
	void uninitialize();
    void update(void);

    void initialize(void);
    void resize(int, int);
    void display(void);
    void update(void);

	//Variable declarations
	int winWidth = giWindowWidth;
	int winHeight = giWindowHeight;
    char keys[26];
    bool bDone = false;


	//Code
	CreateWindow();
    initialize();
	//Message Loop
	XEvent event;
	KeySym keysym;

	while(bDone == false)
	{
        while(XPending(gpDisplay))
        {
            XNextEvent(gpDisplay, &event);
            switch(event.type)
            {
                case MapNotify:
                    break;
                case KeyPress:
                    keysym = XkbKeycodeToKeysym(gpDisplay, event.xkey.keycode, 0, 0);
                    switch(keysym)
                    {
                        case XK_Escape:
                            bDone = true;
                        default:
                            break;
                    }
                    XLookupString(&event.xkey, keys, sizeof(keys), NULL, NULL);
                    switch (keys[0])
                    {
                        case 'F':
                        case 'f':
                            if(bFullscreen == false)
                            {
                                ToggleFullScreen();
                                bFullscreen = true;
                            }
                            else
                            {
                                ToggleFullScreen();
                                bFullscreen = false;
                            }
                            break;
                        default:
                            break;
                    }
                    break;
                case ButtonPress:
                    switch(event.xbutton.button)
                    {
                        case 1:
                            break;
                        case 2:
                            break;
                        case 3:
                            break;
                        case 4:
                            break;
                        default:
                            break;
                    }
                    break;
                case MotionNotify:
                    break;
                case ConfigureNotify:
                    winWidth = event.xconfigure.width;
                    winHeight = event.xconfigure.height;
                    resize(winWidth, winHeight);
                    break;
                case Expose:
                    break;
                case DestroyNotify:
                    break;
                case 33:		// window manager message for close button 
                    bDone = true;
                default:
                    break;		
            }
        }
        update();
        display();
    }    
	uninitialize();
	return 0;
}


void CreateWindow(void){
	//Function prototypes
	void uninitialize(void);

	//Variable declarations
	XSetWindowAttributes winAttribs;
	int defaultScreen;
	int defaultDepth;
	int styleMask;

    //OpenGL changes
    static int frameBufferAttributes[] = {  GLX_RGBA,
                                            GLX_DOUBLEBUFFER, True,
                                            GLX_RED_SIZE, 8,
                                            GLX_GREEN_SIZE, 8,
                                            GLX_BLUE_SIZE, 8,
                                            GLX_ALPHA_SIZE, 8,
                                            GLX_DEPTH_SIZE, 24,
                                            None
                                         };

	//Code
	gpDisplay = XOpenDisplay(NULL);
	if (gpDisplay == NULL)
	{
		printf("ERROR : Unable to open X Display. \nExiting Now...\n");
		uninitialize();
		exit(1);
	}

	defaultScreen = XDefaultScreen(gpDisplay);

	defaultDepth = DefaultDepth(gpDisplay, defaultScreen);

	gpXVisualInfo = glXChooseVisual(gpDisplay, defaultScreen, frameBufferAttributes);
	if(gpXVisualInfo == NULL)
	{
		printf("ERROR : Unable to allocate memory for Visual Info. \nExiting Now...\n");
		uninitialize();
		exit(1);	
	}

	winAttribs.border_pixel = 0;
	winAttribs.background_pixmap = 0;
	winAttribs.colormap = XCreateColormap(gpDisplay,
						  RootWindow(gpDisplay, gpXVisualInfo -> screen),
						  gpXVisualInfo -> visual,
						  AllocNone);
	gColormap = winAttribs.colormap;

	winAttribs.background_pixel = BlackPixel(gpDisplay, defaultScreen);
	winAttribs.event_mask = ExposureMask | VisibilityChangeMask |
							ButtonPressMask | KeyPressMask |
							PointerMotionMask | StructureNotifyMask;

	styleMask = CWBorderPixel | CWBackPixel | CWEventMask | CWColormap;

	gWindow = XCreateWindow(gpDisplay,
					RootWindow(gpDisplay, gpXVisualInfo -> screen),
					0,
					0,
					giWindowWidth,
					giWindowHeight,
					0,
					gpXVisualInfo -> depth,
					InputOutput,
					gpXVisualInfo -> visual,
					styleMask,
					&winAttribs);
	if(!gWindow)
	{
		printf("ERROR : Failed to create main window. \nExiting Now...\n");
		uninitialize();
		exit(1);	
	}

	XStoreName(gpDisplay, gWindow, "FFP - XWindow Checker Board");

	Atom windowManagerDelete = XInternAtom(gpDisplay, "WM_DELETE_WINDOW", True);
	XSetWMProtocols(gpDisplay, gWindow, &windowManagerDelete, 1);

	XMapWindow(gpDisplay, gWindow);	
}


void initialize(void){
    //Function declarations
    void uninitialize(void);
    void resize(int, int);
    void ToggleFullScreen(void);
    bool loadTexture(void);
    //Code
    gGLXContext = glXCreateContext( gpDisplay,
                                    gpXVisualInfo,
                                    NULL,
                                    GL_TRUE
                                  );

    glXMakeCurrent(gpDisplay, gWindow, gGLXContext);
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);           // Black background
    
    glEnable(GL_DEPTH_TEST);		// To enable depth test
	glDepthFunc(GL_LEQUAL);	
    glClearDepth(1.0);		// To give existance to depth buffer....3D specific call  (1.0 is the maximum number we are filling depth buffer with)
    

    glShadeModel(GL_SMOOTH);							//To remove aliasing
    glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);	//To remove distortion

    // Enable texture mapping
    glEnable(GL_TEXTURE_2D); 
    loadTexture();

    resize(giWindowWidth, giWindowHeight);          // This is warm up call to resize()
    ToggleFullScreen();
}

bool loadTexture()
{
	//Function declaration
	void MakeCheckImage(void);

	//Code
	MakeCheckImage();
	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
	glGenTextures(1, &TexImage);
	glBindTexture(GL_TEXTURE_2D, TexImage);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexImage2D(GL_TEXTURE_2D,
		0,
		GL_RGBA,
		CHECK_IMAGE_WIDTH,
		CHECK_IMAGE_HEIGHT,
		0,
		GL_RGBA,
		GL_UNSIGNED_BYTE,
		check_image);
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
}


void MakeCheckImage()
{
	//Variable declarations
	int i, j, c;
	//Code
	for ( i = 0; i < CHECK_IMAGE_HEIGHT; i++)
	{
		for ( j = 0; j < CHECK_IMAGE_WIDTH; j++)
		{
			c = (((i & 0x8) == 0) ^ ((j & 0x8) == 0)) * 255;
			check_image[i][j][0] = (GLubyte)c;
			check_image[i][j][1] = (GLubyte)c;
			check_image[i][j][2] = (GLubyte)c;
			check_image[i][j][3] = 255;
		}
	}
}

void resize(int width, int height){
    //Code
	if (height == 0)
	{
		height = 1;
	}
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45.0f,
		(GLfloat)width/(GLfloat)height,
		0.1f,
		100.0f);
}

void update(){
    //code

}

void display(){

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(0.0f, 0.0f, -3.6f);
	
	//glBindTexture(GL_TEXTURE_2D, TexImage);
	glBegin(GL_QUADS);
	
	/*REDBOOK EXPECTS VERTICES TO COUNT FROM LEFT BOTTOM TO RIGHT BOTTOM CLOCKWISE FOR THIS EXAMPLE*/
	
	//1st Quad
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-2.0f, -1.0f, 0.0f);

	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(-2.0f, 1.0f, 0.0f);

	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(0.0f, 1.0f, 0.0f);

	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(0.0f, -1.0f, 0.0f);
	//glEnd();

	//2nd Quad
	
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(1.0f, -1.0f, 0.0f);

	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(1.0f, 1.0f, 0.0f);

	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(2.41421f, 1.0f, -1.41421f);

	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(2.41421f, -1.0f, -1.41421f);

	glEnd();

    glXSwapBuffers(gpDisplay, gWindow);
}




void ToggleFullScreen(void){
	//Variable declarations
	Atom wm_state;
	Atom fullscreen;
	XEvent xev = { 0 };

	//Code
	wm_state = XInternAtom(gpDisplay, "_NET_WM_STATE", False);
	memset(&xev, 0, sizeof(xev));

	xev.type = ClientMessage;
	xev.xclient.window = gWindow;
	xev.xclient.message_type = wm_state;
	xev.xclient.format = 32;
	xev.xclient.data.l[0] = bFullscreen ? 0 : 1;

	fullscreen = XInternAtom(gpDisplay, "_NET_WM_STATE_FULLSCREEN", False);
	xev.xclient.data.l[1] = fullscreen;

	XSendEvent(gpDisplay,
			RootWindow(gpDisplay, gpXVisualInfo->screen),
			False,
			StructureNotifyMask,
			&xev);
}

void uninitialize(void){
	//Code
    GLXContext currentGLXContext = glXGetCurrentContext();

    if (currentGLXContext != NULL && currentGLXContext == gGLXContext){
        glXMakeCurrent(gpDisplay, 0, 0);
    }
    
    if (gGLXContext) {
        glXDestroyContext(gpDisplay, gGLXContext);
    }
	if(gWindow)
	{
		XDestroyWindow(gpDisplay, gWindow);
	}

	if(gColormap)
	{
		XFreeColormap(gpDisplay, gColormap);
	}

	if (gpXVisualInfo)
	{
		free(gpXVisualInfo);
		gpXVisualInfo = NULL;
	}

	if(gpDisplay)
	{
		XCloseDisplay(gpDisplay);
		gpDisplay = NULL;
	}
}
