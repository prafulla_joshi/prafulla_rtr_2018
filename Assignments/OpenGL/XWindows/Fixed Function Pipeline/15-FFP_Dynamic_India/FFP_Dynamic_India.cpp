#include<iostream>
#include<stdio.h>
#include<stdlib.h>
#include<memory.h>
#define _USE_MATH_DEFINES 1
#include<math.h>

#include<X11/Xlib.h>
#include<X11/Xutil.h>
#include<X11/XKBlib.h>
#include<X11/keysym.h>

#include<GL/gl.h>
#include<GL/glu.h>
#include<GL/glx.h>      // OpenGL for XWindows

//namespaces
using namespace std;

//global variables declarations
bool bFullscreen = false;
Display *gpDisplay = NULL;
XVisualInfo *gpXVisualInfo = NULL;
Colormap gColormap;
Window gWindow;
int giWindowWidth = 800;
int giWindowHeight = 600;
GLXContext gGLXContext;

GLfloat First_I_xValue = -2.2f;
GLfloat A_xValue = 2.1f;
GLfloat N_yValue = 1.9f;
GLfloat Second_I_yValue = -1.9f;

GLfloat Saffron_X = 0.0f;
GLfloat Saffron_Y = 0.0f;
GLfloat Saffron_Z = 0.0f;
GLfloat Green_X = 0.0f;
GLfloat Green_Y = 0.0f;
GLfloat Green_Z = 0.0f;

// Following variables are for update functionality of plane and text values
float xValue = 0.0f;
float yValue = 0.0f;
float zValue = 0.0f;
//float angle = 0.0f;
GLfloat leftUpperAngle = M_PI;		// These angles are needed for circular motion of plane
GLfloat leftLowerAngle = M_PI;
GLfloat rightUpperAngle = (3 * M_PI) / 2;
GLfloat rightLowerAngle = M_PI / 2;
float xValue_Plane = -4.5f;
float yValue_Plane = 0.0f;
float rightXValue = 0.0f;
float rightYValue = 0.0f;
float xPlane = 0.0f;

/* Follwing are the color values of TriColor of each plane*/
float Tri_Saffron_X = 1.0f;
float Tri_Saffron_Y = 0.6f;
float Tri_Saffron_Z = 0.2f;
float Tri_White_All = 1.0f;
float Tri_Green_X = 0.0705882f;
float Tri_Green_Y = 0.5333333f;
float Tri_Green_Z = 0.02745098f;
int flag = 0;				// for checking te condition of fading out the tricolor
float Tri_X_Value = -6.15f;		// Used for removing black line on letters which appears after fade out
GLfloat angle = 0.0f;

//entry point function
int main(void){
	// function prototypes
	void CreateWindow(void);
	void ToggleFullScreen(void);
	void uninitialize();
    void update(void);

    void initialize(void);
    void resize(int, int);
    void display(void);
    void update(void);

	//Variable declarations
	int winWidth = giWindowWidth;
	int winHeight = giWindowHeight;
    char keys[26];
    bool bDone = false;


	//Code
	CreateWindow();
    initialize();
	//Message Loop
	XEvent event;
	KeySym keysym;

	while(bDone == false)
	{
        while(XPending(gpDisplay))
        {
            XNextEvent(gpDisplay, &event);
            switch(event.type)
            {
                case MapNotify:
                    break;
                case KeyPress:
                    keysym = XkbKeycodeToKeysym(gpDisplay, event.xkey.keycode, 0, 0);
                    switch(keysym)
                    {
                        case XK_Escape:
                            bDone = true;
                        default:
                            break;
                    }
                    XLookupString(&event.xkey, keys, sizeof(keys), NULL, NULL);
                    switch (keys[0])
                    {
                        case 'F':
                        case 'f':
                            if(bFullscreen == false)
                            {
                                ToggleFullScreen();
                                bFullscreen = true;
                            }
                            else
                            {
                                ToggleFullScreen();
                                bFullscreen = false;
                            }
                            break;
                        default:
                            break;
                    }
                    break;
                case ButtonPress:
                    switch(event.xbutton.button)
                    {
                        case 1:
                            break;
                        case 2:
                            break;
                        case 3:
                            break;
                        case 4:
                            break;
                        default:
                            break;
                    }
                    break;
                case MotionNotify:
                    break;
                case ConfigureNotify:
                    winWidth = event.xconfigure.width;
                    winHeight = event.xconfigure.height;
                    resize(winWidth, winHeight);
                    break;
                case Expose:
                    break;
                case DestroyNotify:
                    break;
                case 33:		// window manager message for close button 
                    bDone = true;
                default:
                    break;		
            }
        }
        update();
        display();
    }    
	uninitialize();
	return 0;
}


void CreateWindow(void){
	//Function prototypes
	void uninitialize(void);

	//Variable declarations
	XSetWindowAttributes winAttribs;
	int defaultScreen;
	int defaultDepth;
	int styleMask;

    //OpenGL changes
    static int frameBufferAttributes[] = {  GLX_RGBA,
                                            GLX_DOUBLEBUFFER, True,
                                            GLX_RED_SIZE, 8,
                                            GLX_GREEN_SIZE, 8,
                                            GLX_BLUE_SIZE, 8,
                                            GLX_ALPHA_SIZE, 8,
                                            GLX_DEPTH_SIZE, 24,
                                            None
                                         };

	//Code
	gpDisplay = XOpenDisplay(NULL);
	if (gpDisplay == NULL)
	{
		printf("ERROR : Unable to open X Display. \nExiting Now...\n");
		uninitialize();
		exit(1);
	}

	defaultScreen = XDefaultScreen(gpDisplay);

	defaultDepth = DefaultDepth(gpDisplay, defaultScreen);

	gpXVisualInfo = glXChooseVisual(gpDisplay, defaultScreen, frameBufferAttributes);
	if(gpXVisualInfo == NULL)
	{
		printf("ERROR : Unable to allocate memory for Visual Info. \nExiting Now...\n");
		uninitialize();
		exit(1);	
	}

	winAttribs.border_pixel = 0;
	winAttribs.background_pixmap = 0;
	winAttribs.colormap = XCreateColormap(gpDisplay,
						  RootWindow(gpDisplay, gpXVisualInfo -> screen),
						  gpXVisualInfo -> visual,
						  AllocNone);
	gColormap = winAttribs.colormap;

	winAttribs.background_pixel = BlackPixel(gpDisplay, defaultScreen);
	winAttribs.event_mask = ExposureMask | VisibilityChangeMask |
							ButtonPressMask | KeyPressMask |
							PointerMotionMask | StructureNotifyMask;

	styleMask = CWBorderPixel | CWBackPixel | CWEventMask | CWColormap;

	gWindow = XCreateWindow(gpDisplay,
					RootWindow(gpDisplay, gpXVisualInfo -> screen),
					0,
					0,
					giWindowWidth,
					giWindowHeight,
					0,
					gpXVisualInfo -> depth,
					InputOutput,
					gpXVisualInfo -> visual,
					styleMask,
					&winAttribs);
	if(!gWindow)
	{
		printf("ERROR : Failed to create main window. \nExiting Now...\n");
		uninitialize();
		exit(1);	
	}

	XStoreName(gpDisplay, gWindow, "FFP - XWindow Dynamic India");

	Atom windowManagerDelete = XInternAtom(gpDisplay, "WM_DELETE_WINDOW", True);
	XSetWMProtocols(gpDisplay, gWindow, &windowManagerDelete, 1);

	XMapWindow(gpDisplay, gWindow);	
}


void initialize(void){
    //Function declarations
    void uninitialize(void);
    void resize(int, int);
    void ToggleFullScreen(void);
    //Code
    gGLXContext = glXCreateContext( gpDisplay,
                                    gpXVisualInfo,
                                    NULL,
                                    GL_TRUE
                                  );

    glXMakeCurrent(gpDisplay, gWindow, gGLXContext);
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);           // Black background
    resize(giWindowWidth, giWindowHeight);          // This is warm up call to resize()
    ToggleFullScreen();
}

void resize(int width, int height){
    //Code
	if (height == 0)
	{
		height = 1;
	}
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45.0f,
		(GLfloat)width/(GLfloat)height,
		0.1f,
		100.0f);
}

void update(){
    //Code
	if (First_I_xValue < -0.9f)
	{
		First_I_xValue = First_I_xValue + 0.002f;		// changing 'x' value for first I in INDIA so that it can animate from left to its position
	}

	if (First_I_xValue >= -0.9f)					// whether 'I' is placed at proper place or not
	{
		if (A_xValue >= 0.7f)
		{
			A_xValue = A_xValue - 0.002f;				// Changing 'x' value of 'A' in INDIA so that it can animate only after first 'I' is animated and positioned properly
		}
	}
	
	if (A_xValue <= 0.7f)						// whether 'A' is placed at proper place or not
	{
		if (N_yValue >= 0.0f)
		{
			N_yValue = N_yValue - 0.002f;		// Changing 'y' value of 'N' in INDIA so that it animates only after 'A' is animated and positioned properly
		}
	}

	if (N_yValue <= 0.0f)						// whether 'N' is placed at proper place or not
	{
		if (Second_I_yValue <= 0.0f)			// Changing 'y' value of second 'I' in INDIA so that it animates only after 'N' is animated and positioned properly
		{
			Second_I_yValue = Second_I_yValue + 0.002f;
		}
	}
	if (Second_I_yValue >= 0.0f)				// whether second 'I' is placed at proper place or not
	{
		//Below code is for fade in of Saffron color of 'D' in INDIA
		if (Saffron_X <= 1.0f)
		{
			Saffron_X = Saffron_X + 0.01f;
		}
		if(Saffron_Y <= 0.6f)
		{
			Saffron_Y = Saffron_Y + 0.01f;
		}
		if (Saffron_Z <= 0.2f)
		{
			Saffron_Z = Saffron_Z + 0.01f;
		}
		//Below code is for fade in of Green color of 'D' in INDIA
		if (Green_X <= 0.0705882f)
		{
			Green_X = Green_X + 0.01f;
		}
		if (Green_Y <= 0.5333333f)
		{
			Green_Y = Green_Y + 0.01f;
		}
		if (Green_Z <= 0.02745098f)
		{
			Green_Z = Green_Z + 0.01f;
		}
	}

	if (flag == 1)
	{
		// Saffron color strip fade out
		if (Tri_Saffron_X >= 0.0)
		{
			Tri_Saffron_X = Tri_Saffron_X - 0.01f;
		}

		if (Tri_Saffron_Y >= 0.0)
		{
			Tri_Saffron_Y = Tri_Saffron_Y - 0.01f;
		}

		if (Tri_Saffron_Z >= 0.0)
		{
			Tri_Saffron_Z = Tri_Saffron_Z - 0.01f;
		}

		// White color strip fade out

		if (Tri_White_All >= 0.0)
		{
			Tri_White_All = Tri_White_All - 0.01f;
		}

		// Green color strip fade out

		if (Tri_Green_X >= 0.0)
		{
			Tri_Green_X = Tri_Green_X - 0.01f;
		}

		if (Tri_Green_Y >= 0.0)
		{
			Tri_Green_Y = Tri_Green_Y - 0.01f;
		}

		if (Tri_Green_Z >= 0.0)
		{
			Tri_Green_Z = Tri_Green_Z - 0.01f;
		}
	}

	if (Tri_Saffron_X < 0.0) /*|| Tri_White_All < 0.0 || Tri_Green_X < 0.0)*/	// Check whether line has been faded out or not
	{
		//Tri_X_Value = 8.3f;
		Tri_X_Value = Tri_X_Value + 0.5;
		xValue_Plane = 4.0f;
	}
}



void display(){
    
    //function declaration
	void Draw_First_I(void);
	void Draw_N(void);
	void Draw_D(void);
	void Draw_Second_I(void);
	void Draw_A(void);
	void Call_Aeroplane(void);
	
	//Code

	glClear(GL_COLOR_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);

	/*glTranslatef(0.0f, 0.0f, -2.5f);*/

	Draw_First_I();
	Draw_N();
	Draw_D();
	Draw_Second_I();
	Draw_A();
	/*---- Following code is for calling aeroplane functionality after animation of D ----*/


	if (Green_X >= 0.0705882f)		// Whether D is animated and placed at its proper position of not
	{
		Call_Aeroplane();
	}
	/*---- Aeroplane functionlity code ends here ----*/

    glXSwapBuffers(gpDisplay, gWindow);
}


void Draw_First_I()
{
	//Code

	glLoadIdentity();
	glTranslatef(First_I_xValue, 0.0f, -2.5f);

	// Following code is for drawing 'I' in INDIA
	glLineWidth(6.0f);
	glBegin(GL_LINES);

	// Below code is for vertical line of I
	glColor3f(1.0f, 0.6f, 0.2f);							// Saffron color, RGB values taken from wiki
	glVertex3f(0.0f, 0.7f, 0.0f);

	glColor3f(0.0705882f, 0.5333333f, 0.02745098f);			// Green color, RGB values taken from wiki
	glVertex3f(0.0f, -0.7f, 0.0f);
	// Vertical line of I ends here
	// 'I' rendered here

	glEnd();
}

void Draw_N()
{
	//Code
	// Following code is for drawing 'N' in INDIA
	glLoadIdentity();
	glTranslatef(-0.55f, N_yValue, -2.5f);
	glBegin(GL_LINES);

	glEnable(GL_BLEND);
	glEnable(GL_LINE_SMOOTH);

	// Below code for right line of N
	glColor3f(1.0f, 0.6f, 0.2f);							// Saffron color, RGB values taken from wiki 
	glVertex3f(0.15f, 0.7f, 0.0f);

	glColor3f(0.0705882f, 0.5333333f, 0.02745098f);			// Green color, RGB values taken from wiki
	glVertex3f(0.15f, -0.7f, 0.0f);
	// Right line of N ends here

	//Below code for middle line of N
	glColor3f(0.0705882f, 0.5333333f, 0.02745098f);			// Green color, RGB values taken from wiki
	glVertex3f(0.15f, -0.7f, 0.0f);

	glColor3f(1.0f, 0.6f, 0.2f);							// Saffron color, RGB values taken from wiki 
	glVertex3f(-0.15f, 0.7f, 0.0f);
	// Middle line of N ends here

	// Below code for left line of N
	glColor3f(1.0f, 0.6f, 0.2f);							// Saffron color, RGB values taken from wiki 
	glVertex3f(-0.15f, 0.7f, 0.0f);

	glColor3f(0.0705882f, 0.5333333f, 0.02745098f);			// Green color, RGB values taken from wiki
	glVertex3f(-0.15f, -0.7f, 0.0f);
	//Left line ends here
	// 'N' rendered
	glEnd();
}

void Draw_D()
{
	//function declaration
	
	//Code
	// Following code is for drawing 'D' in INDIA
	glLoadIdentity();
	glTranslatef(-0.1f, 0.0f, -2.5f);
	glBegin(GL_LINES);

	//Below code for upper horizontal of D
	glColor3f(Saffron_X, Saffron_Y, Saffron_Z);							// Saffron color, RGB values taken from wiki 
	glVertex3f(-0.15f, 0.7f, 0.0f);

	glColor3f(Saffron_X, Saffron_Y, Saffron_Z);							// Saffron color, RGB values taken from wiki 
	glVertex3f(0.15f, 0.7f, 0.0f);
	// Upper horizontal line of D ends here

	// Below code for right vertical of D
	glColor3f(Saffron_X, Saffron_Y, Saffron_Z);							// Saffron color, RGB values taken from wiki 
	glVertex3f(0.15f, 0.7f, 0.0f);

	glColor3f(Green_X, Green_Y, Green_Z);			// Green color, RGB values taken from wiki
	glVertex3f(0.15f, -0.7f, 0.0f);
	// Right vertical line of D ends here

	// Below code for lower horizontal of D
	glColor3f(Green_X, Green_Y, Green_Z);			// Green color, RGB values taken from wiki
	glVertex3f(0.15f, -0.7f, 0.0f);

	glColor3f(Green_X, Green_Y, Green_Z);			// Green color, RGB values taken from wiki
	glVertex3f(-0.15f, -0.7f, 0.0f);

	//Below code is for left vertical line of D
	glColor3f(Green_X, Green_Y, Green_Z);			// Green color, RGB values taken from wiki
	glVertex3f(-0.1f, -0.7f, 0.0f);

	glColor3f(Saffron_X, Saffron_Y, Saffron_Z);							// Saffron color, RGB values taken from wiki 
	glVertex3f(-0.1f, 0.7f, 0.0f);
	//Left vertical line of D ends here
	// 'D' rendered here

	glEnd();

}

void Draw_Second_I()
{
	//Code
	// Following code is for drawing 'I' in INDIA

	glLoadIdentity();
	/*glTranslatef(0.25f, 0.0f, -2.0f);*/	
	glTranslatef(0.25f, Second_I_yValue, -2.5f);
	glLineWidth(6.0f);
	glBegin(GL_LINES);

	// Below code is for vertical line of I
	glColor3f(1.0f, 0.6f, 0.2f);							// Saffron color, RGB values taken from wiki
	glVertex3f(0.0f, 0.7f, 0.0f);

	glColor3f(0.0705882f, 0.5333333f, 0.02745098f);			// Green color, RGB values taken from wiki
	glVertex3f(0.0f, -0.7f, 0.0f);
	// Vertical line of I ends here
	// 'I' rendered here
	glEnd();
}

void Draw_A()
{
	//Code
	// Following code is for 'A' in INDIA

	glLoadIdentity();
	//glTranslatef(0.7f, 0.0f, -2.0f);
	glTranslatef(A_xValue, 0.0f, -2.5f);
	glLineWidth(6.0f);
	glBegin(GL_LINES);

	// Below code is for left slanting line of 'A'
	glColor3f(1.0f, 0.6f, 0.2f);							// Saffron color, RGB values taken from wiki 
	glVertex3f(0.0f, 0.7f, 0.0f);

	glColor3f(0.0705882f, 0.5333333f, 0.02745098f);			// Green color, RGB values taken from wiki
	glVertex3f(-0.25f, -0.7f, 0.0f);
	// left slanting line of 'A' ends here

	// Below code is for left slanting line of 'A'
	glColor3f(1.0f, 0.6f, 0.2f);							// Saffron color, RGB values taken from wiki 
	glVertex3f(0.0f, 0.7f, 0.0f);

	glColor3f(0.0705882f, 0.5333333f, 0.02745098f);			// Green color, RGB values taken from wiki
	glVertex3f(0.25f, -0.7f, 0.0f);
	// left slanting line of 'A' ends here
	glEnd();

}


/*---- This function is for Calling aeroplane after Dynamic INDIA animation ----*/

void Call_Aeroplane()
{
	//Function declaration
	void Draw_Circle(void);
	void Draw_Text(void);
	void Draw_Aeroplane(void);
	void Draw_A_Strip(void);
	void Draw_Tricolor();
	//Code

	// For Upper left plane which is required in Dynamic India Assignment
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(-1.5, 2.0f, -2.5f);
	//glRotatef(90.0f, 0.0f, 0.0f, 1.0f);
	if (leftUpperAngle < (3.0f*(M_PI / 2.0f)))
	{
		xValue = 2.0f * cos(leftUpperAngle);
		yValue = 2.0f * sin(leftUpperAngle);
		glTranslatef(xValue, yValue, -2.5f);
		//glRotatef(angle, 0.0f, 0.0f, 1.0f);
	}
	//glTranslatef(xValue, yValue, -2.5f);	// Commented to remove the plane once it is on X-axis
	Draw_Aeroplane();
	Draw_Text();
	Draw_Tricolor();
	//leftUpperAngle = leftUpperAngle + 0.00031f;
	leftUpperAngle = leftUpperAngle + 0.001f;
	angle = angle - 0.01f;

	/*--------Lower Plane------------*/

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(-1.5f, -2.0f, -2.5f);
	if (leftLowerAngle > (M_PI / 2.0f))
	{
		xValue = 2.0f * cos(leftLowerAngle);
		yValue = 2.0f * sin(leftLowerAngle);
		glTranslatef(xValue, yValue, -2.5f);
	}
	/*glTranslatef(xValue, yValue, -2.5f);*/		// Commented to remove the plane once it is on X-axis
	Draw_Aeroplane();
	Draw_Text();
	Draw_Tricolor();
	//leftLowerAngle = leftLowerAngle - 0.00031f;		//0.0006f;
	leftLowerAngle = leftLowerAngle - 0.001f;
	/*--------Middle Plane------------*/

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(xValue_Plane, 0.0f, -2.5f);
	if (xValue_Plane <= 2.15f)		// hard-coded value, after which 3 planes separates
	{
		//xValue_Plane = xValue_Plane + 0.00074f;		//0.00095f;
		//xValue_Plane = xValue_Plane + 0.000715f;
		xValue_Plane = xValue_Plane + 0.00237f;
        //glTranslatef(xValue_Plane, 0.0f, -2.5f);
	}
	
	glTranslatef(xValue_Plane, 0.0f, -2.5f);
	Draw_Aeroplane();
	Draw_Text();
	Draw_Tricolor();
	if (xValue_Plane > 2.15f)				// Keep the tricolor after plane gone
	{
		//xValue_Plane = 14.0f;
		//glTranslatef(4.0f, 0.0f, -2.5f);		// sustain tricolor; therefore it is given after if block
		//Draw_Tricolor();
		flag = 1;
	}
	

	if (xValue_Plane > 2.15f)
	{
		Draw_A_Strip();
	}

	/*---- Upper Right Plane ----*/
	if (xValue_Plane > 1.15f)		// 1.15 is the value of X where plane separates,hardcoded value after many trials
	{
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(2.3f, 1.6f, -2.5f);		// here values of X and Y are center point of that arc

		if (rightUpperAngle <= (2.0f*M_PI) + 0.5f)
		{
			rightXValue = 1.5f * cos(rightUpperAngle);
			rightYValue = 1.5f * sin(rightUpperAngle);
			glTranslatef(rightXValue, rightYValue, -2.5f);
			Draw_Aeroplane();
			Draw_Text();
			rightUpperAngle = rightUpperAngle + 0.001f;
		}
	}

	/*---- Lower Right Plane ----*/

	if (xValue_Plane > 1.15f)		// 1.15 is the value of X where plane separates,hardcoded value after many trials
	{
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glTranslatef(2.3f, -1.6f, -2.5f);		// here values of X and Y are center point of that arc

		if (rightLowerAngle >= 0.0f)
		{
			rightXValue = 1.5f * cos(rightLowerAngle);
			rightYValue = 1.5f * sin(rightLowerAngle);
			glTranslatef(rightXValue, rightYValue, -2.5f);
			Draw_Aeroplane();
			Draw_Text();
			rightLowerAngle = rightLowerAngle - 0.001f;
		}
	}

}


/*---- This function is for Drawing Aeroplane after Dynamic INDIA animation ----*/
void Draw_Aeroplane()
{
	//Code
	// Middle rectangle of Aeroplane which is required in Dynamic India Assignment

	glBegin(GL_QUADS);
	glColor3f(0.7294117f, 0.8862745f, 0.9333333f);
	glVertex3f(0.45f, 0.18f, 0.0f);

	glColor3f(0.7294117f, 0.8862745f, 0.9333333f);
	glVertex3f(-0.45f, 0.18f, 0.0f);

	glColor3f(0.7294117f, 0.8862745f, 0.9333333f);
	glVertex3f(-0.45f, -0.18f, 0.0f);

	glColor3f(0.7294117f, 0.8862745f, 0.9333333f);
	glVertex3f(0.45f, -0.18f, 0.0f);
	glEnd();
	/*----------Tricolor behind plane starts here-----------*/

	glBegin(GL_LINES);
	//Saffron line
	glColor3f(1.0f, 0.6f, 0.2f);		// Saffron color, RGBs taken from wikipedia
	glVertex3f(-0.45f, 0.05f, 0.0f);

	glColor3f(1.0f, 0.6f, 0.2f);		// Saffron color, RGBs taken from wikipedia
	glVertex3f(-2.5f, 0.05f, 0.0f);

	//White line
	glColor3f(1.0f, 1.0f, 1.0f);		// White color, RGBs taken from wikipedia
	glVertex3f(-0.45f, 0.03f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);		// White color, RGBs taken from wikipedia
	glVertex3f(-2.5f, 0.03f, 0.0f);

	//Green line
	glColor3f(0.0705882f, 0.5333333f, 0.02745098f);		// Green color, RGBs taken from wikipedia
	glVertex3f(-0.45f, 0.0004f, 0.0f);

	glColor3f(0.0705882f, 0.5333333f, 0.02745098f);		// Green color, RGBs taken from wikipedia
	glVertex3f(-2.5f, 0.0004f, 0.0f);

	glEnd();

	/*----------Tricolor behind plane ends here-----------*/
	// Front Triangle of Aeroplane which is required in Dynamic India Assignment

	glBegin(GL_TRIANGLES);
	glColor3f(0.7294117f, 0.8862745f, 0.9333333f);
	glVertex3f(0.45f, 0.18f, 0.0f);

	glColor3f(0.7294117f, 0.8862745f, 0.9333333f);
	glVertex3f(0.45f, -0.18f, 0.0f);

	glColor3f(0.7294117f, 0.8862745f, 0.9333333f);
	glVertex3f(0.8f, -0.18f, 0.0f);

	glEnd();


	// Upper Triangle of Aeroplane which is required in Dynamic India Assignment

	glBegin(GL_TRIANGLES);

	glColor3f(0.7294117f, 0.8862745f, 0.9333333f);
	glVertex3f(-0.45f, 0.18f, 0.0f);

	glColor3f(0.7294117f, 0.8862745f, 0.9333333f);
	glVertex3f(-0.20f, 0.18f, 0.0f);

	glColor3f(0.7294117f, 0.8862745f, 0.9333333f);
	glVertex3f(-0.45f, 0.45f, 0.0f);

	glEnd();

	// Side Triangle of Aeroplane which is required in Dynamic India Assignment

	glBegin(GL_TRIANGLES);

	glColor3f(0.7294117f, 0.8862745f, 0.9333333f);
	glVertex3f(-0.25f, -0.13f, 0.0f);

	glColor3f(0.7294117f, 0.8862745f, 0.9333333f);
	glVertex3f(-0.25f, -0.40f, 0.0f);

	glColor3f(0.7294117f, 0.8862745f, 0.9333333f);
	glVertex3f(0.45f, -0.13f, 0.0f);

	glEnd();

}


/*---- This function is for Drawing Text on Aeroplane after Dynamic INDIA animation ----*/
void Draw_Text()
{
	glLineWidth(5.0f);
	glBegin(GL_LINES);
	// For 'I' in IAF
	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex3f(-0.2f, 0.15f, 0.0f);

	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex3f(-0.2f, -0.15f, 0.0f);

	// For 'A' in IAF
	//Left line of 'A'
	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex3f(0.0f, 0.15f, 0.0f);

	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex3f(-0.12f, -0.15f, 0.0f);
	//Right line of 'A'
	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex3f(0.0f, 0.15f, 0.0f);

	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex3f(0.12f, -0.15f, 0.0f);

	//Middle strip of 'A'
	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex3f(-0.07f, 0.0f, 0.0f);

	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex3f(0.07f, 0.0f, 0.0f);

	// For 'F' in IAF // vertical line of F
	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex3f(0.2f, 0.15f, 0.0f);

	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex3f(0.2f, -0.15f, 0.0f);

	//Upper Line of 'F'
	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex3f(0.2f, 0.15f, 0.0f);

	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex3f(0.35f, 0.15f, 0.0f);

	//Lower Line of 'F'
	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex3f(0.2f, 0.02f, 0.0f);

	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex3f(0.3f, 0.02f, 0.0f);

	glEnd();
}

/* THIS FUNCTION DRAWS STRIP OF A */

void Draw_A_Strip()
{
	//Code
	//Following code is for tri-color strip in 'A'


	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(0.7f, 0.014f, -2.5f);
	glLineWidth(5.0f);
	glBegin(GL_LINES);

	// Below code for saffron colorkk
	glColor3f(1.0f, 0.6f, 0.2f);							// Saffron color, RGB values taken from wiki 
	glVertex3f(-0.123f, 0.012f, 0.0f);

	glColor3f(1.0f, 0.6f, 0.2f);							// Saffron color, RGB values taken from wiki 
	glVertex3f(0.123f, 0.012f, 0.0f);
	//saffron color ends here

	// Below code for white color
	glColor3f(1.0f, 1.0f, 1.0f);							// White color, RGB values taken from wiki 
	glVertex3f(-0.123f, 0.0f, 0.0f);

	glColor3f(1.0f, 1.0f, 1.0f);							// White color, RGB values taken from wiki 
	glVertex3f(0.123f, 0.0f, 0.0f);
	//white color ends here

	// Below code for green color
	glColor3f(0.0705882f, 0.5333333f, 0.02745098f);			// Green color, RGB values taken from wiki 
	glVertex3f(-0.123f, -0.012f, 0.0f);

	glColor3f(0.0705882f, 0.5333333f, 0.02745098f);			// Green color, RGB values taken from wiki 
	glVertex3f(0.123f, -0.012f, 0.0f);
	//green color ends here

	glEnd();
}

/* THIS FUNCTION DRAWS TRI-COLOR BEHIND THE PLANE */

void Draw_Tricolor()
{
	//Code

	/*----------Tricolor behind plane starts here-----------*/

	glBegin(GL_LINES);
	//Saffron line
	glColor3f(Tri_Saffron_X, Tri_Saffron_Y, Tri_Saffron_Z);		// Saffron color, RGBs taken from wikipedia
	glVertex3f(-0.45f, 0.05f, 0.0f);

	glColor3f(Tri_Saffron_X, Tri_Saffron_Y, Tri_Saffron_Z);		// Saffron color, RGBs taken from wikipedia
	glVertex3f(Tri_X_Value, 0.05f, 0.0f);

	//White line
	glColor3f(Tri_White_All, Tri_White_All, Tri_White_All);		// White color, RGBs taken from wikipedia
	glVertex3f(-0.45f, 0.03f, 0.0f);

	glColor3f(Tri_White_All, Tri_White_All, Tri_White_All);		// White color, RGBs taken from wikipedia
	glVertex3f(Tri_X_Value, 0.03f, 0.0f);

	//Green line
	glColor3f(Tri_Green_X, Tri_Green_Y, Tri_Green_Z);		// Green color, RGBs taken from wikipedia
	glVertex3f(-0.45f, 0.0004f, 0.0f);

	glColor3f(Tri_Green_X, Tri_Green_Y, Tri_Green_Z);		// Green color, RGBs taken from wikipedia
	glVertex3f(Tri_X_Value, 0.0004f, 0.0f);

	glEnd();

	/*----------Tricolor behind plane ends here-----------*/

}


void ToggleFullScreen(void){
	//Variable declarations
	Atom wm_state;
	Atom fullscreen;
	XEvent xev = { 0 };

	//Code
	wm_state = XInternAtom(gpDisplay, "_NET_WM_STATE", False);
	memset(&xev, 0, sizeof(xev));

	xev.type = ClientMessage;
	xev.xclient.window = gWindow;
	xev.xclient.message_type = wm_state;
	xev.xclient.format = 32;
	xev.xclient.data.l[0] = bFullscreen ? 0 : 1;

	fullscreen = XInternAtom(gpDisplay, "_NET_WM_STATE_FULLSCREEN", False);
	xev.xclient.data.l[1] = fullscreen;

	XSendEvent(gpDisplay,
			RootWindow(gpDisplay, gpXVisualInfo->screen),
			False,
			StructureNotifyMask,
			&xev);
}

void uninitialize(void){
	//Code
    GLXContext currentGLXContext = glXGetCurrentContext();

    if (currentGLXContext != NULL && currentGLXContext == gGLXContext){
        glXMakeCurrent(gpDisplay, 0, 0);
    }
    
    if (gGLXContext) {
        glXDestroyContext(gpDisplay, gGLXContext);
    }
	if(gWindow)
	{
		XDestroyWindow(gpDisplay, gWindow);
	}

	if(gColormap)
	{
		XFreeColormap(gpDisplay, gColormap);
	}

	if (gpXVisualInfo)
	{
		free(gpXVisualInfo);
		gpXVisualInfo = NULL;
	}

	if(gpDisplay)
	{
		XCloseDisplay(gpDisplay);
		gpDisplay = NULL;
	}
}
