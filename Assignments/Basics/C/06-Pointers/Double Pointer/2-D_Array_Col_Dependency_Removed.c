#include<stdio.h>
int main(void)
{
	// variable declarations
	int *iArray[5] = { NULL };	// iArray can store 5 integer pointers(base address of 5 1-D arrays)
	int col_size;
	// Code

	printf("\n\n");
	printf("How many columns do you want in array ?(integer expected) ");
	scanf("%d", &col_size);

	// allocating memeory for each column using malloc()

	for (int i = 0; i < 5; i++)
	{
		iArray[i] = (int*)malloc(col_size * sizeof(int));
		if (iArray[i] == NULL)
		{
			printf("Memory allocation failed !!\n");
		}
	}
	printf("\n\n");
	printf("Array dimensions are '5 x %d' \n\n",col_size);
	for (int i = 0; i < 5; i++)
	{
		printf("Enter %d integer elements of row %d :\n", col_size,(i + 1));
		for (int j = 0; j < col_size; j++)
		{
			//scanf("%d", &arr[i][j]);
			scanf("%d", &iArray[col_size]);
		}
	}
	//ptr = arr;
	printf("\n\n");
	printf("Array elements are :\n\n");
	for (int i = 0; i < 5; i++)
	{
		for (int j = 0; j < col_size; j++)
		{
			printf("%d\t", *(*(iArray + i) + j));
			//printf("%d\t", *(*(ptr+i)+j));
		}
		printf("\n");
	}
	getch();
	return 0;
}