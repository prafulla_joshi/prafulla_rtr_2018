#include<stdio.h>
int main(void)
{
	// Variable declarations
	int iArray[5][3];
	int i, j;
	// Code
	printf("\n\n");
	printf("Array dimensions are '5 x 3' \n\n");
	for (int i = 0; i < 5; i++)
	{
		printf("Enter 3 integer elements of %d row :\n", (i + 1));
		for (int j = 0; j < 3; j++)
		{
			scanf("%d", &iArray[i][j]);
		}
	}
	printf("\n\n");
	printf("Array elements are :\n\n");
	for (int i = 0; i < 5; i++)
	{
		for (int j = 0; j < 3; j++)
		{
			printf("%d\t", iArray[i][j]);
		}
		printf("\n");
	}
	getch();
	return 0;
}