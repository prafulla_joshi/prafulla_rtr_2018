#include<stdio.h>
#include<stdlib.h>
int main(void)
{
	// Vriable declarations
	int **ptr = NULL;
	int row_size, col_size;
	//Code
	printf("\n\n");
	printf("Enter number of rows :");
	scanf("%d", &row_size);

	printf("Enter number of columns :");
	scanf("%d", &col_size);

		
	ptr = (int**)malloc(row_size * sizeof(int*));
	if (ptr == NULL)
	{
		printf("Memory allocation failed !!");
		exit(0);
	}

	// Dynamic memory allocation to columns of array using malloc()
	for (int i = 0; i < row_size; i++)
	{
		ptr[i] = (int*)malloc(col_size * sizeof(int));
		if (ptr[i] == NULL)
		{
			printf("Memory allocation failed !!");
		}
	}

	printf("\n\n");
	printf("Array dimensions are '%d x %d' \n\n", row_size,col_size);
	for (int i = 0; i < row_size; i++)
	{
		printf("Enter %d integer elements of row %d :\n", col_size, (i + 1));
		for (int j = 0; j < col_size; j++)
		{
			scanf("%d", (*(ptr+i)+j));
			/*scanf("%d", iArray[col_size]);*/
		}
	}
	printf("\n\n");
	printf("Array elements are :\n\n");
	for (int i = 0; i < row_size; i++)
	{
		for (int j = 0; j < col_size; j++)
		{
			printf("%d\t", *(*(ptr + i) + j));
		}
		printf("\n");
	}
	printf("\n++++++ PRINTING ADDRESSES ++++++\n");
	printf("\n PRINTING BASE ADDRESSES OF ARRAYS OF INTEGER POINTERS (ptr + i) :\n");
	
	for (int i = 0; i < row_size; i++)
	{
			printf("%p ", (ptr + i));
	}

	printf("\n PRINTING BASE ADDRESSES OF FIVE 1-D ARRAYS (ptr[i]) :\n");

	for (int i = 0; i < row_size; i++)
	{
			printf("%p \n", (ptr[i]));
	}

	printf("\n PRINTING ACTUAL ELEMENTS OF  2-D ARRAYS (ptr[i][j]) :\n");

	for (int i = 0; i < row_size; i++)
	{
		for (int j = 0; j < col_size; j++)
		{
			printf("%d\t", ptr[i][j]);
		}
		printf("\n");
	}

	printf("\nNow freeing the memory allocated in reverse order of allocation..\n");
	printf("\nFreeing memory of columns (ptr[i]) ...\n");
	
	for (int i = 0; i < row_size; i++)
	{
		for (int j = 0; j < col_size; j++)
		{
			if (ptr[j] != NULL) 
			{
				free(ptr[j]);
			}
		}
	}

	printf("\nFreeing base memory of rows (ptr) ...\n");
	if (ptr != NULL)
		free(ptr);

	getch();
	return 0;
}