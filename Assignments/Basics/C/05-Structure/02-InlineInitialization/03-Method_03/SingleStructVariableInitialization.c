#include<stdio.h>

	// Inline intialization of struct variable 'data' of type 'struct MyData'

int main(void)
{

	//Defining struct
	struct MyData
	{
		int i;
		float f;
		double d;
		char c;
	}data = { 3,41.5f,18.44995,'S' };

	//Code
	//Displaying values of data memebers of 'struct MyData'
	printf("\n\n");
	printf("Data memebers of 'struct MyData' are :\n\n");
	printf("i = %d\n", data.i);
	printf("f = %f\n", data.f);
	printf("d = %lf\n", data.d);
	printf("c = %c\n", data.c);


	return 0;
}


