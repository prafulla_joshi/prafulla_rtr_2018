#include<stdio.h>

//Defining struct
struct MyData
{
	int i;
	float f;
	double d;
	char c;
};

struct MyData data = { 9,4.5f,9.61998,'P' };

int main(void)
{
	//Code
	//Displaying values of data memebers of 'struct MyData'
	printf("\n\n");
	printf("Data memebers of 'struct MyData' are :\n\n");
	printf("i = %d\n", data.i);
	printf("f = %f\n", data.f);
	printf("d = %lf\n", data.d);
	printf("c = %c\n", data.c);
	return 0;
}


