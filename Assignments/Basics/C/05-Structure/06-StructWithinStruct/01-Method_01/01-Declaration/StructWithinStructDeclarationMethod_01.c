#include<stdio.h>

struct Rectangle
{
	struct MyPoint
	{
		int x;
		int y;
	} point_01, point_02;
}rect;

int main(void)
{
	//Variable declaration
	int length, breadth, area;

	//Code
	printf("\n\n");
	printf("Enter left-most X-coordinate of rectangle :");
	scanf("%d",&rect.point_01.x);

	printf("\n\n");
	printf("Enter bottom-most Y-coordinate of rectangle :");
	scanf("%d",&rect.point_01.y);

	printf("\n\n");
	printf("Enter right-most X-coordinate of rectangle :");
	scanf("%d",&rect.point_02.x);

	printf("\n\n");
	printf("Enter top-most Y-coordinate of rectangle :");
	scanf("%d",&rect.point_02.y);

	length = rect.point_02.y - rect.point_01.y;
	if(length < 0)
		length = length * -1;

	breadth = rect.point_02.x - rect.point_01.x;
	if (breadth < 0)
		breadth = breadth * -1;

	area = length * breadth;

	printf("\n\n");
	printf("Length of Rectangle = %d\n\n",length);
	printf("Breadth of Rectangle = %d\n\n",breadth);
	printf("Area of Rectangle = %d\n\n",area);

	return 0;

}

