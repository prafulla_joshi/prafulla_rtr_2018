#include<stdio.h>

struct MyStruct
{
	int i;
	float f;
	double d;
	char c;
};

union MyUnion
{
	int i;
	float f;
	double d;
	char c;
};

int main(void)
{
	//Variable declarations
	struct MyStruct s;
	union MyUnion u;

	//Code

	/*-------- MyStruct s ---------*/
	printf("\n\n");
	printf("Members of struct are :\n\n");

	s.i = 9;
	s.f = 9.3f;
	s.d = 7.394574;
	s.c = 'M';

	printf("s.i = %d\n\n", s.i);
	printf("s.f = %f\n\n", s.f);
	printf("s.d = %lf\n\n", s.d);
	printf("s.c = %c\n\n", s.c);

	printf("Addresses of members of struct are : \n\n");
	printf("s.i = %p\n\n", &s.i);
	printf("s.f = %p\n\n", &s.f);
	printf("s.d = %p\n\n", &s.d);
	printf("s.c = %p\n\n", &s.c);

	printf("MyStruct s = %p\n\n",&s);

/*-------- MyUnion u ---------*/
	printf("\n\n");
	printf("Members of union u are :\n\n");

	u.i = 5;
	printf("u.i = %d\n\n",u.i);

	u.f = 7.2f;
	printf("u.f = %f\n\n",u.f);

	u.d = 12.7915287;
	printf("u.d = %lf\n\n",u.d);

	u.c = 'S';
	printf("u.c = %c\n\n",u.c);

	printf("Addresses of members of union u are : \n\n");
	printf("u.i = %p\n\n", &u.i);
	printf("u.f = %p\n\n", &u.f);
	printf("u.d = %p\n\n", &u.d);
	printf("u.c = %p\n\n", &u.c);

	printf("MyUnion u = %p\n\n", &u);
	return 0;

}

