#include<stdio.h>

union MyUnion
{
	int i;
	float f;
	double d;
	char c;
};

int main(void)
{
	//Variablde declarions
	union MyUnion u1, u2;

	//Code

	/*-------- MyUnion u1 ---------*/
	printf("\n\n");
	printf("Members of Union u1 are :\n\n");

	u1.i = 9;
	u1.f = 9.3f;
	u1.d = 7.394574;
	u1.c = 'M';

	printf("u1.i = %d\n\n", u1.i);
	printf("u1.f = %f\n\n", u1.f);
	printf("u1.d = %lf\n\n", u1.d);
	printf("u1.c = %c\n\n", u1.c);

	printf("Addresses of members of union u1 are : \n\n");
	printf("u1.i = %p\n\n", &u1.i);
	printf("u1.f = %p\n\n", &u1.f);
	printf("u1.d = %p\n\n", &u1.d);
	printf("u1.c = %p\n\n", &u1.c);

	printf("MyUnion u1 = %p\n\n",&u1);

	/*-------- MyUnion u2 ---------*/
	printf("\n\n");
	printf("Members of union u2 are :\n\n");

	u2.i = 5;
	printf("u2.i = %d\n\n",u2.i);

	u2.f = 7.2f;
	printf("u2.f = %f\n\n",u2.f);

	u2.d = 12.7915287;
	printf("u2.d = %lf\n\n",u2.d);

	u2.c = 'S';
	printf("u2.c = %c\n\n",u2.c);

	printf("Addresses of members of union u2 are : \n\n");
	printf("u2.i = %p\n\n", &u2.i);
	printf("u2.f = %p\n\n", &u2.f);
	printf("u2.d = %p\n\n", &u2.d);
	printf("u2.c = %p\n\n", &u2.c);

	printf("MyUnion u2 = %p\n\n", &u2);

	return 0;
}