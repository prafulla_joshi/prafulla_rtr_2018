#include<stdio.h>

struct MyStruct
{
	int i;
	float f;
	double d;
	char c;
};

union MyUnion
{
	int i;
	float f;
	double d;
	char c;
};

int main(void)
{
	//Variable declarations
	struct MyStruct s;
	union MyUnion u;

	//Code
	printf("\n\n");
	printf("Size of MyStruct = %d\n",sizeof(s));
	printf("\n\n");
	printf("Size of MyUnion = %d\n", sizeof(u));

	return 0;

}

