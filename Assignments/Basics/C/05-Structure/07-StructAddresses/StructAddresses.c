#include<stdio.h>

struct MyData
{
	int i;
	float f;
	double d;
	char c;
};


int main(void)
{
	// Variable declaration
	struct MyData data;

	//Code
	//Assigning data values to the data members of 'struct MyData'
	data.i = 30;
	data.f = 23.76f;
	data.d = 3.8993;
	data.c = 'P';

	//Displaying values of data members of 'struct MyData'
	printf("\n\n");
	printf("Data Members of 'struct MyData' are :\n\n");
	printf("i = %d\n",data.i);
	printf("f = %f\n",data.f);
	printf("d = %lf\n",data.d);
	printf("c = %c\n\n",data.c);

	printf("\n\n");
	printf("Address of data members of 'struct MyData' are :\n\n");
	printf("'i' occupies addresses from %p\n", &data.i);
	printf("'f' occupies addresses from %p\n", &data.f);
	printf("'d' occupies addresses from %p\n", &data.d);
	printf("'c' occupies address %p\n\n", &data.c);

	printf("Starting address of 'struct MyData' variable 'data' = %p\n\n", &data);

	return 0;
}