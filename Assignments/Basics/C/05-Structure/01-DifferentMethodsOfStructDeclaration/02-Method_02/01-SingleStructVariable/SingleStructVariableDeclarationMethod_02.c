#include<stdio.h>

// DEFINING STRUCT
struct MyData
{
	int i;
	float f;
	double d;
};

struct MyData data;	// Declaring a single struct variable of type 'struct MyData' gloabally..

int main(void)
{
	// variable declaration
	int i_size;
	int f_size;
	int d_size;
	int struct_MyData_size;

	//Code
	//Assigning data values to the data members of 'struct MyData'
	data.i = 30;
	data.f = 11.45;
	data.d = 1.2995;

	// Displaying values of data members of 'struct MyData'
	printf("\n\n");
	printf("Data memebrs of 'struct MyData' are : \n\n");
	printf("i = %d\n", data.i);
	printf("f = %f\n", data.f);
	printf("d = %lf\n", data.d);

	// Calculating sizes (in bytes) of the data members of 'struct MyData'
	i_size = sizeof(data.i);
	f_size = sizeof(data.f);
	d_size = sizeof(data.d);

	// Displaying sizes (in bytes) of data members of 'struct MyData'
	printf("\n\n");
	printf("Sizes (in bytes) of data members of 'struct MyData' are : \n\n");
	printf("Size of 'i' = %d bytes\n", i_size);
	printf("Size of 'f' = %d bytes\n", f_size);
	printf("Size of 'd' = %d bytes\n", d_size);

	// Calculating sizes (in bytes) of the entire 'struct MyData'
	struct_MyData_size = sizeof(struct MyData);		// Can also be given as --> sizeof(MyData)

	// Displaying sizes (in bytes) of the entire 'struct MyData'

	printf("\n\n");
	printf("Size of 'struct MyData' : %d bytes\n\n", struct_MyData_size);

	return 0;
}