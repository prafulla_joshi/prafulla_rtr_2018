#include<stdio.h>

int main(void)
{
	//Defining struct
	struct MyPoint
	{
		int x;
		int  y;
	}point_A, point_B, point_C, point_D;		// Declaring 4 struct variables of type 'struct MyPoint'

	//Code

	//Assigning data values to data members of 'struct MyPoint' variable 'point_A'
	point_A.x = 3;
	point_A.y = 0;

	//Assigning data values to data members of 'struct MyPoint' variable 'point_B'
	point_B.x = 1;
	point_B.y = 2;

	//Assigning data values to data members of 'struct MyPoint' variable 'point_C'
	point_C.x = 9;
	point_C.y = 5;

	//Assigning data values to data members of 'struct MyPoint' variable 'point_D'
	point_D.x = 12;
	point_D.y = 4;

	// Displaying values of data members of 'struct MyPoint' (all variables)
	printf("\n\n");
	printf("Co-ordinates (x, y) of point 'A' are : (%d, %d)\n\n", point_A.x, point_A.y);
	printf("Co-ordinates (x, y) of point 'B' are : (%d, %d)\n\n", point_B.x, point_B.y);
	printf("Co-ordinates (x, y) of point 'C' are : (%d, %d)\n\n", point_C.x, point_C.y);
	printf("Co-ordinates (x, y) of point 'D' are : (%d, %d)\n\n", point_D.x, point_D.y);


	return 0;
}