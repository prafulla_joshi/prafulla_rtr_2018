#include<stdio.h>
int main(void)
{
	// DEFINING STRUCT
	struct MyData
	{
		int i;
		float f;
		double d;
	}data;		// Declaring a single struct variable of type 'struct MyData' locally...

	// Variable declaration
	int i_size;
	int f_size;
	double d_size;
	int struct_MyData_size;

	//code
	//Assigninig the values of data memebrs of struct MyData

	data.i = 30;
	data.f = 11.45f;
	data.d = 1.2995;

	//Displaying values of data members of 'strcut MyData'
	printf("\n\n");
	printf("\nData members of 'struct MyData' are :\n\n");
	printf("i = %d\n", data.i);
	printf("f = %f\n", data.f);
	printf("d = %lf\n", data.d);


	//Calculating sizes (in bytes) of data members of 'struct MyData'
	i_size = sizeof(data.i);
	f_size = sizeof(data.f);
	d_size = sizeof(data.d);


	//Displaying sizes (in bytes) of data members of 'struct MyData'
	printf("\n\n");
	printf("Sizes (in bytes) of data members of 'struct MyData' :\n\n");
	printf("Size of 'i' = %d bytes\n", i_size);
	printf("Size of 'f' = %d bytes\n", f_size);
	printf("Size of 'd' = %d bytes\n", d_size);

	//Calculating sizes (in bytes) of entire 'struct MyData'
	struct_MyData_size = sizeof(struct MyData);	// can also give struct name -> sizeof(MyData)

	//Displaying sizes (in bytes) of entire 'struct MyData'
	printf("\n\n");
	printf("Size of 'struct MyData' : %d bytes\n\n",struct_MyData_size);

	return 0;


}