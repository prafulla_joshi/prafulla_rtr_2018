#include<stdio.h>
int main(void)
{
	//Defining Struct
	struct MyPoint
	{
		int x;
		int y;
	}point;		//declaring a single variable of type 'struct MyPoint' locally...

	// Defining Struct
	struct MyPointProperties
	{
		int quadrant;
		char axis_location[10];
	}point_properties;		//declaring a single variable of type 'struct MyPointProprties' locally...

	//Code
	//user input for data memebers of 'struct MyPoint' variable 'point_A'
	printf("\n\n");
	printf("Enter X-Coordinates for A point :");
	scanf("%d", &point.x);
	printf("Enter Y-Coordinates for A point :");
	scanf("%d", &point.y);

	printf("\n\n");
	printf("Point Co-ordinates(x, y) are :(%d, %d) !!\n\n", point.x, point.y);

	if (point.x == 0 && point.y == 0)
		printf("The point is the origin (%d, %d) !!\n", point.x, point.y);
	else		// Atleast one of the two values (either 'X' of 'Y' or BOTH) is a non-zero value...
	{
		if (point.x == 0)		// If 'X' IS ZERO...OBVIOUSLY 'Y' IS THE NON-ZERO VALUE
		{
			if (point.y < 0)	// If 'Y' is -ve
				strcpy(point_properties.axis_location, "Negative Y");
			
			if (point.y > 0)	// If 'Y' is +ve
				strcpy(point_properties.axis_location, "Positive Y");

			point_properties.quadrant = 0;		// A Point Lying On Any Of The Co-ordinate Axes Is NOT A Part Of ANY Quadrant...
			printf("The point lies on the %s axis !!\n\n", point_properties.axis_location);
		}
		else if (point.y == 0)		// If 'Y' IS ZERO...OBVIOUSLY 'X' IS THE NON-ZERO VALUE
		{
			if (point.x < 0)	// If 'X' is -ve
				strcpy(point_properties.axis_location, "Negative Y");

			if (point.x > 0)	// If 'X' is +ve
				strcpy(point_properties.axis_location, "Positive Y");

			point_properties.quadrant = 0;		// A Point Lying On Any Of The Co-ordinate Axes Is NOT A Part Of ANY Quadrant...
			printf("The point lies on the %s axis !!\n\n", point_properties.axis_location);
		}
		else		// Both 'X' and 'Y' are non-zero
		{
			point_properties.axis_location[0] = '\0';	// A Point Lying In ANY Of The 4 Quadrants Cannot Be Lying On Any Of The Co-ordinate Axes...
			
			if (point.x > 0 && point.y > 0)		// 'X' is +ve and 'Y' is +ve
				point_properties.quadrant = 1;

			else if (point.x < 0 && point.y > 0)		// 'X' is -ve and 'Y' is +ve
				point_properties.quadrant = 2;

			else if (point.x < 0 && point.y < 0)		// 'X' is -ve and 'Y' is -ve
				point_properties.quadrant = 3;
			else
				point_properties.quadrant = 4;

			printf("The point lies in quadrant number %d !!!\n\n", point_properties.quadrant);
		}
	}
	return 0;
}