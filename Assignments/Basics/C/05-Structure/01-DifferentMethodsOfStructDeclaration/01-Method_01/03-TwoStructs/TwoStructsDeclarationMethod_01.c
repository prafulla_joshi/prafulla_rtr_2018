#include<stdio.h>

// DEFINING STRUCT
struct MyPoint
{
	int x;
	int y;
}point;	// declaring a single variable of type 'struct MyPoint' globally..

// DEFINING STRUCT
struct MyPointProperties
{
	int quadrant;
	char axis_location[10];
}point_properties;	// declaring a single variable of type 'struct MyPointProperties' globally..

int main(void)
{
	//Code
	//User input fro data members of 'struct MyPoint' variable 'point_A'
	printf("Enter X-Coordinates for a point : ");
	scanf("%d", &point.x);
	printf("Enter Y-Coordinates for a point : ");
	scanf("%d", &point.y);

	printf("\n\n");
	printf("Point co-ordinates (x, y) are : (%d, %d) !!\n\n", point.x, point.y);

	if (point.x == 0 && point.y == 0)
		printf("The point is the origin (%d, %d) !!\n", point.x, point.y);
	else	// Atleast one of the two values is non-zero value..
	{
		if (point.x==0)	// If 'x' is ZERO..Obviously 'y' is non-zero value
		{
			if (point.y < 0)	// 'y' is negative
				strcpy(point_properties.axis_location, "Negative Y");
			if (point.y > 0)
				strcpy(point_properties.axis_location, "Positive Y");

			point_properties.quadrant = 0;	// A point lying on any of the axis is not a part of any quadrant
			printf("The point lies on the %s axis !!", point_properties.axis_location);
		}
		else if(point.y==0)	// If 'y' is ZERO..Obviously 'x' is non-zero value
		{
			if (point.x < 0)
				strcpy(point_properties.axis_location, "Negative X");
			if (point.x > 0)
				strcpy(point_properties.axis_location, "Positive X");

			point_properties.quadrant = 0;	// A point lying on any of the axis is not a part of any quadrant
			printf("The point lies on the %s axis !!\n\n", point_properties.axis_location);
		}
		else	// Both X and Y are non-zero values
		{
			point_properties.axis_location[0] = '\0';	// A point lying in any quadrant cannot be on any axis

			if (point.x > 0 && point.y > 0)	// 'X' is +ve and 'Y' is +ve
				point_properties.quadrant = 1;
			else if (point.x < 0 && point.y > 0) // 'X' is -ve and 'Y' is +ve
				point_properties.quadrant = 2;
			else if (point.x < 0 && point.y < 0)	// 'X' is -ve and 'Y' is -ve
				point_properties.quadrant = 3;
			else									// 'X' is -ve and 'Y' is -ve
				point_properties.quadrant = 4;

			printf("The point lies in quadrant number %d !!\n\n", point_properties.quadrant);
		}
	}
	return 0;
}
