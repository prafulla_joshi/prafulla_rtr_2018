#include <stdio.h>
#include <conio.h>

struct MyData
{
	int i;
	float f;
	double d;
	char ch;
};

int main(void)
{
	//variable declarations
	struct MyData data; //declaring a single struct variable

	//code
	//User input for values of data members of 'struct MyData'
	printf("\n\n");

	printf("Enter value for data memeber i :\n");
	scanf("%d", &data.i);

	printf("Enter value for data memeber f :\n");
	scanf("%f", &data.f);

	printf("Enter value for data memeber d :\n");
	scanf("%lf", &data.d);

	printf("Enter value for data memeber ch :\n");
	data.ch = getch();

	//Display Values Of Data Members Of 'struct MyData'
	printf("\n\n");
	printf("Data memebers of 'struct MyData' are : \n\n");
	printf("i = %d\n", data.i);
	printf("f = %f\n", data.f);
	printf("d = %lf\n", data.d);
	printf("c = %c\n\n", data.ch);

	return 0;
}
