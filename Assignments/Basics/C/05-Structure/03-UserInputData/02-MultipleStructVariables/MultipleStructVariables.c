#include <stdio.h>

struct MyPoint
{
	int x;
	int y;
};

int main(void)
{
	//variable declarations
	struct MyPoint point_A, point_B, point_C, point_D;		//declaring a 4 struct variables

	//code
	//User Input For The Data Members Of 'struct MyPoint' variable 'point_A'
	printf("\n\n");
	printf("Enter X-coordinate for point 'A' : ");
	scanf("%d", &point_A.x);
	printf("Enter Y-coordinate for point 'A' : ");
	scanf("%d", &point_A.y);

	//User Input For The Data Members Of 'struct MyPoint' variable 'point_B'
	printf("\n\n");
	printf("Enter X-coordinate for point 'B' : ");
	scanf("%d", &point_B.x);
	printf("Enter Y-coordinate for point 'B' : ");
	scanf("%d", &point_B.y);

	//User Input For The Data Members Of 'struct MyPoint' variable 'point_C'
	printf("\n\n");
	printf("Enter X-coordinate for point 'C' : ");
	scanf("%d", &point_C.x);
	printf("Enter Y-coordinate for point 'C' : ");
	scanf("%d", &point_C.y);

	//User Input For The Data Members Of 'struct MyPoint' variable 'point_D'
	printf("\n\n");
	printf("Enter X-coordinate for point 'D' : ");
	scanf("%d", &point_D.x);
	printf("Enter Y-coordinate for point 'D' : ");
	scanf("%d", &point_D.y);

	//Displaying Values Of The Data Members Of 'struct MyPoint' (all variables)
	printf("\n\n");
	printf("Co-ordinates (x, y) of point 'A' are : (%d, %d)\n\n", point_A.x, point_A.y);
	printf("Co-ordinates (x, y) of point 'B' are : (%d, %d)\n\n", point_B.x, point_B.y);
	printf("Co-ordinates (x, y) of point 'C' are : (%d, %d)\n\n", point_C.x, point_C.y);
	printf("Co-ordinates (x, y) of point 'D' are : (%d, %d)\n\n", point_D.x, point_D.y);

	return 0;
}
