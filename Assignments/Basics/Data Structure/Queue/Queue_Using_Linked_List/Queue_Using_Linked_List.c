#include<stdio.h>
#include<stdlib.h>

struct node
{
	int data;
	struct node *next;
}*front, *rear;


//Global Function declaration
//Function Declaration
int IsQueueEmpty();


int IsQueueEmpty()
{
	//Code
	if (front == NULL && rear == NULL)
	{
		return 1;
	}
	return 0;
}



int main(void)
{
	//Function declaration
	void Create(int);
	int Display(void);
	void EnQueue(int);
	int DeQueue();
	//Variable declarations
	int ch, NumberOfElements, position, element, result;

	//Code
	//head = NULL;	// making the head NULL since the list is empty initially
	front = rear = NULL;
	while (1)
	{
		printf("\n0)Exit \n1)Create \n2)Display \n3)EnQueue \n4)DeQueue  \n");
		printf("\nEnter your choice :");
		scanf("%d", &ch);
		switch (ch)
		{
		case 0:
			exit(0);
			break;
		case 1:
			printf("\nHow many elements do you want to add in queue :");
			scanf("%d", &NumberOfElements);
			for (int i = 0; i < NumberOfElements; i++)
			{
				printf("Enter the element :");
				scanf("%d", &element);
				Create(element);
			}
			break;
		case 2:
			Display();
			break;
		case 3:
			printf("\nEnter the element to add at rear of queue :");
			scanf("%d", &element);
			EnQueue(element);
			printf("\nElement added at the rear successfully !!");
			break;
		case 4:
			DeQueue();
			break;
		default:
			printf("\nPlease enter the correct choice :");
			break;
		}
	}
}


void Create(int element)
{
	//Code
	struct node *tmp, *q;

	tmp = (struct node*)malloc(sizeof(struct node));
	tmp->data = element;
	tmp->next = NULL;

	if (IsQueueEmpty())
	{
		front = rear = tmp;									// Point  front and rear to tmp...since it will be the first element
	}
	else
	{
		q = front;
		while (q->next != NULL)
			q = q->next;
		q->next = tmp;								// Put address of tmp in q
		rear = tmp;								// make rear to point to tmp
	}
}

int Display()
{
	//Code
	struct node *q;
	if (IsQueueEmpty())
	{
		printf("\nQueue is empty\n\n");
		return 1;
	}
	q = front;								// Point q to front
	printf("\nThe Queue is :\n");
	while (q != NULL)
	{
		printf("\n%d", q->data);
		q = q->next;				// point q to q->next
	}
	printf("\n");
	return 0;
}


void EnQueue(int element)
{
	//Variable declaration
	struct node *tmp, *q;

	//Code

	tmp = (struct node*)malloc(sizeof(struct node));
	tmp->data = element;
	tmp->next = NULL;

	if (!IsQueueEmpty())
	{
		q = front;
		while (q->next != NULL)
			q = q->next;
		q->next = tmp;								// Put address of tmp in q
		rear = tmp;								// make rear to point to tmp
	}
	else
	{
		front = rear = tmp;			// Do this if queue is empty, i.e, tmp will be 1st element
	}
	
}


int DeQueue()
{
	//Variablde declaration
	struct node *tmp;
	int x;
	//Code
	if (!IsQueueEmpty())			
	{
		tmp = front;						// Make tmp point to front
		x = front->data;
		front = front->next;				// Make head to point to next element in the list(2nd element..;so that first element can be deleted safely)
		free(tmp);
		printf("\nElement deleted successfully !!\n");
		
		if (front == NULL)
		{
			rear = NULL;
		}
		printf("\nDeleted Element = %d\n", x);
		return 0;
	}
	else
	{
		printf("\nQueue is Empty !!");
		return 1;
	}
}

