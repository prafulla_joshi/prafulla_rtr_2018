/*THIS PROGRAM WILL REPLCE ALL VOWELS IN STRING WITH *(asterisk) SYMBOL  */

#include<stdio.h>
#define MAX_STRING_LENGTH 512

int main(void)
{
	// function declaration
	int MyStrlen(char[]);
	void MyStrcpy(char[], char[]);

	//Variable declaration
	char chArray_Original[MAX_STRING_LENGTH], chArray_Vowels_Replaced[MAX_STRING_LENGTH];
	int iStringLength;
	int i;

	//code

	//  STRIING INPUT
	printf("\n\n");
	printf("Enter a string :\n\n");
	gets_s(chArray_Original, MAX_STRING_LENGTH);
	
	// STRING OUTPUT
	MyStrcpy(chArray_Vowels_Replaced, chArray_Original);
	iStringLength = MyStrlen(chArray_Vowels_Replaced);

	for (i = 0; i < iStringLength; i++)
	{
		switch (chArray_Vowels_Replaced[i])
		{
		case 'A':
		case 'a':
		case 'E':
		case 'e':
		case 'I':
		case 'i':
		case 'O':
		case 'o':
		case 'U':
		case 'u':
			chArray_Vowels_Replaced[i] = '*';
			break;
		default:
			break;
		}
	}

	// STRING OUTPUT

	printf("\n\n");
	printf("String entered by you :\n\n");
	printf("%s\n", chArray_Original);

	printf("\n\n");
	printf("String after replcement of vowels by * is :\n\n");
	printf("%s\n", chArray_Vowels_Replaced);

	return 0;
}

int MyStrlen(char str[])
{
	//Variable declaration
	int j;
	int string_length = 0;

	//Code
	for (j = 0; j < MAX_STRING_LENGTH; j++)
	{
		if (str[j] == '\0')
			break;
		else
			string_length++;
	}

	return string_length;
}

void MyStrcpy(char str_destination[], char str_source[])
{
	// Function declaration
	int MyStrlen(char[]);

	// Variable declaration
	int iStringLength = 0;
	int j = 0;

	// Code
	iStringLength = MyStrlen(str_source);
	for ( j = 0; j < iStringLength; j++)
		str_destination[j] = str_source[j];

	str_destination[j] = '\0';
}