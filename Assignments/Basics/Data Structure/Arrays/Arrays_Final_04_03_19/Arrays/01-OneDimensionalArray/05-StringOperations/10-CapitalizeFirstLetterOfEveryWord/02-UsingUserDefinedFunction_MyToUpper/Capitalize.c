#include<stdio.h>
#define MAX_STRING_LENGTH 512

#define SPACE ' '
#define COMMA ','
#define FULLSTOP '.'
#define EXCLAMATION '!'
#define QUESTION_MARK '?'

int main(void)
{
	//Function declaration
	int MyStrlen(char[]);
	char MyToUpper(char);

	//variable declarations 
	char chArray[MAX_STRING_LENGTH], chArray_CapitalizedFirstLetterOfEveryWord[MAX_STRING_LENGTH]; // A Character Array Is A String
	int iStringLength;
	int i, j;

	printf("\n\n");
	printf("Enter a string :\n\n");
	gets_s(chArray, MAX_STRING_LENGTH);

	iStringLength = MyStrlen(chArray);
	j = 0;
	for (i = 0; i < iStringLength; i++)
	{
		if (i == 0)	// First letter of every sentence must be capital
			chArray_CapitalizedFirstLetterOfEveryWord[j] = MyToUpper(chArray[i]);
		else if (chArray[i] == SPACE)	// Words are separated by spaces
		{
			chArray_CapitalizedFirstLetterOfEveryWord[j] = chArray[i];
			chArray_CapitalizedFirstLetterOfEveryWord[j + 1] = MyToUpper(chArray[i + 1]);
			// Already two characters (at indices i and i+1) have been considered in this else-if block, hence we are extra increamenting i and j
			j++;
			i++;
		}
		else if ((chArray[i] == FULLSTOP || chArray[i] == COMMA || chArray[i] == EXCLAMATION || chArray[i] == QUESTION_MARK) && chArray[i] != SPACE)
		{
			chArray_CapitalizedFirstLetterOfEveryWord[j] = chArray[i];
			chArray_CapitalizedFirstLetterOfEveryWord[j + 1] = SPACE;
			chArray_CapitalizedFirstLetterOfEveryWord[j + 2] = MyToUpper(chArray[i + 1]);

			// Already two characters (at indices i and i+1) have been considered in this else-if block, hence we are extra increamenting i and j
			// Already two characters (at indices j , j+1 and j+2) have been considered in this else-if block, hence we are extra increamenting i and j
			j = j + 2;
			i++;
		}
		else
			chArray_CapitalizedFirstLetterOfEveryWord[j] = chArray[i];
		j++;
	}

	chArray_CapitalizedFirstLetterOfEveryWord[j] = '\0';

	/****** STRING OUTPUT*********/

	printf("\n\n");
	printf("String entered by you is :\n\n");
	printf("%s\n", chArray);

	printf("\n\n");
	printf("String after capitalizing first letter of every word is :\n\n");
	printf("%s\n", chArray_CapitalizedFirstLetterOfEveryWord);

	return 0;
}


int MyStrlen(char str[])
{
	//Variable declration
	int j;
	int string_length = 0;
	//code
	for (j = 0; j < MAX_STRING_LENGTH; j++)
	{
		if (str[j] == '\0')
			break;
		else
			string_length++;
	}

	return string_length;
}

char MyToUpper(char ch)
{
	//Variable declaration
	int num;
	int c;

	// Code
	/*
	ASCII VALUE OF 'a'(97) - ASCII VALUE OF 'A'(65) = 32 
	THIS SUBTRACTION WILL GIVE THE EXACT DIFFERENCE BETWEEN UPPER AND LOWER CASE CHARACTERS
	ASCII VALUES OF 'a' to 'z' => 97 to 122
	ASCII VALUES OF 'A' to 'Z' => 65 to 90
	
	*/

	num = 'a' - 'A';
	if ((int)ch >= 97 && (int)ch <= 122)
	{
		c = (int)ch - num;
		return (char)c;
	}
	else
		return ch;
}
