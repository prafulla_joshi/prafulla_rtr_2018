#include<stdio.h>
#define MAX_STRING_LENGTH 512

int main(void)
{
	// Variable declarations
	char chArray_One[MAX_STRING_LENGTH];
	char chArray_Two[MAX_STRING_LENGTH];	// A character array is a string

	//Code
	//String Input
	printf("\n\n");
	printf("Enter 1st string :\n\n");
	gets_s(chArray_One, MAX_STRING_LENGTH);

	printf("\n\n");
	printf("Enter 2nd string :\n\n");
	gets_s(chArray_Two, MAX_STRING_LENGTH);

	//String Concat

	printf("\n\n");
	printf("***** BEFORE CONCATENATION ******");
	printf("The 1st string entered by you :");
	printf("%s\n", chArray_One);

	printf("\n\n");
	printf("The 2nd string entered by you :");
	printf("%s\n", chArray_One);

	strcat(chArray_One, chArray_Two);

	printf("\n\n");
	printf("***** AFTER CONCATENATION ******");
	printf("The 1st string entered by you :");
	printf("%s\n", chArray_One);

	printf("\n\n");
	printf("The 2nd string entered by you :");
	printf("%s\n", chArray_One);

	return 0;
}	