#include<stdio.h>
#define MAX_STRING_LENGTH 512

int main(void)
{
	//Function declaration
	void MyStrcat(char[], char[]);

	// Variable declarations
	char chArray_One[MAX_STRING_LENGTH];
	char chArray_Two[MAX_STRING_LENGTH];	// A character array is a string

	//Code
	//String Input
	printf("\n\n");
	printf("Enter 1st string :\n\n");
	gets_s(chArray_One, MAX_STRING_LENGTH);

	printf("\n\n");
	printf("Enter 2nd string :\n\n");
	gets_s(chArray_Two, MAX_STRING_LENGTH);

	//String Concat

	printf("\n\n");
	printf("***** BEFORE CONCATENATION ******");
	printf("The 1st string entered by you :");
	printf("%s\n", chArray_One);

	printf("\n\n");
	printf("The 2nd string entered by you :");
	printf("%s\n", chArray_One);

	MyStrcat(chArray_One, chArray_Two);

	printf("\n\n");
	printf("***** AFTER CONCATENATION ******");
	printf("The 1st string entered by you :");
	printf("%s\n", chArray_One);

	printf("\n\n");
	printf("The 2nd string entered by you :");
	printf("%s\n", chArray_One);

	return 0;
}

void MyStrcat(char str_destination[], char str_source[])
{
	// function declaration
	int MyStrlen(char[]);

	//Variable declaration
	int iStringLength_Source = 0, iStringLength_Destination = 0;
	int i, j;
	iStringLength_Source = MyStrlen(str_source);
	iStringLength_Destination = MyStrlen(str_destination);

	// ARRAY INDICES BEGIN FROM 0, HENCE, LAST VALID INDEX OF ARRAY WILL ALWAYS BE (LENGTH - 1)
	// SO, CONCATENATION MUST BEGIN FROM INDEX NUMBER EQUAL TO LENGTH OF THE ARRAY 'str_destination'
	// WE NEED TO PUT THE CHARACTER WHICH IS AT FIRST INDEX OF 'str_source' TO THE (LAST INDEX + 1) OF 'str_destination'

	for (i = iStringLength_Destination, j = 0; j < iStringLength_Source; i++, j++)
	{
		str_destination[i] = str_source[j];
	}

	str_destination[i] = '\0';
}


int MyStrlen(char str[])
{
	//Variable declaration
	int j;
	int string_len = 0;

	//Code
	// *** DETERMINING EXACT LENGTH OF THE STRING, BY DETECTING THE FIRST OCCURENCE OF NULL-TERMINATING CHARACTER ( \0 ) ***
	for (j = 0; j < MAX_STRING_LENGTH; j++)
	{
		if (str[j] == '\0')
			break;
		else
			string_len++;
	}
	return string_len;
}