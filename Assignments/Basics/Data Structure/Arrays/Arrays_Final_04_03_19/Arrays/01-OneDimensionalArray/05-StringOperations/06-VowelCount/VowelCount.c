#include<stdio.h>
#define MAX_STRING_LENGTH 512

int main(void)
{
	//functiom declaration
	int MyStrlen(char[]);
	
	//Variable declaration
	char chArray[MAX_STRING_LENGTH];	// A character array is a string
	int iStringLength;
	int count_A = 0, count_E = 0, count_I = 0, count_O = 0, count_U = 0;
	int i;

	//Code
	// STRING INPUT
	printf("\n\n");
	printf("Enter a string :\n\n");
	gets_s(chArray, MAX_STRING_LENGTH);

	// STRING OUTPUT

	printf("\n\n");
	printf("String entered by you :\n\n");
	printf("%s\n", chArray);

	iStringLength = MyStrlen(chArray);

	for (i = 0; i < iStringLength; i++)
	{
		switch (chArray[i])
		{
		case 'A':
		case 'a':
			count_A++;
			break;
		case 'E':
		case 'e':
			count_E++;
			break;
		case 'I':
		case 'i':
			count_I++;
			break;
		case 'O':
		case 'o':
			count_O++;
			break;
		case 'U':
		case 'u':
			count_U++;
			break;
		}
	}

	printf("\n\n");
	printf("NUmber of volwels and their respective occurances are :");
	printf("'A' has occured = %d times !!!\n\n", count_A);
	printf("'E' has occured = %d times !!!\n\n", count_E);
	printf("'I' has occured = %d times !!!\n\n", count_I);
	printf("'O' has occured = %d times !!!\n\n", count_O);
	printf("'U' has occured = %d times !!!\n\n", count_U);

	return 0;
}

int MyStrlen(char str[])
{
	//variable declaration
	int j;
	int string_length = 0;

	for (j = 0; j < MAX_STRING_LENGTH; j++)
	{
		if (str[j] == '\0')
			break;
		else
			string_length++;
	}
	return string_length;
}