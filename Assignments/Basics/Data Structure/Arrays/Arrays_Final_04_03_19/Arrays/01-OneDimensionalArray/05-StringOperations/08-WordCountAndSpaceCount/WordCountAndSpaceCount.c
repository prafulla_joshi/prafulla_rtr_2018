#include<stdio.h>
#define MAX_STRING_LENGTH 512

int main(void)
{
	//functiom declaration
	int MyStrlen(char[]);

	//Variable declaration
	char chArray[MAX_STRING_LENGTH];	// A character array is a string
	int iStringLength;
	int count_A = 0, count_E = 0, count_I = 0, count_O = 0, count_U = 0;
	int i;
	int word_count = 0, space_count = 0;

	// Code
	// STRING INPUT
	printf("\n\n");
	printf("Enter a string :\n\n");
	gets_s(chArray, MAX_STRING_LENGTH);

	iStringLength = MyStrlen(chArray);

	for (i = 0; i < iStringLength; i++)
	{
		switch (chArray[i])
		{
		case 32:			// 32 is the ASCII value for SPACE (' ') character
			space_count++;
			break;
		default:
			break;
		}
	}
	word_count = space_count + 1;

	// *** STRING OUTPUT ***
	printf("\n\n");
	printf("String entered by you :\n\n");
	printf("%s\n", chArray);

	printf("\n\n");
	printf("Number of Spaces in string :%d\n\n", space_count);
	printf("Number of Words in string :%d\n\n", word_count);

	return 0;
}


int MyStrlen(char str[])
{
	int j;
	int string_length = 0;

	for (j = 0; j < MAX_STRING_LENGTH; j++)
	{
		if (str[j] == '\0')
			break;
		else
			string_length++;
	}
	return string_length;
}