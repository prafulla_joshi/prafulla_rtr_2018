#include<stdio.h>
#include<string.h>
#define MAX_STRING_LENGTH 512

int main(void)
{
	//Variable declarations
	char chArray_Original[MAX_STRING_LENGTH]; // A character array is a String

	// Code

	// *** STRING INPUT

	printf("\n\n");
	printf("Enter a string :\n");
	gets_s(chArray_Original, MAX_STRING_LENGTH);

	//String Output

	printf("\n\n");
	printf("The original string entered by you is :\n\n");
	printf("%s\n", chArray_Original);

	printf("\n\n");
	printf("The reversed string is :\n\n");
	printf("%s\n", strrev(chArray_Original));

	return 0;
}