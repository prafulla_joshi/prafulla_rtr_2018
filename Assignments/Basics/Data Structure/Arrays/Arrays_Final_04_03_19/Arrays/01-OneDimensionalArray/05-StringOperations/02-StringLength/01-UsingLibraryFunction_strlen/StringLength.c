#include <stdio.h>

#define MAX_STRING_LENGTH 512

int main(void)
{
	//variable declarations 
	char chArray[MAX_STRING_LENGTH]; // A Character Array Is A String
	int iStringLength = 0;

	//Code

	//String Input
	printf("\n\n");
	printf("Enter the string :");
	gets_s(chArray, MAX_STRING_LENGTH);

	//String output
	printf("\n\n");
	printf("String entered by you is :\n\n");
	printf("%s\n", chArray);

	//String Length

	printf("\n\n");
	iStringLength = strlen(chArray);
	printf("Length of string is = %d characters !!\n\n", iStringLength);

	return 0;
}