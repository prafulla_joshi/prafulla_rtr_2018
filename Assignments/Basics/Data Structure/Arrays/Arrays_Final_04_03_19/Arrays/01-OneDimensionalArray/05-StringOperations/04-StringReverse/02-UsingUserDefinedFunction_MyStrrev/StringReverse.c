#include<stdio.h>
#define MAX_STRING_LENGTH 512

int main(void)
{
	//function declarations
	void MyStrrev(char[], char[]);

	//variable declarations

	char chArray_Original[MAX_STRING_LENGTH];
	char chArray_Reversed[MAX_STRING_LENGTH];	// A character array is string

	//Code

	//String Input

	printf("\n\n");
	printf("Enter a string :\n\n");
	gets_s(chArray_Original, MAX_STRING_LENGTH);

	//String Reverse
	MyStrrev(chArray_Reversed, chArray_Original);

	// String Output
	printf("\n\n");
	printf("The original string entered by you is :\n\n");
	printf("%s\n", chArray_Original);

	printf("\n\n");
	printf("The revered string is :\n\n");
	printf("%s\n", chArray_Reversed);

	return 0;
}

void MyStrrev(char str_reversed[], char str_original[])
{
	//function declarations
	int MyStrlen(char[]);

	//Variable declarations

	int iStringLength = 0;
	int i, j, len;

	//Code
	iStringLength = MyStrlen(str_original);

	// Array indices start from 0, hence last index will always be 'length - 1'
	len = iStringLength - 1;

	/*We need to put the character which is at last index of 'str_original' to the 1st index of 'str_reversed', and so on...*/

	for (i = 0, j = len; i < iStringLength,j>=0; i++,j--)
	{
		str_reversed[i] = str_original[j];
	}

	str_reversed[i] = '\0';

}


int MyStrlen(char str[])
{
	//Variable declarations
	int j;
	int string_length = 0;

	//Code
	/*Determine exact length of srting by detecting 1st occurance of null character*/

	for (j = 0; j < MAX_STRING_LENGTH; j++)
	{
		if (str[j] == '\0')
			break;
		else
			string_length++;
	}

	return string_length;
}

