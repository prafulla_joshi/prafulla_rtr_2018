#include<stdio.h>
int main(void)
{
	// Variable declarations
	int iArray[] = { 9,30,6,15,42,67,18,62,8,11 };
	int int_size;
	int iArray_size;
	int iArray_num_elements;

	float fArray[] = { 5.2f,1.9f,7.2f,4.8f,5.9f,7.1f,2.9f,6.7f,5.5f,9.1f };
	int float_size;
	int fArray_size;
	int fArray_num_elements;

	char cArray[] = { 'A','S','T','R','O','M','E','D','I','C','O','M','P' };
	int char_size;
	int cArray_size;
	int cArray_num_elements;

	int i;
	//Code

	//**** iArray[] ******
	printf("\n\n");
	printf("In-line Initialization and Loop display of elements of array 'iArray[]': \n");
	
	int_size = sizeof(int);
	iArray_size = sizeof(iArray);
	iArray_num_elements = iArray_size / int_size;

	for (i = 0; i < iArray_num_elements; i++)
	{
		printf("iArray[%d] (Element %d) = %d\n", i, (i + 1), iArray[i]);
	}

	printf("\n");
	printf("Size of data type 'int'                             = %d bytes\n", int_size);
	printf("Number of elements in 'iArray[]'                    = %d elements", iArray_num_elements);
	printf("Size of array 'iArray[]' (%d elements * %d bytes)   = %d bytes\n", iArray_num_elements, int_size, iArray_size);


	//***** fArray[] ******

	printf("\n");
	printf("In-line Initialization and Loop display of elements of array 'fArray[]': \n");
	float_size = sizeof(float);
	fArray_size = sizeof(fArray);
	fArray_num_elements = fArray_size / float_size;

	for (int i = 0; i < fArray_num_elements; i++)
	{
		printf("fArray[%d] (Element %d) = %f\n", i, (i + 1), fArray[i]);
	}

	printf("Size of data type 'float'                           = %d bytes\n", float_size);
	printf("Number of elements in 'fArray[]'                    = %d elements", fArray_num_elements);
	printf("Size of array 'fArray[]' (%d elements * %d bytes)   = %d bytes\n", fArray_num_elements, float_size, fArray_size);

	//***** cArray[] *******
	printf("\n");
	printf("In-line Initialization and Piece-meal display of elements of array 'cArray[]': \n");
	char_size = sizeof(char);
	cArray_size = sizeof(cArray);
	cArray_num_elements = cArray_size / char_size;

	for (int i = 0; i < cArray_num_elements; i++)
	{
		printf("cArray[%d] (Element %d) = %d\n", i, (i + 1), cArray[i]);
	}

	printf("Size of data type 'char'                            = %d bytes\n", char_size);
	printf("Number of elements in 'cArray[]'                    = %d elements", cArray_num_elements);
	printf("Size of array 'cArray[]' (%d elements * %d bytes)   = %d bytes\n", cArray_num_elements, char_size, cArray_size);

	getch();
	return 0;
}