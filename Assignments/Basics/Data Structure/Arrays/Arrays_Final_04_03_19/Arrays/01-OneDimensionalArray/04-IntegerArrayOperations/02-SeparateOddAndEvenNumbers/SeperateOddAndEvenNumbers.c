#include<stdio.h>
#define NUM_ELEMENTS 10

int main(void)
{
	//Variable declarations
	int iArray[NUM_ELEMENTS];
	int num;

	//Code

	//Array Input
	printf("Enter 10 integer elements for array :\n\n");
	for (int i = 0; i < NUM_ELEMENTS; i++)
	{
		scanf("%d", &num);
		iArray[i] = num;
	}

	//Separate out Even numbers from array
	printf("\n\n");
	printf("Even numbers from the array elements are :\n\n");
	for (int i = 0; i < NUM_ELEMENTS; i++)
	{
		if (iArray[i] % 2 == 0)
			printf("%d\n", iArray[i]);
	}

	//Separate out Odd numbers from array
	printf("\n\n");
	printf("Odd numbers from the array elements are :\n\n");
	for (int i = 0; i < NUM_ELEMENTS; i++)
	{
		if (iArray[i] % 2 != 0)
			printf("%d\n", iArray[i]);
	}

	return 0;
}