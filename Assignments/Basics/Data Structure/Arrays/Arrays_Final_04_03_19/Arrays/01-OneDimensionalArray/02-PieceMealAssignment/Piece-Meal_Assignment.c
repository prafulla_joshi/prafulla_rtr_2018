#include<stdio.h>
#include<conio.h>
int main(void)
{
	int iArrayOne[10];
	int iArrayTwo[10];

	//******iArrayOne[]********
	// Piece-Meal Assignment
	iArrayOne[0] = 3;
	iArrayOne[1] = 7;
	iArrayOne[2] = 4;
	iArrayOne[3] = 31;
	iArrayOne[4] = 89;
	iArrayOne[5] = 51;
	iArrayOne[6] = 19;
	iArrayOne[7] = 16;
	iArrayOne[8] = 91;
	iArrayOne[9] = 11;

	printf("\n");
	printf("Piece-Meal Display of 'iArraryOne[]' \n\n");
	printf("1st element of array 'iArrayOne[]'   = %d\n",iArrayOne[0]);
	printf("2nd element of array 'iArrayOne[]'   = %d\n",iArrayOne[1]);
	printf("3rd element of array 'iArrayOne[]'   = %d\n",iArrayOne[2]);
	printf("4th element of array 'iArrayOne[]'   = %d\n",iArrayOne[3]);
	printf("5th element of array 'iArrayOne[]'   = %d\n",iArrayOne[4]);
	printf("6th element of array 'iArrayOne[]'   = %d\n",iArrayOne[5]);
	printf("7th element of array 'iArrayOne[]'   = %d\n",iArrayOne[6]);
	printf("8th element of array 'iArrayOne[]'   = %d\n",iArrayOne[7]);
	printf("9th element of array 'iArrayOne[]'   = %d\n",iArrayOne[8]);
	printf("10th element of array 'iArrayOne[]'  = %d\n",iArrayOne[9]);

	//******* iArrayTwo[] *********
	//Piece-Meal assignmnet from user defined values
	printf("\n\n");

	printf("\nEnter the 1st element of 'iArrayTwo[]' :");
	scanf("%d", &iArrayTwo[0]);
	printf("\nEnter the 2nd element of 'iArrayTwo[]' :");
	scanf("%d", &iArrayTwo[1]);
	printf("\nEnter the 3rd element of 'iArrayTwo[]' :");
	scanf("%d", &iArrayTwo[2]);
	printf("\nEnter the 4th element of 'iArrayTwo[]' :");
	scanf("%d", &iArrayTwo[3]);
	printf("\nEnter the 5th element of 'iArrayTwo[]' :");
	scanf("%d", &iArrayTwo[4]);
	printf("\nEnter the 6th element of 'iArrayTwo[]' :");
	scanf("%d", &iArrayTwo[5]);
	printf("\nEnter the 7th element of 'iArrayTwo[]' :");
	scanf("%d", &iArrayTwo[6]);
	printf("\nEnter the 8th element of 'iArrayTwo[]' :");
	scanf("%d", &iArrayTwo[7]);
	printf("\nEnter the 9th element of 'iArrayTwo[]' :");
	scanf("%d", &iArrayTwo[8]);
	printf("\nEnter the 10th element of 'iArrayTwo[]' :");
	scanf("%d", &iArrayTwo[9]);

	printf("\n\n");
	printf("Piece-Meal Display of Elements of user defined values of 'iArrayTwo[]' \n\n");
	printf("1st element of array 'iArrayTwo[]'   = %d\n", iArrayTwo[0]);
	printf("2nd element of array 'iArrayTwo[]'   = %d\n", iArrayTwo[1]);
	printf("3rd element of array 'iArrayTwo[]'   = %d\n", iArrayTwo[2]);
	printf("4th element of array 'iArrayTwo[]'   = %d\n", iArrayTwo[3]);
	printf("5th element of array 'iArrayTwo[]'   = %d\n", iArrayTwo[4]);
	printf("6th element of array 'iArrayTwo[]'   = %d\n", iArrayTwo[5]);
	printf("7th element of array 'iArrayTwo[]'   = %d\n", iArrayTwo[6]);
	printf("8th element of array 'iArrayTwo[]'   = %d\n", iArrayTwo[7]);
	printf("9th element of array 'iArrayTwo[]'   = %d\n", iArrayTwo[8]);
	printf("10th element of array 'iArrayTwo[]'  = %d\n", iArrayTwo[9]);
	
	getch();
	return 0;


	

}