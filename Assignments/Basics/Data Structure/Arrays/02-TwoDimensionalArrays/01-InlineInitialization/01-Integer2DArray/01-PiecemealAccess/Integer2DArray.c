#include<stdio.h>
int main(void)
{
	//Variable declaration
	int iArray[5][3] = { {1,2,3},
						{2,4,6},
						{3,6,9},
						{4,8,12},
						{5,10,15} };	// IN-LINE INITIALIZATION OF ARRAY
	int int_size;
	int iArray_size;
	int iArray_num_elements, iArray_num_rows, iArray_num_columns;
	
	//Code
	printf("\n\n");
	int_size = sizeof(int);

	iArray_size = sizeof(iArray);
	printf("Size of 2-D array is = %d\n\n", iArray_size);

	iArray_num_rows = iArray_size / sizeof(iArray[0]);
	printf("Number of rows in 2-D array are = %d\n\n", iArray_num_rows);

	iArray_num_columns = sizeof(iArray[0]) / int_size;
	printf("Number of columns in 2-D array are = %d\n\n", iArray_num_columns);

	iArray_num_elements = iArray_num_columns * iArray_num_rows;
	printf("Number of elements in 2-D array are = %d\n\n", iArray_num_elements);

	printf("\n\n");
	printf("Elements in 2-D array are :\n\n");

	// *** ARRAY INDICES BEGIN FROM 0, HENCE, 1ST ROW IS ACTUALLY 0TH ROW AND 1ST COLUMN IS ACTUALLY 0TH COLUMN ***

	/*** Row 1 ***/
	printf("***** ROW 1 ******\n");
	printf("iArray[0][0] = %d\n", iArray[0][0]);
	printf("iArray[0][1] = %d\n", iArray[0][1]);
	printf("iArray[0][2] = %d\n", iArray[0][2]);

	printf("\n\n");

	/*** Row 2 ***/
	printf("***** ROW 2 ******\n");
	printf("iArray[1][0] = %d\n", iArray[1][0]);
	printf("iArray[1][1] = %d\n", iArray[1][1]);
	printf("iArray[1][2] = %d\n", iArray[1][2]);

	printf("\n\n");

	/*** Row 3 ***/
	printf("***** ROW 3 ******\n");
	printf("iArray[2][0] = %d\n", iArray[2][0]);
	printf("iArray[2][1] = %d\n", iArray[2][1]);
	printf("iArray[2][2] = %d\n", iArray[2][2]);

	printf("\n\n");
	
	/*** Row 4 ***/
	printf("***** ROW 4 ******\n");
	printf("iArray[3][0] = %d\n", iArray[3][0]);
	printf("iArray[3][1] = %d\n", iArray[3][1]);
	printf("iArray[3][2] = %d\n", iArray[3][2]);

	printf("\n\n");
	
	/*** Row 5 ***/
	printf("***** ROW 5 ******\n");
	printf("iArray[4][0] = %d\n", iArray[4][0]);
	printf("iArray[4][1] = %d\n", iArray[4][1]);
	printf("iArray[4][2] = %d\n", iArray[4][2]);

	printf("\n\n");

	return 0;
}
