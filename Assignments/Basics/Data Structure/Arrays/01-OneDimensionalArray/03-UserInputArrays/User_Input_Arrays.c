#include<stdio.h>
#include<conio.h>
// MACRO conatant can be used as array size
// Hence this program's array sizes can be changed by just changing the following global values

#define INT_ARRAY_SIZE 5
#define FLOAT_ARRAY_SIZE 3
#define CHAR_ARRAY_SIZE 15

int main(void)
{
	// Variable declarations
	int iArray[INT_ARRAY_SIZE];
	float fArray[FLOAT_ARRAY_SIZE];
	char cArray[CHAR_ARRAY_SIZE];

	// Code

	//******** ARRAY INPUT ************
	printf("\n\n");
	printf("Enter the elements for the integer array :\n\n");
	for (int i = 0; i < INT_ARRAY_SIZE; i++)
	{
		scanf("%d", &iArray[i]);
	}
	
	printf("\n\n");
	printf("Enter the elemeneyts for float array :\n\n");
	for (int i = 0; i < FLOAT_ARRAY_SIZE; i++)
	{
		scanf("%f", &fArray[i]);
	}

	printf("\n\n");
	printf("Enter the elements for character array :\n\n");
	for (int i = 0; i < CHAR_ARRAY_SIZE; i++)
	{
		cArray[i] = getch();
		printf("%c\n", cArray[i]);
	}

	//*********ARRAY OUTPUT***************

	printf("\n\n");
	printf("Integer array entered by you :\n\n");
	for (int i = 0; i < INT_ARRAY_SIZE; i++)
	{
		printf("%d\n", iArray[i]);
	}
	printf("\n\n");
	printf("Float array entered by you :\n\n");
	for (int i = 0; i < FLOAT_ARRAY_SIZE; i++)
	{
		printf("%f\n", fArray[i]);
	}

	printf("\n\n");
	printf("Character array entered by you :\n\n");
	for (int i = 0; i < CHAR_ARRAY_SIZE; i++)
	{
		printf("%c\n", cArray[i]);
	}
	getch();
	return 0;
}