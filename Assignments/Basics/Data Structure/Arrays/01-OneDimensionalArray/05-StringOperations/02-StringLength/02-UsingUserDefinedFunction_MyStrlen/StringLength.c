#include <stdio.h>

#define MAX_STRING_LENGTH 512

int main(void)
{
	//Function declarations
	int MyStrlen(char[]);

	//variable declarations 
	char chArray[MAX_STRING_LENGTH]; // A Character Array Is A String
	int iStringLength = 0;

	//Code

	//String Input
	printf("\n\n");
	printf("Enter the string :");
	gets_s(chArray, MAX_STRING_LENGTH);

	//String output
	printf("\n\n");
	printf("String entered by you is :\n\n");
	printf("%s\n", chArray);

	//String length

	printf("\n\n");
	iStringLength = MyStrlen(chArray);
	printf("String lnegth = %d characters !!\n\n", iStringLength);
	return 0;
}

int MyStrlen(char str[])
{
	//Variable declaration

	int string_len = 0;

	//Code
	//Determining  extact length of string by detecting the 1st occurance of null characters
	for (int i = 0; i < MAX_STRING_LENGTH; i++)
	{
		if (str[i] == '\0')
			break;
		else
			string_len++;
	}
	return (string_len);
}