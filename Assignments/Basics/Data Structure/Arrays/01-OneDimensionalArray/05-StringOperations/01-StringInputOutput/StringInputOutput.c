#include<stdio.h>
#define MAX_STRING_LENGTH 512

int main(void)
{
	// Variable declarations
	char chArray[MAX_STRING_LENGTH];		// A character array is a string
	
	// Code

	printf("\n\n");
	printf("Enter a string :\n\n");
	gets_s(chArray, MAX_STRING_LENGTH);

	printf("\n\n");
	printf("String entered by you is :\n\n");
	printf("%s\n", chArray);
	return 0;
}
