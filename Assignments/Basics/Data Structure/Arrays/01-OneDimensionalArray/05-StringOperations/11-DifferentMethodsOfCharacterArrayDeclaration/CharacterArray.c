/*DIFFERENT METHODS OF CHARACTER ARRAY DECLARATION*/
#include<stdio.h>
int main(void)
{
	// Variable declarations
	char chArray_01[] = { 'A','S','T','R','O','M','E','D','I','C','O','M','P','\0' }; //Must giVe \0 explicitly for proper initialization
	char chArray_02[9] = { 'W','E','L','C','O','M','E','S','\0' };	//Must give \0 explicitly for proper initialization
	char chArray_03[] = { 'Y','O','U','\0' };	//Must giVe \0 explicitly for proper initialization
	char chArray_04[] = "To";	// \0 is assumed, size is given as 3 though string has only 2 characters
	char chArray_05[] = "REAL TIME RENDERING BATCH OF 2018-19-20";	// \0 is assumed, size is given as 40 though string has 39 characters
	
	char chArray_WithoutNullTerminator[] = { 'H','e','l','l','o' };

	//Code
	printf("\n\n");

	printf("Size of charray_01 : %d\n\n", sizeof(chArray_01));
	printf("Size of charray_02 : %d\n\n", sizeof(chArray_02));
	printf("Size of charray_03 : %d\n\n", sizeof(chArray_03));
	printf("Size of charray_04 : %d\n\n", sizeof(chArray_04));
	printf("Size of charray_05 : %d\n\n", sizeof(chArray_05));

	printf("\n\n");

	printf("The strings are :\n\n");
	printf("chArray_01 : %s\n\n", chArray_01);
	printf("chArray_02 : %s\n\n", chArray_02);
	printf("chArray_03 : %s\n\n", chArray_03);
	printf("chArray_04 : %s\n\n", chArray_04);
	printf("chArray_05 : %s\n\n", chArray_05);

	printf("\n\n");
	printf("Size of chArray_WithoutNullTerminator : %d\n\n", sizeof(chArray_WithoutNullTerminator));
	printf("chArray_WithoutNullTerminator : %s\n\n", chArray_WithoutNullTerminator);

	return 0;
}