#include<stdio.h>
#define MAX_STRING_LENGTH 512

int main(void)
{
	//Function declaration
	void MyStrcpy(char[], char[]);

	//Variable declarations
	char chArray_Original[MAX_STRING_LENGTH];
	char chArray_Copy[MAX_STRING_LENGTH];

	//Code

	//String Input
	printf("\n\n");
	printf("Enter the string :");
	gets_s(chArray_Original, MAX_STRING_LENGTH);

	//String Copy
	strcpy(chArray_Copy, chArray_Original);

	//String output
	printf("\n\n");
	printf("Original String entered by you is :\n\n");
	printf("%s\n", chArray_Original);

	printf("\n\n");
	printf("Original String entered by you is :\n\n");
	printf("%s\n", chArray_Copy);

	return 0;
}

void MyStrcpy(char str_destination[], char str_source[])
{
	//Function declaration
	int MyStrlen(char[]);
	int j;
	//Variable declaration
	int iStringLength = 0;
	
	//Code

	iStringLength = MyStrlen(str_source);
	for ( j = 0; j < iStringLength; j++)
	{
		str_destination[j] = str_source[j];
	}

	str_destination[j] = '\0';
}

int MyStrlen(char str[])
{
	//Variable declaration
	int string_len = 0;

	//Code

	//Determining string length by detecting 1st occurance of null character
	for (int i = 0; i < MAX_STRING_LENGTH; i++)
	{
		if (str[i] == '\0')
			break;
		else
			string_len++;
	}
	return string_len;
}