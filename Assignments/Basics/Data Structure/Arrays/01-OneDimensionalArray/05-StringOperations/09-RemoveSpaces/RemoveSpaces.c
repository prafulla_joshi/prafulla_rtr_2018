#include<stdio.h>
#define MAX_STRING_LENGTH 512

int main(void)
{
	// function declaration
	int MyStrlen(char[]);
	void MyStrcpy(char[], char[]);

	//Variable declaration
	char chArray[MAX_STRING_LENGTH], chArray_Spaces_Removed[MAX_STRING_LENGTH];
	int iStringLength;
	int i, j;

	// Code

	// STRING INPUT
	printf("\n\n");
	printf("Enter a string :\n\n");
	gets_s(chArray, MAX_STRING_LENGTH);

	iStringLength = MyStrlen(chArray);
	j = 0;
	for (i = 0; i < iStringLength; i++)
	{
		if (chArray[i] == ' ')
			continue;
		else
		{
			chArray_Spaces_Removed[j] = chArray[i];
			j++;
		}
	}

	chArray_Spaces_Removed[j] = '\0';

	// STRING OUTPUT
	printf("\n\n");
	printf("String entered by you :\n\n");
	printf("%s\n", chArray);

	printf("\n\n");
	printf("String after removal of spaces :\n\n");
	printf("%s\n", chArray_Spaces_Removed);

	return 0;
}

int MyStrlen(char str[])
{
	int j;
	int string_length = 0;
	for (j = 0; j < MAX_STRING_LENGTH; j++)
	{
		if (str[j] == '\0')
			break;
		else
			string_length++;
	}
	return string_length;
}