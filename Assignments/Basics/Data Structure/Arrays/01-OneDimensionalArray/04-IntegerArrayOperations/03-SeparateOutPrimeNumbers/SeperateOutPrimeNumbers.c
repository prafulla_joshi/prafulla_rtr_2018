#include<stdio.h>
#define NUM_ELEMENTS 10

int main(void)
{
	//Variable declarations
	int iArray[NUM_ELEMENTS];
	int num, count = 0;

	//Code

	//Array Input
	printf("Enter 10 integer elements for array :\n\n");
	for (int i = 0; i < NUM_ELEMENTS; i++)
	{
		scanf("%d", &num);

		// If 'num' is negative then convert it to positive (multiply by -1)
		if (num < 0)
			num = -1 * num;

		iArray[i] = num;
	}

	//Printing entire array
	printf("\n\n");
	printf("Array elements are :\n\n");
	for (int i = 0; i < NUM_ELEMENTS; i++)
		printf("%d\n", iArray[i]);

	// Separate out Prime numbers from array
	printf("\n\n");
	printf("Prime numbers from the array are :\n\n");
	for (int i = 0; i < NUM_ELEMENTS; i++)
	{
		for (int j = 1; j <= iArray[i]; j++)
		{
			if ((iArray[i] % j) == 0)
				count++;
		}

		/*   NUMBER 1 IS NEITHER PRIME NOR COMPOSITE
		IF A NUMBER IS PRIME, IT IS ONLY DIVISIBLE BY 1 AND ITSELF
		HENCE IF A NUMBER IS PRIME THEN VALUE OF 'count' WILL BE EXACTLY 2
		IF THE VALUE OF 'count' IS GREATER THAN 2, THEN THE NUMBER IS NOT PRIME
		THE VALUE OF 'count' WILL BE 1 ONLY IF iArray[i] IS 1   */
		if (count == 2)
			printf("%d\n", iArray[i]);
		
		count = 0;		// RESET 'count' TO 0 FOR CHECKING THE NEXT NUMBER
	}
	return 0;
}



