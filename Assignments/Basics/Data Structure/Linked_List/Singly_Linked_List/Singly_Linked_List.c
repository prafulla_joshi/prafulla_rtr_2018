#include<stdio.h>
#include<stdlib.h>

struct node
{
	int data;
	struct node *next;
}*head;


int main(void)
{
	//Function declaration
	void Create(int);
	int Display(void);
	void Add_At_Start(int);
	void Add_After(int, int);
	int Delete_Element(int);
	int Reverse(void);
	//Variable declarations
	int ch, NumberOfElements, position, element;

	//Code
	head = NULL;	// making the head NULL since the list is empty initially

	while (1)
	{
		printf("\n0)Exit \n1)Create \n2)Display \n3)Add_At_Start \n4)Add_After \n5)Delete_Element \n6)Reverse \n");
		printf("\nEnter your choice :");
		scanf("%d", &ch);
		switch (ch)
		{
		case 0:
			exit(0);
			break;
		case 1:
			printf("\nHow many elements do you want to add linked list :");
			scanf("%d", &NumberOfElements);
			for (int i = 0; i < NumberOfElements; i++)
			{
				printf("Enter the element :");
				scanf("%d", &element);
				Create(element);
			}
			break;
		case 2:
			Display();
			break;
		case 3:
			printf("\nEnter the element to add at start of linked list :");
			scanf("%d", &element);
			Add_At_Start(element);
			printf("\nElement added at the begining successfully !!");
			break;
		case 4:
			printf("Enter the position after which you want to add a new element :");
			scanf("%d", &position);
			printf("Enter the element to do you want to add in linked list :");
			scanf("%d", &element);
			Add_After(element, position);
			break;
		case 5:
			printf("\nEnter the element to be deleted :");
			scanf("%d", &element);
			Delete_Element(element);
			break;
		case 6:
			printf("\nThe reversed linked list is :\n");
			Reverse();
			Display();
			break;
		default:
			printf("\nPlease enter the correct choice :");
			break;
		}
	}
}


void Create(int element)
{
	//Code
	struct node *tmp, *q;

	tmp = (struct node*)malloc(sizeof(struct node));
	tmp->data = element;
	tmp->next = NULL;

	if (head == NULL)
	{
		head = tmp;									// Point head to tmp...since it will be the first element
	}
	else
	{
		q = head;
		while (q->next != NULL)
			q = q->next;
		q->next = tmp;								// Put address of tmp in q
	}
}

int Display()
{
	//Code
	struct node *q;
	if (head == NULL)
	{
		printf("\nList is empty\n\n");
		return 0;
	}
	q = head;								// Point q to head
	printf("\nThe list is :\n");
	while (q != NULL)
	{
		printf("\n%d", q->data);
		q = q->next;				// point q to q->next
	}
	printf("\n");
}


void Add_At_Start(int element)
{
	//Code

	struct node *tmp;
	tmp = (struct node*)malloc(sizeof(struct node));
	tmp->data = element;
	tmp->next = head;			// Making the tmp to point to the node where head is pointing now

	head = tmp;					// Make head to point to tmp
}

void Add_After(int element, int position)
{
	//Code

	struct node *tmp, *q;
	q = head;							// Point q to head
	tmp = (struct node *)malloc(sizeof(struct node));
	tmp->data = element;
	for  (int i = 0; i < position-1; i++)
		q = q->next;
	tmp->next = q->next;				// We are first making forward link and then breaking the previous link. Make tmp->next to point to q->next
	q->next = tmp;						// Making q point to tmp by this statement
}

int Delete_Element(int element)
{
	//code
	struct node *tmp,*q;
	/*---FOLLOWING CODE IS FOR FIRST ELEMENT DELETION---*/
	if (head->data == element)			// Check whether it is the first element or not where head is pointing
	{
		tmp = head;						// Make tmp point to head
		head = head->next;				// Make head to point to next element in the list(2nd element..;so that first element can be deleted safely)
		free(tmp);
		printf("\nElement deleted successfully !!\n");
		return 0;
	}

	/*---FOLLOWING CODE IS FOR MIDDLE ELEMENT DELETION---*/

	q = head;							// Making q to point to head
	while (q->next->next!=NULL)
	{									// Here we want tmp at actual poistion of the element to be deleted and q just before it
		if (q->next->data == element)
		{
			tmp = q->next;				// Making tmp to point to p->next(next node of q)
			q->next = tmp->next;
			free(tmp);
			printf("\nElement deleted successfully !!\n");
			return 0;
		}
		q = q->next;
	}

	/*---FOLLOWING CODE IS FOR LAST ELEMENT DELETION---*/


	if (q->next->data == element)
	{
		tmp = q->next;
		free(tmp);
		q->next = NULL;
		printf("\nElement deleted successfully !!\n");
		return 0;
	}

	printf("\nElement not found !!!\n");
}


int Reverse()
{
	//Code

	struct node *p1, *p2, *p3;
	if (head->next == NULL)
	{
		return 0;
	}
	p1 = head;
	p2 = p1->next;
	p3 = p2->next;
	p1->next = NULL;
	p2->next = p1;
	while (p3 != NULL)
	{
		p1 = p2;
		p2 = p3;
		p3 = p3->next;
		p2->next = p1;
	}
	head = p2;
	return 0;
}