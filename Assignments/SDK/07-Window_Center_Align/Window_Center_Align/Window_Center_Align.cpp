// Headerfiles
#include<windows.h>
#define WIDTH 800
#define HEIGHT 600
// Global function declarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

// WinMain

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	// Variable declarations
	TCHAR szAppName[] = TEXT("Window_Center_Align");
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	// Following variables required for getting screen widht and height to align window at center
	int iMonitorWidth;
	int iMonitorHeight;
	int xPoint;
	int yPoint;
	//Code

	iMonitorWidth = GetSystemMetrics(SM_CXSCREEN);		// Get the Monitor width
	iMonitorHeight = GetSystemMetrics(SM_CYSCREEN);		// Get the Monitor height

	xPoint = (iMonitorWidth / 2) - (WIDTH / 2);			// For center alignment of window
	yPoint = (iMonitorHeight / 2) - (HEIGHT / 2);		//Get the x and y for createwindow parameters

	// Registering the class

	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.lpszClassName = szAppName;
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hIconSm = LoadIcon(NULL, IDC_ARROW);
	wndclass.lpszMenuName = NULL;

	// Registering the class
	RegisterClassEx(&wndclass);

	//Creating the window

	hwnd = CreateWindow(szAppName,
		TEXT("Center Aligned Window--Prafulla"),
		WS_OVERLAPPEDWINDOW,
		xPoint,
		yPoint,
		WIDTH,
		HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	// Show window and update window
	ShowWindow(hwnd, iCmdShow);
	UpdateWindow(hwnd);

	// Message loop

	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

	return (int)msg.wParam;
}


LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	// Variable declarations
	HDC hdc;
	RECT rc;
	TCHAR str[] = TEXT("This window is aligned at center of the monitor !!");
	PAINTSTRUCT ps;
	// Code
	switch (iMsg)
	{
	case WM_PAINT:
		GetClientRect(hwnd, &rc);
		hdc = BeginPaint(hwnd, &ps);
		SetBkColor(hdc, RGB(200, 255, 0));
		SetTextColor(hdc, RGB(0, 0, 0));
		DrawText(hdc, str, -1, &rc, DT_SINGLELINE | DT_CENTER | DT_VCENTER);
		EndPaint(hwnd, &ps);
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	}

	return DefWindowProc(hwnd, iMsg, wParam, lParam);
}